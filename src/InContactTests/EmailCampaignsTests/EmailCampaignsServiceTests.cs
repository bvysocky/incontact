﻿#region License
// The MIT License (MIT)
//
// Copyright (c) 2015 Scott Lance, Ethan Tipton
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// 
// The most recent version of this license can be found at: http://opensource.org/licenses/MIT
#endregion

using InContactSdk.EmailCampaigns;
using InContactSdk.Helpers;
using InContactSdk.Models;
using InContactSdk.Validation;
using Moq;
using System;
using System.Diagnostics.CodeAnalysis;
using System.Net.Http;
using System.Threading.Tasks;
using Xunit;

namespace InContactTests.EmailCampaignsTests
{
    [ExcludeFromCodeCoverage]
    public class EmailCampaignsServiceTests
    {
        private EmailCampaignsService emailCampaignsService = null;
        private IInContactSettings settings = null;
        private Mock<IValidationService> validationService = null;
        private Mock<ISerialization> serialization = null;
        private Mock<IWebServiceRequest> webServiceRequest = null;

        public EmailCampaignsServiceTests()
        {
            settings = new InContactSettings("123456789", "D27D92A2-6A0B-4F33-BE8A-3C63E597AB18");
            validationService = new Mock<IValidationService>();
            serialization = new Mock<ISerialization>();
            webServiceRequest = new Mock<IWebServiceRequest>();
        }

        public void InitializeContactTrackingService<T>()
            where T : class, new()
        {
            validationService.Setup(m => m.Validate(It.IsAny<IValidatable>()));
            serialization.Setup(m => m.Serialize(It.IsAny<object>())).Returns("");
            serialization.Setup(m => m.Deserialize<T>(It.IsAny<string>())).Returns(new T());
            emailCampaignsService = new EmailCampaignsService(settings, validationService.Object, webServiceRequest.Object);
        }

        // *************************************************************************************************    

        [Fact]
        public async Task GetEmailCampaignsAsync_Uses_Correct_Endpoint()
        {
            // Arrange
            InitializeContactTrackingService<PaginatedResult<EmailCampaignResponse>>();
            Uri endpoint = null;

            webServiceRequest.Setup(m => m.GetDeserializedAsync<PaginatedResult<EmailCampaignResponse>>(It.IsAny<Uri>(), It.IsAny<string>()))
                .Callback((Uri a, string b) => endpoint = a)
                .ReturnsAsync(new PaginatedResult<EmailCampaignResponse>());

            // Act
            await emailCampaignsService.GetEmailCampaignsAsync();

            // Assert
            Assert.Equal(string.Format("{0}emailmarketing/campaigns?api_key={1}", settings.BaseUrl, settings.ApiKey), endpoint.AbsoluteUri);
        }

        [Fact]
        public async Task GetEmailCampaignsAsync_Limit_500_Uses_Correct_Endpoint()
        {
            // Arrange
            InitializeContactTrackingService<PaginatedResult<EmailCampaignResponse>>();
            int limit = 500;
            Uri endpoint = null;

            webServiceRequest.Setup(m => m.GetDeserializedAsync<PaginatedResult<EmailCampaignResponse>>(It.IsAny<Uri>(), It.IsAny<string>()))
                .Callback((Uri a, string b) => endpoint = a)
                .ReturnsAsync(new PaginatedResult<EmailCampaignResponse>());

            // Act
            await emailCampaignsService.GetEmailCampaignsAsync(limit);

            // Assert
            Assert.Equal(string.Format("{0}emailmarketing/campaigns?api_key={1}&limit=500", settings.BaseUrl, settings.ApiKey), endpoint.AbsoluteUri);
        }

        [Fact]
        public async Task GetEmailCampaignsAsync_ModifiedSince_Is_Not_Null_Uses_Correct_Endpoint()
        {
            // Arrange
            InitializeContactTrackingService<PaginatedResult<EmailCampaignResponse>>();
            var modifiedSince = new DateTime(2010, 1, 1);
            Uri endpoint = null;

            webServiceRequest.Setup(m => m.GetDeserializedAsync<PaginatedResult<EmailCampaignResponse>>(It.IsAny<Uri>(), It.IsAny<string>()))
                .Callback((Uri a, string b) => endpoint = a)
                .ReturnsAsync(new PaginatedResult<EmailCampaignResponse>());

            // Act
            await emailCampaignsService.GetEmailCampaignsAsync(modifiedSince: modifiedSince);

            // Assert
            Assert.Equal(string.Format("{0}emailmarketing/campaigns?api_key={1}&modified_since={2}", settings.BaseUrl, settings.ApiKey, modifiedSince.ToString("o")), endpoint.AbsoluteUri);
        }

        [Fact]
        public async Task GetEmailCampaignsAsync_EmailCampaignStatus_Draft_Uses_Correct_Endpoint()
        {
            // Arrange
            InitializeContactTrackingService<PaginatedResult<EmailCampaignResponse>>();
            var status = EmailCampaignStatus.Draft;
            Uri endpoint = null;

            webServiceRequest.Setup(m => m.GetDeserializedAsync<PaginatedResult<EmailCampaignResponse>>(It.IsAny<Uri>(), It.IsAny<string>()))
                .Callback((Uri a, string b) => endpoint = a)
                .ReturnsAsync(new PaginatedResult<EmailCampaignResponse>());

            // Act
            await emailCampaignsService.GetEmailCampaignsAsync(status: status);

            // Assert
            Assert.Equal(string.Format("{0}emailmarketing/campaigns?api_key={1}&status=DRAFT", settings.BaseUrl, settings.ApiKey), endpoint.AbsoluteUri);
        }

        [Fact]
        public async Task GetEmailCampaignsAsync_Limit_500_ModifiedSince_Not_Null_EmailCampaignStatus_Draft_Uses_Correct_Endpoint()
        {
            // Arrange
            InitializeContactTrackingService<PaginatedResult<EmailCampaignResponse>>();
            var limit = 500;
            var modifiedSince = new DateTime(2010, 1, 1);
            var status = EmailCampaignStatus.Draft;
            Uri endpoint = null;

            webServiceRequest.Setup(m => m.GetDeserializedAsync<PaginatedResult<EmailCampaignResponse>>(It.IsAny<Uri>(), It.IsAny<string>()))
                .Callback((Uri a, string b) => endpoint = a)
                .ReturnsAsync(new PaginatedResult<EmailCampaignResponse>());

            // Act
            await emailCampaignsService.GetEmailCampaignsAsync(limit, modifiedSince, status);

            // Assert
            Assert.Equal(string.Format("{0}emailmarketing/campaigns?api_key={1}&limit=500&modified_since={2}&status=DRAFT", settings.BaseUrl, settings.ApiKey, modifiedSince.ToString("o")), endpoint.AbsoluteUri);
        }

        [Fact]
        public async Task GetEmailCampaignsAsync_Model_Is_Valid_Verify_MakeSerializedRequestAsync()
        {
            // Arrange
            InitializeContactTrackingService<PaginatedResult<EmailCampaignResponse>>();

            // Act
            await emailCampaignsService.GetEmailCampaignsAsync();

            // Assert
            webServiceRequest.Verify(m => m.GetDeserializedAsync<PaginatedResult<EmailCampaignResponse>>(It.IsAny<Uri>(), It.IsAny<string>()), Times.Once());
        }

        // *************************************************************************************************

        [Fact]
        public async Task CreateEmailCampaignAsync_Uses_Correct_Endpoint()
        {
            // Arrange
            InitializeContactTrackingService<EmailCampaign>();
            var emailCampaign = new EmailCampaign
            {
                Name = "Test SDK Campaign",
                Subject = "Test SDK Campaign",
                FromName = "California Institute of Technology",
                FromEmail = "scooper@caltech.edu",
                ReplyToEmail = "scooper@caltech.edu",
                IsPermissionReminderEnabled = false,
                IsViewAsWebpageEnabled = false,
                GreetingSalutations = "Greetings and Salutations",
                GreetingName = GreetingName.FirstName,
                GreetingString = "Dear, ",
                EmailContent = "<html><body><p>This is test email campaign</p></body></html>",
                TextContent = "This is a test email campaign",
                EmailContentFormat = EmailContentFormat.Html
            };
            Uri endpoint = null;

            webServiceRequest.Setup(m => m.PostDeserializedAsync<EmailCampaign, EmailCampaign>(It.IsAny<Uri>(), It.IsAny<string>(), It.IsAny<EmailCampaign>()))
                .Callback((Uri a, string b, EmailCampaign c) => endpoint = a)
                .ReturnsAsync(new EmailCampaign());

            // Act
            await emailCampaignsService.CreateEmailCampaignAsync(emailCampaign);

            // Assert
            Assert.Equal(string.Format("{0}emailmarketing/campaigns?api_key={1}", settings.BaseUrl, settings.ApiKey), endpoint.AbsoluteUri);
        }

        [Fact]
        public async Task CreateEmailCampaignAsync_Is_Valid_Verify_MakeSerializedRequestAsync()
        {
            // Arrange
            InitializeContactTrackingService<EmailCampaign>();
            var emailCampaign = new EmailCampaign
            {
                Name = "Test SDK Campaign",
                Subject = "Test SDK Campaign",
                FromName = "California Institute of Technology",
                FromEmail = "scooper@caltech.edu",
                ReplyToEmail = "scooper@caltech.edu",
                IsPermissionReminderEnabled = false,
                IsViewAsWebpageEnabled = false,
                GreetingSalutations = "Greetings and Salutations",
                GreetingName = GreetingName.FirstName,
                GreetingString = "Dear, ",
                EmailContent = "<html><body><p>This is test email campaign</p></body></html>",
                TextContent = "This is a test email campaign",
                EmailContentFormat = EmailContentFormat.Html
            };

            // Act
            await emailCampaignsService.CreateEmailCampaignAsync(emailCampaign);

            // Assert
            webServiceRequest.Verify(m => m.PostDeserializedAsync<EmailCampaign, EmailCampaign>(It.IsAny<Uri>(), It.IsAny<string>(), It.IsAny<EmailCampaign>()), Times.Once());
        }

        // *************************************************************************************************      

        [Fact]
        public async Task GetEmailCampaignAsync_UpdateSummary_False_Uses_Correct_Endpoint()
        {
            // Arrange
            InitializeContactTrackingService<EmailCampaign>();
            var campaignId = "123456";
            Uri endpoint = null;

            webServiceRequest.Setup(m => m.GetDeserializedAsync<EmailCampaign>(It.IsAny<Uri>(), It.IsAny<string>()))
                .Callback((Uri a, string b) => endpoint = a)
                .ReturnsAsync(new EmailCampaign());

            // Act
            await emailCampaignsService.GetEmailCampaignAsync(campaignId);

            // Assert
            Assert.Equal(string.Format("{0}emailmarketing/campaigns/{1}?api_key={2}&updateSummary=False", settings.BaseUrl, campaignId, settings.ApiKey), endpoint.AbsoluteUri);
        }

        [Fact]
        public async Task GetEmailCampaignAsync_UpdateSummary_True_Uses_Correct_Endpoint()
        {
            // Arrange
            InitializeContactTrackingService<EmailCampaign>();
            var campaignId = "123456";
            var updateSummary = true;
            Uri endpoint = null;

            webServiceRequest.Setup(m => m.GetDeserializedAsync<EmailCampaign>(It.IsAny<Uri>(), It.IsAny<string>()))
                .Callback((Uri a, string b) => endpoint = a)
                .ReturnsAsync(new EmailCampaign());

            // Act
            await emailCampaignsService.GetEmailCampaignAsync(campaignId, updateSummary);

            // Assert
            Assert.Equal(string.Format("{0}emailmarketing/campaigns/{1}?api_key={2}&updateSummary=True", settings.BaseUrl, campaignId, settings.ApiKey), endpoint.AbsoluteUri);
        }

        [Fact]
        public async Task GetEmailCampaignAsync_Is_Valid_Verify_MakeSerializedRequestAsync()
        {
            // Arrange
            InitializeContactTrackingService<EmailCampaign>();
            var campaignId = "123456";

            // Act
            await emailCampaignsService.GetEmailCampaignAsync(campaignId);

            // Assert
            webServiceRequest.Verify(m => m.GetDeserializedAsync<EmailCampaign>(It.IsAny<Uri>(), It.IsAny<string>()), Times.Once());
        }

        // *************************************************************************************************       

        [Fact]
        public async Task UpdateEmailCampaignsAsync_Uses_Correct_Endpoint()
        {
            // Arrange
            InitializeContactTrackingService<EmailCampaign>();
            var campaignId = "123456";
            var emailCampaign = new EmailCampaign
            {
                Name = "Test SDK Campaign",
                Subject = "Test SDK Campaign",
                FromName = "California Institute of Technology",
                FromEmail = "scooper@caltech.edu",
                ReplyToEmail = "scooper@caltech.edu",
                IsPermissionReminderEnabled = false,
                IsViewAsWebpageEnabled = false,
                GreetingSalutations = "Greetings and Salutations",
                GreetingName = GreetingName.FirstName,
                GreetingString = "Dear, ",
                EmailContent = "<html><body><p>This is test email campaign</p></body></html>",
                TextContent = "This is a test email campaign",
                EmailContentFormat = EmailContentFormat.Html
            };
            Uri endpoint = null;

            webServiceRequest.Setup(m => m.PutDeserializedAsync<EmailCampaign, EmailCampaign>(It.IsAny<Uri>(), It.IsAny<string>(), It.IsAny<EmailCampaign>()))
                .Callback((Uri a, string b, EmailCampaign c) => endpoint = a)
                .ReturnsAsync(new EmailCampaign());

            // Act
            await emailCampaignsService.UpdateEmailCampaignAsync(campaignId, emailCampaign);

            // Assert
            Assert.Equal(string.Format("{0}emailmarketing/campaigns/{1}?api_key={2}", settings.BaseUrl, campaignId, settings.ApiKey), endpoint.AbsoluteUri);
        }

        [Fact]
        public async Task UpdateEmailCampaignAsync_Is_Valid_Verify_MakeSerializedRequestAsync()
        {
            // Arrange
            InitializeContactTrackingService<EmailCampaign>();
            var campaignId = "123456";
            var emailCampaign = new EmailCampaign
            {
                Name = "Test SDK Campaign",
                Subject = "Test SDK Campaign",
                FromName = "California Institute of Technology",
                FromEmail = "scooper@caltech.edu",
                ReplyToEmail = "scooper@caltech.edu",
                IsPermissionReminderEnabled = false,
                IsViewAsWebpageEnabled = false,
                GreetingSalutations = "Greetings and Salutations",
                GreetingName = GreetingName.FirstName,
                GreetingString = "Dear, ",
                EmailContent = "<html><body><p>This is test email campaign</p></body></html>",
                TextContent = "This is a test email campaign",
                EmailContentFormat = EmailContentFormat.Html
            };

            // Act
            await emailCampaignsService.UpdateEmailCampaignAsync(campaignId, emailCampaign);

            // Assert
            webServiceRequest.Verify(m => m.PutDeserializedAsync<EmailCampaign, EmailCampaign>(It.IsAny<Uri>(), It.IsAny<string>(), It.IsAny<EmailCampaign>()), Times.Once());
        }

        // *************************************************************************************************

        [Fact]
        public async Task DeleteEmailCampaignsAsync_Uses_Correct_Endpoint()
        {
            // Arrange
            InitializeContactTrackingService<EmailCampaign>();
            var campaignId = "123456";
            Uri endpoint = null;

            webServiceRequest.Setup(m => m.DeleteAsync(It.IsAny<Uri>(), It.IsAny<string>()))
                .Callback((Uri a, string b) => endpoint = a)
                .ReturnsAsync(new HttpResponseMessage());

            // Act
            await emailCampaignsService.DeleteEmailCampaignAsync(campaignId);

            // Assert
            Assert.Equal(string.Format("{0}emailmarketing/campaigns/{1}?api_key={2}", settings.BaseUrl, campaignId, settings.ApiKey), endpoint.AbsoluteUri);
        }

        [Fact]
        public async Task DeleteEmailCampaignAsync_Is_Valid_Verify_MakeSerializedRequestAsync()
        {
            // Arrange
            InitializeContactTrackingService<EmailCampaign>();
            var campaignId = "123456";

            // Act
            await emailCampaignsService.DeleteEmailCampaignAsync(campaignId);

            // Assert
            webServiceRequest.Verify(m => m.DeleteAsync(It.IsAny<Uri>(), It.IsAny<string>()), Times.Once());
        }

    }
}