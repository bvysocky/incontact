﻿#region License
// The MIT License (MIT)
//
// Copyright (c) 2015 Scott Lance, Ethan Tipton
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// 
// The most recent version of this license can be found at: http://opensource.org/licenses/MIT
#endregion

using InContactSdk.EmailCampaigns;
using InContactSdk.EmailCampaigns.Validation;
using InContactSdk.Helpers;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using Xunit;

namespace InContactTests.EmailCampaignsTests.ValidationTests
{
    [ExcludeFromCodeCoverage]
    public class EmailCampaignValidatorTests
    {
        private readonly EmailCampaignValidator validator;

        public EmailCampaignValidatorTests()
        {
            validator = new EmailCampaignValidator();
        }

        // *************************************************************************************************

        [Fact]
        public void EmailContent_Is_Null_Generates_Error()
        {
            // Arrange
            var emailCampaign = new EmailCampaign
            {
                EmailContent = ""
            };

            // Act
            var result = validator.Validate(emailCampaign);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage == "EmailContent is required"));
        }

        [Fact]
        public void EmailContent_Length_Is_Greater_Than_930000_Characters_Generates_Error()
        {
            // Arrange
            StringBuilder emailContent = new StringBuilder();
            for (int i = 0; i < 930002; i++)
                emailContent.Append("k");

            var emailCampaign = new EmailCampaign
            {
                EmailContent = emailContent.ToString()
            };

            // Act
            var result = validator.Validate(emailCampaign);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage == "EmailContent must not exceed 930,000 characters"));
        }

        [Fact]
        public void EmailContent_Is_Not_Empty_When_Email_Template_Type_Is_STOCK_Generates_Error()
        {
            // Arrange
            var emailCampaign = new EmailCampaign
            {
                EmailContent = "<html><body>Test email</body></html>",
                TemplateType = TemplateType.Stock
            };

            // Act
            var result = validator.Validate(emailCampaign);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage == "EmailContent cannot be set when TemplateType is set to STOCK or TEMPLATE_V2"));
        }

        [Fact]
        public void EmailContent_Is_Not_Empty_When_Email_Template_Type_Is_TEMPLATE_V2_Generates_Error()
        {
            // Arrange
            var emailCampaign = new EmailCampaign
            {
                EmailContent = "<html><body>Test email</body></html>",
                TemplateType = TemplateType.TemplateV2
            };

            // Act
            var result = validator.Validate(emailCampaign);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage == "EmailContent cannot be set when TemplateType is set to STOCK or TEMPLATE_V2"));
        }

        [Fact]
        public void EmailContent_Is_Not_Empty_When_Email_Template_Type_Is_CUSTOM_Is_Valid()
        {
            // Arrange
            var emailCampaign = new EmailCampaign
            {
                EmailContent = "<html><body>Test email</body></html>",
                TemplateType = TemplateType.Custom
            };

            // Act
            var result = validator.Validate(emailCampaign);

            // Assert
            Assert.False(result.Errors.Any(e => e.ErrorMessage == "EmailContent cannot be set when TemplateType is set to STOCK or TEMPLATE_V2"));
        }

        [Fact]
        public void EmailContent_Is_Null_When_Email_Template_Type_Is_Valid()
        {
            // Arrange
            var emailCampaign = new EmailCampaign
            {
                TemplateType = TemplateType.Stock
            };

            // Act
            var result = validator.Validate(emailCampaign);

            // Assert
            Assert.False(result.Errors.Any(e => e.ErrorMessage == "EmailContent cannot be set when TemplateType is set to STOCK or TEMPLATE_V2"));
        }

        [Fact]
        public void EmailContent_Is_Null_When_Email_Template_Type_Is_TEMPLATE_V2_Is_Valid()
        {
            // Arrange
            var emailCampaign = new EmailCampaign
            {
                TemplateType = TemplateType.TemplateV2
            };

            // Act
            var result = validator.Validate(emailCampaign);

            // Assert
            Assert.False(result.Errors.Any(e => e.ErrorMessage == "EmailContent cannot be set when TemplateType is set to STOCK or TEMPLATE_V2"));
        }

        [Fact]
        public void EmailContentFormat_Is_None_Is_Valid()
        {
            // Arrange
            var emailCampaign = new EmailCampaign
            {
                EmailContentFormat = EmailContentFormat.None
            };

            // Act
            var result = validator.Validate(emailCampaign);

            // Assert
            Assert.False(result.Errors.Any(e => e.ErrorMessage == "EmailContentFormat must be blank or one of the following values: HTML, XHTML"));
        }

        [Fact]
        public void EmailContentFormat_Is_HTML_Is_Valid()
        {
            // Arrange
            var emailCampaign = new EmailCampaign
            {
                EmailContentFormat = EmailContentFormat.Html
            };

            // Act
            var result = validator.Validate(emailCampaign);

            // Assert
            Assert.False(result.Errors.Any(e => e.ErrorMessage == "EmailContentFormat must be blank or one of the following values: HTML, XHTML"));
        }

        [Fact]
        public void EmailContentFormat_Is_XHTML_Is_Valid()
        {
            // Arrange
            var emailCampaign = new EmailCampaign
            {
                EmailContentFormat = EmailContentFormat.Xhtml
            };

            // Act
            var result = validator.Validate(emailCampaign);

            // Assert
            Assert.False(result.Errors.Any(e => e.ErrorMessage == "EmailContentFormat must be blank or one of the following values: HTML, XHTML"));
        }

        [Fact]
        public void EmailContentFormat_Is_Set_When_Template_Type_Is_TEMPLATE_V2_Generates_Error()
        {
            // Arrange
            var emailCampaign = new EmailCampaign
            {
                EmailContentFormat = EmailContentFormat.Html,
                TemplateType = TemplateType.TemplateV2
            };

            // Act
            var result = validator.Validate(emailCampaign);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage == "EmailContentFormat cannot be set when TemplateType is set to TEMPLATE_V2"));
        }

        [Fact]
        public void EmailContentFormat_Is_Not_Set_When_Template_Type_Is_TEMPLATE_V2_Is_Valid()
        {
            // Arrange
            var emailCampaign = new EmailCampaign
            {
                EmailContentFormat = EmailContentFormat.None,
                TemplateType = TemplateType.TemplateV2
            };

            // Act
            var result = validator.Validate(emailCampaign);

            // Assert
            Assert.False(result.Errors.Any(e => e.ErrorMessage == "EmailContentFormat cannot be set when TemplateType is set to TEMPLATE_V2"));
        }

        [Fact]
        public void FromEmail_Is_Blank_Generates_Error()
        {
            // Arrange
            var emailCampaign = new EmailCampaign
            {
                FromEmail = ""
            };

            // Act
            var result = validator.Validate(emailCampaign);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage == "FromEmail is required"));
        }

        [Fact]
        public void FromEmail_Is_Greater_Than_80_Characters_Generates_Error()
        {
            // Arrange
            var emailCampaign = new EmailCampaign
            {
                FromEmail = "thisisasuperduperinsanelylongemailthatnooneintheirrightmindwouldmake@reallystupidemail.com"
            };

            // Act
            var result = validator.Validate(emailCampaign);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage == "FromEmail must not exceed 80 characters"));
        }

        [Fact]
        public void FromName_Is_Blank_Generates_Error()
        {
            // Arrange
            var emailCampaign = new EmailCampaign
            {
                FromName = ""
            };

            // Act
            var result = validator.Validate(emailCampaign);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage == "FromName is required"));
        }

        [Fact]
        public void FromName_Is_Greater_Than_80_Characters_Generates_Error()
        {
            // Arrange
            var emailCampaign = new EmailCampaign
            {
                FromName = "thisisasuperduperinsanelylongNamethatnooneintheirrightmindwouldNametheirchildandwouldbecriminaliftheydid"
            };

            // Act
            var result = validator.Validate(emailCampaign);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage == "FromName must not exceed 100 characters"));
        }

        [Fact]
        public void Greeting_Salutation_Is_Greater_Than_50_Characters_Generates_Error()
        {
            // Arrange
            // This is stupid...  Yep thats 1,501 characters of stupid
            var emailCampaign = new EmailCampaign
            {
                GreetingSalutations = "Dear Mr Mrs Ms Wives Children Grandparents And Everyone Else In This Whole Freaking World Receiving This Email:"
            };

            // Act
            var result = validator.Validate(emailCampaign);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage == "GreetingSalutations must not exceed 50 characters"));
        }

        [Fact]
        public void GreetingSalutations_Is_Blank_Is_Valid()
        {
            // Arrange
            var emailCampaign = new EmailCampaign
            {
                GreetingSalutations = ""
            };

            // Act
            var result = validator.Validate(emailCampaign);

            // Assert
            Assert.False(result.Errors.Any(e => e.ErrorMessage == "GreetingSalutations must not exceed 80 characters"));
        }

        [Fact]
        public void GreetingString_Is_Greater_Than_1500_Characters_Generates_Error()
        {
            // Arrange
            // This is stupid...  Yep thats 1,501 characters of stupid
            var emailCampaign = new EmailCampaign
            {
                GreetingString = "stupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupids"
            };

            // Act
            var result = validator.Validate(emailCampaign);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage == "GreetingString must not exceed 1,500 characters"));
        }

        [Fact]
        public void GreetingString_Is_Blank_Is_Valid()
        {
            // Arrange
            var emailCampaign = new EmailCampaign
            {
                GreetingString = ""
            };

            // Act
            var result = validator.Validate(emailCampaign);

            // Assert
            Assert.False(result.Errors.Any(e => e.ErrorMessage == "GreetingString must not exceed 1,500 characters"));
        }

        [Fact]
        public void Name_Is_Blank_Generates_Error()
        {
            // Arrange
            var emailCampaign = new EmailCampaign
            {
                GreetingString = ""
            };

            // Act
            var result = validator.Validate(emailCampaign);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage == "Name is required"));
        }

        [Fact]
        public void Name_Is_Greater_Than_80_Characters_Generates_Error()
        {
            // Arrange         
            var emailCampaign = new EmailCampaign
            {
                Name = "thisisasuperduperinsanelylongNamethatnooneintheirrightmindwouldNametheirchildandwouldbecriminaliftheydid"
            };

            // Act
            var result = validator.Validate(emailCampaign);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage == "Name must not exceed 80 characters"));
        }

        [Fact]
        public void Permission_Reminder_Text_Greater_Than_1500_Characters_Generates_Error()
        {
            // Arrange
            // This is stupid...  Yep thats 1,501 characters of stupid
            var emailCampaign = new EmailCampaign
            {
                PermissionReminderText = "stupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupidstupids"
            };

            // Act
            var result = validator.Validate(emailCampaign);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage == "PermissionReminderText must not exceed 1,500 characters"));
        }

        [Fact]
        public void Permission_Reminder_Text_Is_Blank_Is_Valid()
        {
            // Arrange
            var emailCampaign = new EmailCampaign
            {
                PermissionReminderText = ""
            };

            // Act
            var result = validator.Validate(emailCampaign);

            // Assert
            Assert.False(result.Errors.Any(e => e.ErrorMessage == "PermissionReminderText must not exceed 1,500 characters"));
        }

        [Fact]
        public void Permission_Reminder_Text_Is_Blank_When_IsPermissionReminderEnabled_Generates_Error()
        {
            // Arrange
            var emailCampaign = new EmailCampaign
            {
                PermissionReminderText = "",
                IsPermissionReminderEnabled = true
            };

            // Act
            var result = validator.Validate(emailCampaign);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage == "PermissionReminderText is required when IsPermissionReminderEnabled is true"));
        }

        [Fact]
        public void Permission_Reminder_Text_Is_Not_Blank_When_IsPermissionReminderEnabled_Is_Valid()
        {
            // Arrange
            var emailCampaign = new EmailCampaign
            {
                PermissionReminderText = "This is permission reminder text",
                IsPermissionReminderEnabled = true
            };

            // Act
            var result = validator.Validate(emailCampaign);

            // Assert
            Assert.False(result.Errors.Any(e => e.ErrorMessage == "PermissionReminderText is required when IsPermissionReminderEnabled is true"));
        }

        [Fact]
        public void ReplyToEmail_Is_Blank_Generates_Error()
        {
            // Arrange
            var emailCampaign = new EmailCampaign
            {
                ReplyToEmail = ""
            };

            // Act
            var result = validator.Validate(emailCampaign);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage == "ReplyToEmail is required"));
        }

        [Fact]
        public void ReplyToEmail_Is_Greater_Than_80_Characters_Generates_Error()
        {
            // Arrange
            var emailCampaign = new EmailCampaign
            {
                ReplyToEmail = "thisisasuperduperinsanelylongemailthatnooneintheirrightmindwouldmake@reallystupidemail.com"
            };

            // Act
            var result = validator.Validate(emailCampaign);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage == "ReplyToEmail must not exceed 80 characters"));
        }

        public void Style_Sheet_Set_When_EmailContentFormat_Is_XHTML_Is_Valid()
        {
            // Arrange
            var emailCampaign = new EmailCampaign
            {
                StyleSheet = "somestylesheet.css",
                EmailContentFormat = EmailContentFormat.Xhtml
            };

            // Act
            var result = validator.Validate(emailCampaign);

            // Assert
            Assert.False(result.Errors.Any(e => e.ErrorMessage == "StyleSheet must be empty if the EmailContentFormat is not XHTML"));
        }

        [Fact]
        public void Style_Sheet_Set_When_EmailContentFormat_Is_HTML_Generates_Error()
        {
            // Arrange
            var emailCampaign = new EmailCampaign
            {
                StyleSheet = "somestylesheet.css",
                EmailContentFormat = EmailContentFormat.Html
            };

            // Act
            var result = validator.Validate(emailCampaign);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage == "StyleSheet must be empty if the EmailContentFormat is not XHTML"));
        }

        [Fact]
        public void Style_Sheet_Is_Null_When_EmailContentFormat_Is_HTML_Is_Valid()
        {
            // Arrange
            var emailCampaign = new EmailCampaign
            {
                EmailContentFormat = EmailContentFormat.Html
            };

            // Act
            var result = validator.Validate(emailCampaign);

            // Assert
            Assert.False(result.Errors.Any(e => e.ErrorMessage == "StyleSheet must be empty if the EmailContentFormat is not XHTML"));
        }

        [Fact]
        public void Style_Sheet_Set_When_Template_Type_Is_TEMPLATE_V2_Generates_Error()
        {
            // Arrange
            var emailCampaign = new EmailCampaign
            {
                StyleSheet = "somestylesheet.css",
                TemplateType = TemplateType.TemplateV2
            };

            // Act
            var result = validator.Validate(emailCampaign);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage == "StyleSheet cannot be set when TemplateType is set to TEMPLATE_V2"));
        }

        [Fact]
        public void Style_Sheet_Is_Null_When_Template_Type_Is_TEMPLATE_V2_Is_Valid()
        {
            // Arrange
            var emailCampaign = new EmailCampaign
            {
                TemplateType = TemplateType.TemplateV2
            };

            // Act
            var result = validator.Validate(emailCampaign);

            // Assert
            Assert.False(result.Errors.Any(e => e.ErrorMessage == "StyleSheet cannot be set when TemplateType is set to TEMPLATE_V2"));
        }

        [Fact]
        public void Style_Sheet_Set_When_Template_Type_Is_Custom_Is_Valid()
        {
            // Arrange
            var emailCampaign = new EmailCampaign
            {
                StyleSheet = "somestylesheet.css",
                TemplateType = TemplateType.Custom
            };

            // Act
            var result = validator.Validate(emailCampaign);

            // Assert
            Assert.False(result.Errors.Any(e => e.ErrorMessage == "StyleSheet cannot be set when TemplateType is set to TEMPLATE_V2"));
        }

        [Fact]
        public void Style_Sheet_Not_Set_When_Template_Type_Is_Custom_Is_Valid()
        {
            // Arrange
            var emailCampaign = new EmailCampaign
            {
                StyleSheet = "",
                TemplateType = TemplateType.Custom
            };

            // Act
            var result = validator.Validate(emailCampaign);

            // Assert
            Assert.False(result.Errors.Any(e => e.ErrorMessage == "StyleSheet cannot be set when TemplateType is set to TEMPLATE_V2"));
        }

        [Fact]
        public void Subject_Is_Empty_Generates_Error()
        {
            // Arrange
            var emailCampaign = new EmailCampaign
            {
                Subject = ""
            };

            // Act
            var result = validator.Validate(emailCampaign);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage == "Subject is required"));
        }

        [Fact]
        public void Subject_Is_Greater_Than_200_Characters_Generates_Error()
        {
            // Arrange
            var emailCampaign = new EmailCampaign
            {
                Subject = "really super long Subject that no sane person would even include in their email but this person did and so this Subject is now invalid according to the rules of the constant contact api for generating email campaigns"
            };

            // Act
            var result = validator.Validate(emailCampaign);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage == "Subject must not exceed 200 characters"));
        }

        [Fact]
        public void TextContent_Is_Null_Generates_Error()
        {
            // Arrange
            var emailCampaign = new EmailCampaign
            {
                EmailContent = ""
            };

            // Act
            var result = validator.Validate(emailCampaign);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage == "TextContent is required"));
        }

        [Fact]
        public void TextContent_Length_Is_Greater_Than_930000_Characters_Generates_Error()
        {
            // Arrange            
            StringBuilder textContent = new StringBuilder();
            for (int i = 0; i < 930002; i++)
                textContent.Append("k");

            var emailCampaign = new EmailCampaign
            {
                TextContent = textContent.ToString()
            };

            // Act
            var result = validator.Validate(emailCampaign);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage == "TextContent must not exceed 930,000 characters"));
        }

        [Fact]
        public void TextContent_Is_Not_Empty_When_Email_Template_Type_Is_STOCK_Generates_Error()
        {
            // Arrange
            var emailCampaign = new EmailCampaign
            {
                TextContent = "<html><body>Test email</body></html>",
                TemplateType = TemplateType.Stock
            };

            // Act
            var result = validator.Validate(emailCampaign);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage == "TextContent cannot be set when TemplateType is set to STOCK or TEMPLATE_V2"));
        }

        [Fact]
        public void TextContent_Is_Not_Empty_When_Email_Template_Type_Is_TEMPLATE_V2_Generates_Error()
        {
            // Arrange
            var emailCampaign = new EmailCampaign
            {
                TextContent = "<html><body>Test email</body></html>",
                TemplateType = TemplateType.TemplateV2
            };

            // Act
            var result = validator.Validate(emailCampaign);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage == "TextContent cannot be set when TemplateType is set to STOCK or TEMPLATE_V2"));
        }

        [Fact]
        public void TextContent_Is_Not_Empty_When_Email_Template_Type_Is_CUSTOM_Is_Valid()
        {
            // Arrange
            var emailCampaign = new EmailCampaign
            {
                TextContent = "<html><body>Test email</body></html>",
                TemplateType = TemplateType.Custom
            };

            // Act
            var result = validator.Validate(emailCampaign);

            // Assert
            Assert.False(result.Errors.Any(e => e.ErrorMessage == "TextContent cannot be set when TemplateType is set to STOCK or TEMPLATE_V2"));
        }

        [Fact]
        public void TextContent_Is_Null_When_Email_Template_Type_Is_Valid()
        {
            // Arrange
            var emailCampaign = new EmailCampaign
            {
                TemplateType = TemplateType.Stock
            };

            // Act
            var result = validator.Validate(emailCampaign);

            // Assert
            Assert.False(result.Errors.Any(e => e.ErrorMessage == "TextContent cannot be set when TemplateType is set to STOCK or TEMPLATE_V2"));
        }

        [Fact]
        public void TextContent_Is_Null_When_Email_Template_Type_Is_TEMPLATE_V2_Is_Valid()
        {
            // Arrange
            var emailCampaign = new EmailCampaign
            {
                TemplateType = TemplateType.TemplateV2
            };

            // Act
            var result = validator.Validate(emailCampaign);

            // Assert
            Assert.False(result.Errors.Any(e => e.ErrorMessage == "TextContent cannot be set when TemplateType is set to STOCK or TEMPLATE_V2"));
        }

        [Fact]
        public void TextContent_Does_Not_Contain_Text_Tags_When_EmailContentFormat_Is_XHTML_Generates_Error()
        {
            // Arrange
            var emailCampaign = new EmailCampaign
            {
                EmailContentFormat = EmailContentFormat.Xhtml,
                TextContent = "this is text content"
            };

            // Act
            var result = validator.Validate(emailCampaign);

            Assert.True(result.Errors.Any(e => e.ErrorMessage == "TextContent must be wrapped in a <text> tag when EmailContentFormat is XHTML"));
        }

        [Fact]
        public void TextContent_Does_Not_Contain_Closing_Text_Tag_When_EmailContentFormat_Is_XHTML_Generates_Error()
        {
            // Arrange
            var emailCampaign = new EmailCampaign
            {
                EmailContentFormat = EmailContentFormat.Xhtml,
                TextContent = "<text>his is text content"
            };

            // Act
            var result = validator.Validate(emailCampaign);

            Assert.True(result.Errors.Any(e => e.ErrorMessage == "TextContent must be wrapped in a <text> tag when EmailContentFormat is XHTML"));
        }

        [Fact]
        public void TextContent_Contains_Text_Tags_When_EmailContentFormat_Is_XHTML_Is_Valid()
        {
            // Arrange
            var emailCampaign = new EmailCampaign
            {
                EmailContentFormat = EmailContentFormat.Xhtml,
                TextContent = "<text>his is text content</text>"
            };

            // Act
            var result = validator.Validate(emailCampaign);

            Assert.False(result.Errors.Any(e => e.ErrorMessage == "TextContent must be wrapped in a <text> tag when EmailContentFormat is XHTML"));
        }

        [Fact]
        public void ViewAsWebPageLinkText_Is_Greater_Than_50_Characters_Generates_Error()
        {
            // Arrange
            var emailCampaign = new EmailCampaign
            {
                ViewAsWebPageLinkText = "this view as a web page link text exceeds fifty characters"
            };

            // Act
            var result = validator.Validate(emailCampaign);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage == "ViewAsWebPageLinkText must not exceed 50 characters"));
        }

        [Fact]
        public void ViewAsWebPageLinkText_Is_Empty_Is_Valid()
        {
            // Arrange
            var emailCampaign = new EmailCampaign
            {
                ViewAsWebPageLinkText = ""
            };

            // Act
            var result = validator.Validate(emailCampaign);

            // Assert
            Assert.False(result.Errors.Any(e => e.ErrorMessage == "ViewAsWebPageLinkText must not exceed 50 characters"));
        }

        [Fact]
        public void ViewAsWebPageLinkText_Is_Null_When_IsViewAsWebpageEnabled_Generates_Error()
        {
            // Arrange
            var emailCampaign = new EmailCampaign
            {
                IsViewAsWebpageEnabled = true,
            };

            // Act
            var result = validator.Validate(emailCampaign);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage == "ViewAsWebPageLinkText is required when IsViewAsWebpageEnabled is set to true"));
        }

        [Fact]
        public void ViewAsWebPageLinkText_Is_Not_Empty_When_IsViewAsWebpageEnabled_Is_Valid()
        {
            // Arrange
            var emailCampaign = new EmailCampaign
            {
                IsViewAsWebpageEnabled = true,
                ViewAsWebPageLinkText = "Click Here to view this page as a webpage"
            };

            // Act
            var result = validator.Validate(emailCampaign);

            // Assert
            Assert.False(result.Errors.Any(e => e.ErrorMessage == "ViewAsWebPageLinkText is required when IsViewAsWebpageEnabled is set to true"));
        }

        [Fact]
        public void ViewAsWebPageText_Is_Greater_Than_50_Characters_Generates_Error()
        {
            // Arrange
            var emailCampaign = new EmailCampaign
            {
                ViewAsWebPageText = "this view as a web page text exceeds fifty characters"
            };

            // Act
            var result = validator.Validate(emailCampaign);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage == "ViewAsWebPageText must not exceed 50 characters"));
        }

        [Fact]
        public void ViewAsWebPageText_Is_Empty_Is_Valid()
        {
            // Arrange
            var emailCampaign = new EmailCampaign
            {
                ViewAsWebPageText = ""
            };

            // Act
            var result = validator.Validate(emailCampaign);

            // Assert
            Assert.False(result.Errors.Any(e => e.ErrorMessage == "ViewAsWebPageText must not exceed 50 characters"));
        }

        [Fact]
        public void ViewAsWebPageText_Is_Null_When_IsViewAsWebpageEnabled_Generates_Error()
        {
            // Arrange
            var emailCampaign = new EmailCampaign
            {
                IsViewAsWebpageEnabled = true
            };

            // Act
            var result = validator.Validate(emailCampaign);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage == "ViewAsWebPageText is required when IsViewAsWebpageEnabled is set to true"));
        }

        [Fact]
        public void ViewAsWebPageText_Is_Not_Empty_When_IsViewAsWebpageEnabled_Is_Valid()
        {
            // Arrange
            var emailCampaign = new EmailCampaign
            {
                IsViewAsWebpageEnabled = true,
                ViewAsWebPageText = "Click Here to view this page as a webpage"
            };

            // Act
            var result = validator.Validate(emailCampaign);

            // Assert
            Assert.False(result.Errors.Any(e => e.ErrorMessage == "ViewAsWebPageText is required when IsViewAsWebpageEnabled is set to true"));
        }
    }
}