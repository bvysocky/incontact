﻿#region License
// The MIT License (MIT)
//
// Copyright (c) 2015 Scott Lance, Ethan Tipton
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// 
// The most recent version of this license can be found at: http://opensource.org/licenses/MIT
#endregion

using InContactSdk.EmailCampaignScheduleService;
using InContactSdk.EmailCampaignScheduling;
using InContactSdk.Helpers;
using InContactSdk.Models;
using InContactSdk.Objects;
using InContactSdk.Validation;
using Moq;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Net.Http;
using System.Threading.Tasks;
using Xunit;

namespace InContactTests.EmailCampaignSchedulingTests
{
    [ExcludeFromCodeCoverage]
    public class EmailCampaignSchedulingServiceTests
    {
        private EmailCampaignSchedulingService emailCampaignSchedulingService = null;
        private IInContactSettings settings = null;
        private Mock<IValidationService> validationService = null;
        private Mock<ISerialization> serialization = null;
        private Mock<IWebServiceRequest> webServiceRequest = null;

        public EmailCampaignSchedulingServiceTests()
        {
            settings = new InContactSettings("123456789", "D27D92A2-6A0B-4F33-BE8A-3C63E597AB18");
            validationService = new Mock<IValidationService>();
            serialization = new Mock<ISerialization>();
            webServiceRequest = new Mock<IWebServiceRequest>();
        }

        public void InitializeEmailCampaignSchedulingService<T>()
            where T : class, new()
        {
            validationService.Setup(m => m.Validate(It.IsAny<IValidatable>()));
            serialization.Setup(m => m.Serialize(It.IsAny<object>())).Returns("");
            serialization.Setup(m => m.Deserialize<T>(It.IsAny<string>())).Returns(new T());
            emailCampaignSchedulingService = new EmailCampaignSchedulingService(settings, validationService.Object, webServiceRequest.Object);
        }

        // *************************************************************************************************      

        [Fact]
        public async Task GetEmailCampaignSchedulesAsync_Uses_Correct_Endpoint()
        {
            // Arrange
            InitializeEmailCampaignSchedulingService<ScheduleResponse>();
            var campaignId = "123456";
            Uri endpoint = null;

            webServiceRequest.Setup(m => m.GetDeserializedAsync<IEnumerable<ScheduleResponse>>(It.IsAny<Uri>(), It.IsAny<string>()))
                .Callback((Uri a, string b) => endpoint = a)
                .ReturnsAsync(new List<ScheduleResponse>());

            // Act
            await emailCampaignSchedulingService.GetEmailCampaignSchedulesAsync(campaignId);

            // Assert
            Assert.Equal(string.Format("{0}emailmarketing/campaigns/{1}/schedules?api_key={2}", settings.BaseUrl, campaignId, settings.ApiKey), endpoint.AbsoluteUri);
        }

        [Fact]
        public async Task GetEmailCampaignSchedulesAsync_CampaignId_Is_Not_NullOrWhiteSpace_Verify_MakeSerializedRequestAsync()
        {
            // Arrange
            InitializeEmailCampaignSchedulingService<ScheduleResponse>();
            var campaignId = "123456";

            // Act
            await emailCampaignSchedulingService.GetEmailCampaignSchedulesAsync(campaignId);

            // Assert
            webServiceRequest.Verify(m => m.GetDeserializedAsync<IEnumerable<ScheduleResponse>>(It.IsAny<Uri>(), It.IsAny<string>()), Times.Once());
        }

        // *************************************************************************************************
        
        [Fact]
        public async Task CreateEmailCampaignsScheduleAsync_Uses_Correct_Endpoint()
        {
            // Arrange
            InitializeEmailCampaignSchedulingService<ScheduleResponse>();
            var campaignId = "123456";
            var scheduledDate = DateTime.Now.AddDays(1);
            Uri endpoint = null;

            webServiceRequest.Setup(m => m.PostDeserializedAsync<ScheduledDate, ScheduleResponse>(It.IsAny<Uri>(), It.IsAny<string>(), It.IsAny<ScheduledDate>()))
                .Callback((Uri a, string b, ScheduledDate c) => endpoint = a)
                .ReturnsAsync(new ScheduleResponse());

            // Act
            await emailCampaignSchedulingService.CreateEmailCampaignScheduleAsync(campaignId, scheduledDate);

            // Assert
            Assert.Equal(string.Format("{0}emailmarketing/campaigns/{1}/schedules?api_key={2}", settings.BaseUrl, campaignId, settings.ApiKey), endpoint.AbsoluteUri);
        }

        [Fact]
        public async Task CreateEmailCampaignScheduleAsync_CampaignId_Is_Not_NullOrWhiteSpace_Verify_MakeSerializedRequestAsync()
        {
            // Arrange
            InitializeEmailCampaignSchedulingService<ScheduleResponse>();
            var campaignId = "123456";
            var scheduledDate = DateTime.Now.AddDays(1);

            // Act
            await emailCampaignSchedulingService.CreateEmailCampaignScheduleAsync(campaignId, scheduledDate);

            // Assert
            webServiceRequest.Verify(m => m.PostDeserializedAsync<ScheduledDate, ScheduleResponse>(It.IsAny<Uri>(), It.IsAny<string>(), It.IsAny<ScheduledDate>()), Times.Once());
        }

        // *************************************************************************************************

        [Fact]
        public async Task GetEmailCampaignScheduleAsync_Uses_Correct_Endpoint()
        {
            // Arrange
            InitializeEmailCampaignSchedulingService<ScheduleResponse>();
            var campaignId = "123456";
            var scheduleId = "123456";
            Uri endpoint = null;

            webServiceRequest.Setup(m => m.GetDeserializedAsync<ScheduleResponse>(It.IsAny<Uri>(), It.IsAny<string>()))
                .Callback((Uri a, string b) => endpoint = a)
                .ReturnsAsync(new ScheduleResponse());

            // Act
            await emailCampaignSchedulingService.GetEmailCampaignScheduleAsync(campaignId, scheduleId);

            // Assert
            Assert.Equal(string.Format("{0}emailmarketing/campaigns/{1}/schedules/{2}?api_key={3}", settings.BaseUrl, campaignId, scheduleId, settings.ApiKey), endpoint.AbsoluteUri);
        }

        [Fact]
        public async Task GetEmailCampaignScheduleAsync_Is_Valid_Verify_MakeSerializedRequestAsync()
        {
            // Arrange
            InitializeEmailCampaignSchedulingService<ScheduleResponse>();
            var campaignId = "123456";
            var scheduleId = "123456";

            // Act
            await emailCampaignSchedulingService.GetEmailCampaignScheduleAsync(campaignId, scheduleId);

            // Assert
            webServiceRequest.Verify(m => m.GetDeserializedAsync<ScheduleResponse>(It.IsAny<Uri>(), It.IsAny<string>()), Times.Once());
        }

        // *************************************************************************************************             

        [Fact]
        public async Task UpdateEmailCampaignScheduleAsync_Uses_Correct_Endpoint()
        {
            // Arrange
            InitializeEmailCampaignSchedulingService<ScheduleResponse>();
            var campaignId = "123456";
            var scheduleId = "123456";
            var scheduledDate = DateTime.Now.AddDays(1);
            Uri endpoint = null;

            webServiceRequest.Setup(m => m.PutDeserializedAsync<ScheduledDate, ScheduleResponse>(It.IsAny<Uri>(), It.IsAny<string>(), It.IsAny<ScheduledDate>()))
                .Callback((Uri a, string b, ScheduledDate c) => endpoint = a)
                .ReturnsAsync(new ScheduleResponse());

            // Act
            await emailCampaignSchedulingService.UpdateEmailCampaignScheduleAsync(campaignId, scheduleId, scheduledDate);

            // Assert
            Assert.Equal(string.Format("{0}emailmarketing/campaigns/{1}/schedules/{2}?api_key={3}", settings.BaseUrl, campaignId, scheduleId, settings.ApiKey), endpoint.AbsoluteUri);
        }

        [Fact]
        public async Task UpdateEmailCampaignScheduleAsync_Is_Valid_Verify_MakeSerializedRequestAsync()
        {
            // Arrange
            InitializeEmailCampaignSchedulingService<ScheduleResponse>();
            var campaignId = "123456";
            var scheduleId = "123456";
            var scheduledDate = DateTime.Now.AddDays(1);

            // Act
            await emailCampaignSchedulingService.UpdateEmailCampaignScheduleAsync(campaignId, scheduleId, scheduledDate);

            // Assert
            webServiceRequest.Verify(m => m.PutDeserializedAsync<ScheduledDate, ScheduleResponse>(It.IsAny<Uri>(), It.IsAny<string>(), It.IsAny<ScheduledDate>()), Times.Once());
        }

        // *************************************************************************************************       

        [Fact]
        public async Task DeleteEmailCampaignScheduleAsync_Uses_Correct_Endpoint()
        {
            // Arrange
            InitializeEmailCampaignSchedulingService<ScheduleResponse>();
            var campaignId = "123456";
            var scheduleId = "123456";
            Uri endpoint = null;

            webServiceRequest.Setup(m => m.DeleteAsync(It.IsAny<Uri>(), It.IsAny<string>()))
                .Callback((Uri a, string b) => endpoint = a)
                .ReturnsAsync(new HttpResponseMessage());

            // Act
            await emailCampaignSchedulingService.DeleteEmailCampaignScheduleAsync(campaignId, scheduleId);

            // Assert
            Assert.Equal(string.Format("{0}emailmarketing/campaigns/{1}/schedules/{2}?api_key={3}", settings.BaseUrl, campaignId, scheduleId, settings.ApiKey), endpoint.AbsoluteUri);
        }

        [Fact]
        public async Task DeleteEmailCampaignScheduleAsync_Is_Valid_Verify_MakeRequestAsync()
        {
            // Arrange
            InitializeEmailCampaignSchedulingService<ScheduleResponse>();
            var campaignId = "123456";
            var scheduleId = "123456";

            // Act
            await emailCampaignSchedulingService.DeleteEmailCampaignScheduleAsync(campaignId, scheduleId);

            // Assert
            webServiceRequest.Verify(m => m.DeleteAsync(It.IsAny<Uri>(), It.IsAny<string>()), Times.Once());
        }
    }
}