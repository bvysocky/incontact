﻿#region License
// The MIT License (MIT)
//
// Copyright (c) 2015 Scott Lance, Ethan Tipton
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// 
// The most recent version of this license can be found at: http://opensource.org/licenses/MIT
#endregion

using InContactSdk.Contacts;
using InContactSdk.Exceptions;
using InContactSdk.Helpers;
using InContactSdk.Models;
using InContactSdk.Validation;
using Moq;
using System;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Xunit;

namespace InContactTests.ContactsTests
{
    [ExcludeFromCodeCoverage]
    public class ContactsServiceTests
    {
        private ContactsService contactsService = null;
        private IInContactSettings settings = null;
        private Mock<IValidationService> validationService = null;
        private Mock<ISerialization> serialization = null;
        private Mock<IWebServiceRequest> webServiceRequest = null;

        public ContactsServiceTests()
        {
            settings = new InContactSettings("123456789", "D27D92A2-6A0B-4F33-BE8A-3C63E597AB18");
            validationService = new Mock<IValidationService>();
            serialization = new Mock<ISerialization>();
            webServiceRequest = new Mock<IWebServiceRequest>();
        }

        public void InitializeContactService<T>()
            where T : class, new()
        {
            validationService.Setup(m => m.Validate(It.IsAny<IValidatable>()));
            serialization.Setup(m => m.Serialize(It.IsAny<object>())).Returns("");
            serialization.Setup(m => m.Deserialize<T>(It.IsAny<string>())).Returns(new T());
            contactsService = new ContactsService(settings, validationService.Object, webServiceRequest.Object);
        }

        // ************************************************************************************************

        [Fact]
        public async Task GetContact_Uses_Correct_Endpoint()
        {
            // Arrange
            InitializeContactService<Contact>();
            string contactId = "1234567";

            Uri endpoint = null;

            webServiceRequest.Setup(m => m.GetDeserializedAsync<Contact>(It.IsAny<Uri>(), It.IsAny<string>()))
                .Callback((Uri a, string b) => endpoint = a)
                .ReturnsAsync(new Contact());

            // Act
            await contactsService.GetContactAsync(contactId);

            // Assert
            Assert.Equal(string.Format("{0}contacts/{1}?api_key={2}", settings.BaseUrl, contactId, settings.ApiKey), endpoint.AbsoluteUri);

        }

        [Fact]
        public async Task GetContact_Verify_WebServiceRequest_MakeRequest()
        {
            // Arrange
            InitializeContactService<Contact>();
            string contactId = "1234567";

            // Act
            await contactsService.GetContactAsync(contactId);

            // Assert
            webServiceRequest.Verify(m => m.GetDeserializedAsync<Contact>(It.IsAny<Uri>(), It.IsAny<string>()), Times.Once);
        }

        // *************************************************************************************************       

        [Fact]
        public async Task CreateContactAsync_ActionBy_None_Uses_Correct_Endpoint()
        {
            // Arrange
            InitializeContactService<Contact>();
            var actionBy = ActionBy.None;
            var contact = new Contact()
            {
                Addresses = new Address[]
                {
                    new Address
                    {
                        AddressType = ContactAddressType.Business,
                        City = "Pasadena",
                        CountryCode = CountryCode.US,
                        Line1 = "California Institute of Technology",                            
                        PostalCode = "91125",
                        State = "CA"
                    }                    
                },
                CellPhone = "6263956811",
                CompanyName = "California Institute of Technology",
                CustomFields = new CustomField[]
                {
                    new CustomField { Name = CustomFieldValue.CustomField1, Value = "Physics"}
                },
                EmailAddresses = new ContactEmailAddress[]
                {
                    new ContactEmailAddress 
                    { 
                        EmailAddress = "scooper@caltech.edu",                          
                    }
                },
                Fax = "6263956812",
                FirstName = "Sheldon",
                HomePhone = "6263956813",
                JobTitle = "Theoretical Physicist",
                LastName = "Cooper",
                Lists = new List[]
                {
                    new List { Id = "1" }
                },
                Notes = new ContactNote[]
                {
                    new ContactNote { Note = "Not insane.  His mother had him tested"}
                },
                PrefixName = "Mr."
            };

            Uri endpoint = null;

            webServiceRequest.Setup(m => m.PostDeserializedAsync<Contact, Contact>(It.IsAny<Uri>(), It.IsAny<string>(), It.IsAny<Contact>()))
                .Callback((Uri a, string b, Contact c) => endpoint = a)
                .ReturnsAsync(new Contact());

            // Act
            await contactsService.CreateContactAsync(contact, actionBy);

            // Assert
            Assert.Equal(string.Format("{0}contacts?api_key={1}", settings.BaseUrl, settings.ApiKey), endpoint.AbsoluteUri);
        }

        [Fact]
        public async Task CreateContactAsync_ActionBy_ActionByOwner_Uses_Correct_Endpoint()
        {
            // Arrange
            InitializeContactService<Contact>();
            var actionBy = ActionBy.ActionByOwner;
            var contact = new Contact()
            {
                Addresses = new Address[]
                {
                    new Address
                    {
                        AddressType = ContactAddressType.Business,
                        City = "Pasadena",
                        CountryCode = CountryCode.US,
                        Line1 = "California Institute of Technology",                            
                        PostalCode = "91125",
                        State = "CA"
                    }                    
                },
                CellPhone = "6263956811",
                CompanyName = "California Institute of Technology",
                CustomFields = new CustomField[]
                {
                    new CustomField { Name = CustomFieldValue.CustomField1, Value = "Physics"}
                },
                EmailAddresses = new ContactEmailAddress[]
                {
                    new ContactEmailAddress 
                    { 
                        EmailAddress = "scooper@caltech.edu",                          
                    }
                },
                Fax = "6263956812",
                FirstName = "Sheldon",
                HomePhone = "6263956813",
                JobTitle = "Theoretical Physicist",
                LastName = "Cooper",
                Lists = new List[]
                {
                    new List { Id = "1" }
                },
                Notes = new ContactNote[]
                {
                    new ContactNote { Note = "Not insane.  His mother had him tested"}
                },
                PrefixName = "Mr."
            };

            Uri endpoint = null;

            webServiceRequest.Setup(m => m.PostDeserializedAsync<Contact, Contact>(It.IsAny<Uri>(), It.IsAny<string>(), It.IsAny<Contact>()))
                .Callback((Uri a, string b, Contact c) => endpoint = a)
                .ReturnsAsync(new Contact());

            // Act
            await contactsService.CreateContactAsync(contact, actionBy);

            // Assert
            Assert.Equal(string.Format("{0}contacts?api_key={1}&action_by=ACTION_BY_OWNER", settings.BaseUrl, settings.ApiKey), endpoint.AbsoluteUri);
        }

        [Fact]
        public async Task CreateContactAsync_Contact_Is_Valid_Verify_WebServiceRequest_MakeRequest()
        {
            // Arrange
            InitializeContactService<Contact>();
            var actionBy = ActionBy.ActionByOwner;
            var contact = new Contact()
            {
                Addresses = new Address[]
                {
                    new Address
                    {
                        AddressType = ContactAddressType.Business,
                        City = "Pasadena",
                        CountryCode = CountryCode.US,
                        Line1 = "California Institute of Technology",                            
                        PostalCode = "91125",
                        State = "CA"
                    }                    
                },
                CellPhone = "6263956811",
                CompanyName = "California Institute of Technology",
                CustomFields = new CustomField[]
                {
                    new CustomField { Name = CustomFieldValue.CustomField1, Value = "Physics"}
                },
                EmailAddresses = new ContactEmailAddress[]
                {
                    new ContactEmailAddress 
                    { 
                        EmailAddress = "scooper@caltech.edu",                          
                    }
                },
                Fax = "6263956812",
                FirstName = "Sheldon",
                HomePhone = "6263956813",
                JobTitle = "Theoretical Physicist",
                LastName = "Cooper",
                Lists = new List[]
                {
                    new List { Id = "1" }
                },
                Notes = new ContactNote[]
                {
                    new ContactNote { Note = "Not insane.  His mother had him tested"}
                },
                PrefixName = "Mr."
            };

            // Act
            await contactsService.CreateContactAsync(contact, actionBy);

            // Assert
            webServiceRequest.Verify(m => m.PostDeserializedAsync<Contact, Contact>(It.IsAny<Uri>(), It.IsAny<string>(), It.IsAny<Contact>()), Times.Once);
        }

        // *************************************************************************************************

        [Fact]
        public async Task UpdateContact_ActionBy_None_Uses_Correct_Endpoint()
        {
             // Arrange
            InitializeContactService<Contact>();
            var actionBy = ActionBy.None;
            var contactId = "1";
            var contact = new Contact()
            {
                Addresses = new Address[]
                {
                    new Address
                    {
                        AddressType = ContactAddressType.Business,
                        City = "Pasadena",
                        CountryCode = CountryCode.US,
                        Line1 = "California Institute of Technology",                            
                        PostalCode = "91125",
                        State = "CA"
                    }                    
                },
                CellPhone = "6263956811",
                CompanyName = "California Institute of Technology",
                CustomFields = new CustomField[]
                {
                    new CustomField { Name = CustomFieldValue.CustomField1, Value = "Physics"}
                },
                EmailAddresses = new ContactEmailAddress[]
                {
                    new ContactEmailAddress 
                    { 
                        EmailAddress = "scooper@caltech.edu",                          
                    }
                },
                Fax = "6263956812",
                FirstName = "Sheldon",
                HomePhone = "6263956813",
                JobTitle = "Theoretical Physicist",
                LastName = "Cooper",
                Lists = new List[]
                {
                    new List { Id = "1" }
                },
                Notes = new ContactNote[]
                {
                    new ContactNote { Note = "Not insane.  His mother had him tested"}
                },
                PrefixName = "Mr."
            };
         
            Uri endpoint = null;

            webServiceRequest.Setup(m => m.PutDeserializedAsync<Contact, Contact>(It.IsAny<Uri>(), It.IsAny<string>(), It.IsAny<Contact>()))
                .Callback((Uri a, string b, Contact c) => endpoint = a)
                .ReturnsAsync(new Contact());

            // Act
            await contactsService.UpdateContactAsync(contact, contactId, actionBy);

            // Assert
            Assert.Equal(string.Format("{0}contacts/{1}?api_key={2}", settings.BaseUrl, contactId, settings.ApiKey), endpoint.AbsoluteUri);
        }

        [Fact]
        public async Task UpdateContact_ActionBy_ActionByOwner_Uses_Correct_Endpoint()
        {
             // Arrange
            InitializeContactService<Contact>();
            var actionBy = ActionBy.ActionByOwner;
            var contactId = "1";
            var contact = new Contact()
            {
                Addresses = new Address[]
                {
                    new Address
                    {
                        AddressType = ContactAddressType.Business,
                        City = "Pasadena",
                        CountryCode = CountryCode.US,
                        Line1 = "California Institute of Technology",                            
                        PostalCode = "91125",
                        State = "CA"
                    }                    
                },
                CellPhone = "6263956811",
                CompanyName = "California Institute of Technology",
                CustomFields = new CustomField[]
                {
                    new CustomField { Name = CustomFieldValue.CustomField1, Value = "Physics"}
                },
                EmailAddresses = new ContactEmailAddress[]
                {
                    new ContactEmailAddress 
                    { 
                        EmailAddress = "scooper@caltech.edu",                          
                    }
                },
                Fax = "6263956812",
                FirstName = "Sheldon",
                HomePhone = "6263956813",
                JobTitle = "Theoretical Physicist",
                LastName = "Cooper",
                Lists = new List[]
                {
                    new List { Id = "1" }
                },
                Notes = new ContactNote[]
                {
                    new ContactNote { Note = "Not insane.  His mother had him tested"}
                },
                PrefixName = "Mr."
            };
         
            Uri endpoint = null;

            webServiceRequest.Setup(m => m.PutDeserializedAsync<Contact, Contact>(It.IsAny<Uri>(), It.IsAny<string>(), It.IsAny<Contact>()))
                .Callback((Uri a, string b, Contact c) => endpoint = a)
                .ReturnsAsync(new Contact());

            // Act
            await contactsService.UpdateContactAsync(contact, contactId, actionBy);

            // Assert
            Assert.Equal(string.Format("{0}contacts/{1}?api_key={2}&action_by=ACTION_BY_OWNER", settings.BaseUrl, contactId, settings.ApiKey), endpoint.AbsoluteUri);
        }

        [Fact]
        public async Task UpdateContact_Has_Error_Throws_InContactException()
        {
            // Arrange
            InitializeContactService<Contact>();
            var actionBy = ActionBy.None;
            var contactId = "1";
            var contact = new Contact
            {
                Addresses = new Address[] 
                { 
                    new Address { AddressType = ContactAddressType.Business },
                    new Address { AddressType = ContactAddressType.Business },                
                }
            };

            try
            {
                // Act
                await contactsService.UpdateContactAsync(contact, contactId, actionBy);
            }
            catch (InContactException ex)
            {
                // Assert    
                Assert.True(ex.ValidationErrors.Any(e => e.ErrorMessage == "Addresses may only contain one personal and one business address"));
            }
        }

        [Fact]
        public async Task UpdateContact_Contact_Is_Valid_Verify_WebServiceRequest_MakeRequest()
        {
            // Arrange
            InitializeContactService<Contact>();
            var actionBy = ActionBy.ActionByOwner;
            var contactId = "1";
            var contact = new Contact()
            {
                Addresses = new Address[]
                {
                    new Address
                    {
                        AddressType = ContactAddressType.Business,
                        City = "Pasadena",
                        CountryCode = CountryCode.US,
                        Line1 = "California Institute of Technology",                            
                        PostalCode = "91125",
                        State = "CA"
                    }                    
                },
                CellPhone = "6263956811",
                CompanyName = "California Institute of Technology",
                CustomFields = new CustomField[]
                {
                    new CustomField { Name = CustomFieldValue.CustomField1, Value = "Physics"}
                },
                EmailAddresses = new ContactEmailAddress[]
                {
                    new ContactEmailAddress 
                    { 
                        EmailAddress = "scooper@caltech.edu",                          
                    }
                },
                Fax = "6263956812",
                FirstName = "Sheldon",
                HomePhone = "6263956813",
                JobTitle = "Theoretical Physicist",
                LastName = "Cooper",
                Lists = new List[]
                {
                    new List { Id = "1" }
                },
                PrefixName = "Mr."
            };

            // Act
            await contactsService.UpdateContactAsync(contact, contactId, actionBy);

            // Assert
            webServiceRequest.Verify(m => m.PutDeserializedAsync<Contact, Contact>(It.IsAny<Uri>(), It.IsAny<string>(), It.IsAny<Contact>()), Times.Once);
        }

        // *************************************************************************************************    

        [Fact]
        public async Task DeleteContact_Uses_Correct_Endpoint()
        {
            // Arrange
            InitializeContactService<Contact>();
            string contactId = "1234567";

            Uri endpoint = null;

            webServiceRequest.Setup(m => m.DeleteAsync(It.IsAny<Uri>(), It.IsAny<string>()))
               .Callback((Uri a, string b) => endpoint = a)
               .ReturnsAsync(new HttpResponseMessage());

            // Act
            await contactsService.DeleteContactAsync(contactId);

            // Assert
            Assert.Equal(string.Format("{0}contacts/{1}?api_key={2}", settings.BaseUrl, contactId, settings.ApiKey), endpoint.AbsoluteUri);

        }

        [Fact]
        public async Task DeleteContactAsync_ContactId_Is_Empty_Verify_MakeRequest()
        {
            // Arrange                 
            InitializeContactService<Contact>();
            var contactId = "1";

            // Act
            await contactsService.DeleteContactAsync(contactId);

            // Assert
            webServiceRequest.Verify(m => m.DeleteAsync(It.IsAny<Uri>(), It.IsAny<string>()), Times.Once);
        }
    }
}