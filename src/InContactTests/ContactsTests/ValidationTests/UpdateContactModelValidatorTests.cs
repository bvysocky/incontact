﻿#region License
// The MIT License (MIT)
//
// Copyright (c) 2015 Scott Lance, Ethan Tipton
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// 
// The most recent version of this license can be found at: http://opensource.org/licenses/MIT
#endregion

using InContactSdk.Contacts;
using InContactSdk.Contacts.Models;
using InContactSdk.Contacts.Validation;
using InContactSdk.Helpers;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Xunit;

namespace InContactTests.ContactsTests.ValidationTests
{
    [ExcludeFromCodeCoverage]
    public class UpdateContactModelValidatorTests
    {
        private readonly UpdateContactModelValidator validator;

        public UpdateContactModelValidatorTests()
        {
            validator = new UpdateContactModelValidator();
        }

        // *************************************************************************************************

        [Fact]
        public void ContactId_Is_Null_Generates_Error()
        {
            // Arrange
            var contact = new Contact();
            var actionBy = ActionBy.None;
            var model = new UpdateContactModel { Contact = contact, ContactId = null, ActionBy = actionBy };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage == "ContactId is required"));

        }

        [Fact]
        public void ContactId_Is_Empty_Generates_Error()
        {
            // Arrange
            var contact = new Contact();
            var contactId = "";
            var actionBy = ActionBy.None;
            var model = new UpdateContactModel { Contact = contact, ContactId = contactId, ActionBy = actionBy };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage == "ContactId is required"));

        }

        // *************************************************************************************************

        [Fact]
        public void Contact_Notes_Is_Not_Null_Generates_Error()
        {
            // Arrange
            var contact = new Contact()
            {
                Notes = new ContactNote[0]
            };
            var contactId = "1";
            var actionBy = ActionBy.None;
            var model = new UpdateContactModel { Contact = contact, ContactId = contactId, ActionBy = actionBy };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage == "Notes cannot be included in the Contact Update"));
        }

        // *************************************************************************************************

        [Fact]
        public void Model_Is_Valid_Generates_No_Errors()
        {
            // Arrange
            var contact = new Contact();
            var contactId = "1";
            var actionBy = ActionBy.None;
            var model = new UpdateContactModel { Contact = contact, ContactId = contactId, ActionBy = actionBy };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.True(result.Errors.Count == 0);
        }
    }
}