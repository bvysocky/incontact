﻿#region License
// The MIT License (MIT)
//
// Copyright (c) 2015 Scott Lance, Ethan Tipton
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// 
// The most recent version of this license can be found at: http://opensource.org/licenses/MIT
#endregion

using InContactSdk.Contacts;
using InContactSdk.Contacts.Validation;
using InContactSdk.Helpers;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Xunit;

namespace InContactTests.ContactsTests.ValidationTests
{
    [ExcludeFromCodeCoverage]
    public class ContactValidatorTests
    {
        private readonly ContactValidator validator;

        public ContactValidatorTests()
        {
            validator = new ContactValidator();
        }

        // *************************************************************************************************

        [Fact]
        public void Addresses_Contains_More_Than_Two_Addresses_Generates_Error()
        {
            // Arrange
            var contact = new Contact()
            {
                Addresses = new Address[] 
                {
                    new Address {},                    
                    new Address {},
                    new Address {}
                },
            };

            // Act
            var result = validator.Validate(contact);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage == "Addresses array cannot contain more than 2 items"));
        }

        [Fact]
        public void Addresses_Contains_Two_Personal_Addressess_Generates_Error()
        {
            // Arrange
            var contact = new Contact()
            {
                Addresses = new Address[] 
                {
                    new Address { AddressType = ContactAddressType.Personal },                    
                    new Address { AddressType = ContactAddressType.Personal }                    
                },
            };

            // Act
            var result = validator.Validate(contact);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage == "Addresses may only contain one personal and one business address"));
        }

        [Fact]
        public void Addresses_Contains_Two_Business_Addressess_Generates_Error()
        {
            // Arrange
            var contact = new Contact()
            {
                Addresses = new Address[] 
                {
                    new Address { AddressType = ContactAddressType.Business },                    
                    new Address { AddressType = ContactAddressType.Business }                    
                },
            };

            // Act
            var result = validator.Validate(contact);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage == "Addresses may only contain one personal and one business address"));
        }

        [Fact]
        public void Addresses_Contains_One_Business_Address_One_Personal_Address_Is_Valid()
        {
            // Arrange
            var contact = new Contact()
            {
                Addresses = new Address[] 
                {
                    new Address { AddressType = ContactAddressType.Personal },                    
                    new Address { AddressType = ContactAddressType.Business }                    
                },
            };

            // Act
            var result = validator.Validate(contact);

            // Assert
            Assert.False(result.Errors.Any(e => e.ErrorMessage == "Addresses array cannot contain more than 2 items" || e.ErrorMessage == "Addresses may only contain one personal and one business address"));
        }

        // *************************************************************************************************

        [Fact]
        public void CellPhone_Contains_More_Than_50_Characters_Generates_Error()
        {
            // Arrange
            var contact = new Contact()
            {
                CellPhone = "626395681100000000000000000000000000000000000000000"
            };

            // Act
            var result = validator.Validate(contact);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage == "CellPhone cannot contain more than 50 characters"));
        }

        // *************************************************************************************************

        [Fact]
        public void CompanyName_Contains_More_Than_50_Characters_Generates_Error()
        {
            // Arrange
            var contact = new Contact()
            {
                CompanyName = "California Technical Insitute of the State Of California"
            };

            // Act
            var result = validator.Validate(contact);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage == "CompanyName cannot contain more than 50 characters"));
        }

        // *************************************************************************************************

        [Fact]
        public void Custom_Fields_Contains_More_Than_15_Elements_Generates_Error()
        {
            // Arrange
            var contact = new Contact()
            {
                CustomFields = new CustomField[16]
            };

            // Act
            var result = validator.Validate(contact);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage == "CustomFields array cannot contain more than 15 items"));
        }

        // *************************************************************************************************

        [Fact]
        public void Email_Address_Is_Null_Generates_Error()
        {
            // Arrange
            var contact = new Contact();

            // Act
            var result = validator.Validate(contact);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage == "At least one EmailAddress must be included"));
        }

        // *************************************************************************************************

        [Fact]
        public void Fax_Contains_More_Than_50_Characters_Generates_Error()
        {
            // Arrange
            var contact = new Contact()
            {
                Fax = "626395681100000000000000000000000000000000000000000"
            };

            // Act
            var result = validator.Validate(contact);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage == "Fax cannot contain more than 50 characters"));
        }

        // *************************************************************************************************

        [Fact]
        public void First_Name_Contains_More_Than_50_Characters_Generates_Error()
        {
            // Arrange
            var contact = new Contact()
            {
                FirstName = "Sheldooooooooooooooooooooooooooooooooooooooooooooon"
            };

            // Act
            var result = validator.Validate(contact);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage == "FirstName cannot contain more than 50 characters"));
        }

        // *************************************************************************************************

        [Fact]
        public void HomePhone_Contains_More_Than_50_Characters_Generates_Error()
        {
            // Arrange
            var contact = new Contact()
            {
                HomePhone = "626395681100000000000000000000000000000000000000000"
            };

            // Act
            var result = validator.Validate(contact);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage == "HomePhone cannot contain more than 50 characters"));
        }

        // *************************************************************************************************

        [Fact]
        public void JobTitle_Contains_More_Than_50_Characters_Generates_Error()
        {
            // Arrange
            var contact = new Contact()
            {
                JobTitle = "Theoretical Phyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyysicist"
            };

            // Act
            var result = validator.Validate(contact);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage == "JobTitle cannot contain more than 50 characters"));
        }

        // *************************************************************************************************

        [Fact]
        public void Last_Name_Contains_More_Than_50_Characters_Generates_Error()
        {
            // Arrange
            var contact = new Contact()
            {
                LastName = "Cooooooooooooooooooooooooooooooooooooooooooooooooper"
            };

            // Act
            var result = validator.Validate(contact);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage == "LastName cannot contain more than 50 characters"));
        }

        // *************************************************************************************************

        [Fact]
        public void Lists_Is_Null_Generates_Error()
        {
            // Arrange
            var contact = new Contact();

            // Act
            var result = validator.Validate(contact);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage == "At least one List must be included"));
        }

        // *************************************************************************************************

        [Fact]
        public void Prefix_Name_Contains_More_Than_4_Characters_Generates_Error()
        {
            // Arrange
            var contact = new Contact() { PrefixName = "LongName" };

            // Act
            var result = validator.Validate(contact);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage == "PrefixName cannot contain more than 4 characters"));
        }

        // *************************************************************************************************

        [Fact]
        public void Work_Phone_Contains_More_Than_50_Characters_Generates_Error()
        {
            // Arrange
            var contact = new Contact()
            {
                WorkPhone = "626395681100000000000000000000000000000000000000000"
            };

            // Act
            var result = validator.Validate(contact);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage == "WorkPhone cannot contain more than 50 characters"));
        }
    }
}