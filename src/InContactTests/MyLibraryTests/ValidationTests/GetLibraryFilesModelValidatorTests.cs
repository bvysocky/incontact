﻿#region License
// The MIT License (MIT)
//
// Copyright (c) 2015 Scott Lance, Ethan Tipton
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// 
// The most recent version of this license can be found at: http://opensource.org/licenses/MIT
#endregion

using System.Diagnostics.CodeAnalysis;
using System.Linq;
using FluentValidation.TestHelper;
using InContactSdk.MyLibrary.Models;
using InContactSdk.MyLibrary.Validation;
using Xunit;

namespace InContactTests.MyLibraryTests.ValidationTests
{
    [ExcludeFromCodeCoverage]
    public class GetLibraryFilesModelValidatorTests
    {
        private readonly GetLibraryFilesModelValidator validator;

        public GetLibraryFilesModelValidatorTests()
        {
            validator = new GetLibraryFilesModelValidator();
        }

        // *************************************************************************************************

        [Fact]
        public void QueryLimit_IfValueIsLessThanOne_FailsValidation()
        {
            // Arrange
            var success = true;
            var model = new GetLibraryFilesModel()
            {
                QueryLimit = 0
            };

            // Act
            try
            {
                validator.ShouldHaveValidationErrorFor(x => x.QueryLimit, model);
            }
            catch (ValidationTestException)
            {
                success = false;
            }

            // Assert
            Assert.True(success);
        }

        [Fact]
        public void QueryLimit_IfValueIsGreaterThanOneThousand_FailsValidation()
        {
            // Arrange
            var success = true;
            var model = new GetLibraryFilesModel()
            {
                QueryLimit = 1001
            };

            // Act
            try
            {
                validator.ShouldHaveValidationErrorFor(x => x.QueryLimit, model);
            }
            catch (ValidationTestException)
            {
                success = false;
            }

            // Assert
            Assert.True(success);
        }

        [Fact]
        public void QueryLimit_IfValueIsOne_PassesValidation()
        {
            // Arrange
            var success = true;
            var model = new GetLibraryFilesModel()
            {
                QueryLimit = 1
            };

            // Act
            try
            {
                validator.ShouldNotHaveValidationErrorFor(x => x.QueryLimit, model);
            }
            catch (ValidationTestException)
            {
                success = false;
            }

            // Assert
            Assert.True(success);
        }

        [Fact]
        public void QueryLimit_IfValueIsOneThousand_PassesValidation()
        {
            // Arrange
            var success = true;
            var model = new GetLibraryFilesModel()
            {
                QueryLimit = 1000
            };

            // Act
            try
            {
                validator.ShouldNotHaveValidationErrorFor(x => x.QueryLimit, model);
            }
            catch (ValidationTestException)
            {
                success = false;
            }

            // Assert
            Assert.True(success);
        }

        [Fact]
        public void QueryLimit_IfInvalid_AddsError()
        {
            // Arrange
            var model = new GetLibraryFilesModel()
            {
                QueryLimit = 0
            };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.True(result.Errors.Where(e => e.ErrorMessage == "You must specify a number from 1 - 1000").Any());
        }

        // *************************************************************************************************

    }
}
