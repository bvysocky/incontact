﻿#region License
// The MIT License (MIT)
//
// Copyright (c) 2015 Scott Lance, Ethan Tipton
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// 
// The most recent version of this license can be found at: http://opensource.org/licenses/MIT
#endregion

using InContactSdk.Helpers;
using InContactSdk.Reports;
using InContactSdk.Reports.Validation;
using System;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Xunit;

namespace InContactTests.ReportsTests.ValidationTests
{
    [ExcludeFromCodeCoverage]
    public class ActivityReportModelValidatorTests
    {
        private readonly ActivityReportModelValidator validator;

        public ActivityReportModelValidatorTests()
        {
            validator = new ActivityReportModelValidator();
        }

        // *************************************************************************************************

        [Fact]
        public void Id_Is_NullOrWhiteSpace_Generates_Error()
        {
            // Arrange
            var contactId = "";
            var report = ServiceEndpoint.Opens;
            var createdSince = DateTime.Now;
            var limit = 1;

            var model = new ActivityReportModel { Id = contactId, ContactService = true, Report = report, CreatedSince = createdSince, Limit = limit };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage == "Id is required"));
        }

        [Fact]
        public void Id_Is_Not_NullOrWhiteSpace_Is_Valid()
        {
            // Arrange
            var contactId = "123456";
            var report = ServiceEndpoint.Opens;
            var createdSince = DateTime.Now;
            var limit = 1;

            var model = new ActivityReportModel { Id = contactId, ContactService = true, Report = report, CreatedSince = createdSince, Limit = limit };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.False(result.Errors.Any(e => e.ErrorMessage == "Id is required"));
        }

        // *************************************************************************************************      

        [Fact]
        public void Report_Is_BounceReport_Is_Valid()
        {
            // Arrange
            var contactId = "";
            var report = ServiceEndpoint.Bounces;
            var createdSince = DateTime.Now;
            var limit = 1;

            var model = new ActivityReportModel { Id = contactId, ContactService = true, Report = report, CreatedSince = createdSince, Limit = limit };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.False(result.Errors.Any(e => e.ErrorMessage == "report must be a valid report"));
        }

        [Fact]
        public void Report_Is_ClickReport_Is_Valid()
        {
            // Arrange
            var contactId = "";
            var report = ServiceEndpoint.Clicks;
            var createdSince = DateTime.Now;
            var limit = 1;

            var model = new ActivityReportModel { Id = contactId, ContactService = true, Report = report, CreatedSince = createdSince, Limit = limit };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.False(result.Errors.Any(e => e.ErrorMessage == "report must be a valid report"));
        }

        [Fact]
        public void Report_Is_DetailedReport_Is_Valid()
        {
            // Arrange
            var contactId = "";
            var report = ServiceEndpoint.Tracking;
            var createdSince = DateTime.Now;
            var limit = 1;

            var model = new ActivityReportModel { Id = contactId, ContactService = true, Report = report, CreatedSince = createdSince, Limit = limit };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.False(result.Errors.Any(e => e.ErrorMessage == "report must be a valid report"));
        }

        [Fact]
        public void Report_Is_ForwardReport_Is_Valid()
        {
            // Arrange
            var contactId = "";
            var report = ServiceEndpoint.Forwards;
            var createdSince = DateTime.Now;
            var limit = 1;

            var model = new ActivityReportModel { Id = contactId, ContactService = true, Report = report, CreatedSince = createdSince, Limit = limit };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.False(result.Errors.Any(e => e.ErrorMessage == "report must be a valid report"));
        }

        [Fact]
        public void Report_Is_OpenReport_Is_Valid()
        {
            // Arrange
            var contactId = "";
            var report = ServiceEndpoint.Opens;
            var createdSince = DateTime.Now;
            var limit = 1;

            var model = new ActivityReportModel { Id = contactId, ContactService = true, Report = report, CreatedSince = createdSince, Limit = limit };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.False(result.Errors.Any(e => e.ErrorMessage == "report must be a valid report"));
        }

        [Fact]
        public void Report_Is_SendReport_Is_Valid()
        {
            // Arrange
            var contactId = "";
            var report = ServiceEndpoint.Sends;
            var createdSince = DateTime.Now;
            var limit = 1;

            var model = new ActivityReportModel { Id = contactId, ContactService = true, Report = report, CreatedSince = createdSince, Limit = limit };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.False(result.Errors.Any(e => e.ErrorMessage == "report must be a valid report"));
        }

        [Fact]
        public void Report_Is_UnsubscribeReport_Is_Valid()
        {
            // Arrange
            var contactId = "";
            var report = ServiceEndpoint.Unsubscribes;
            var createdSince = DateTime.Now;
            var limit = 1;

            var model = new ActivityReportModel { Id = contactId, ContactService = true, Report = report, CreatedSince = createdSince, Limit = limit };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.False(result.Errors.Any(e => e.ErrorMessage == "report must be a valid report"));
        }

        [Fact]
        public void CreatedSince_Is_Before_1_1_1970_Generates_Error()
        {
            // Arrange
            var contactId = "";
            var report = ServiceEndpoint.Opens;
            var createdSince = new DateTime(1969, 12, 31);
            var limit = 1;

            var model = new ActivityReportModel { Id = contactId, ContactService = true, Report = report, CreatedSince = createdSince, Limit = limit };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage == "createdSince must be a in a ISO 8601 format and must be a date greater than 1/1/1970"));
        }

        // *************************************************************************************************

        [Fact]
        public void CreatedSince_Is_After_1_1_1970_Is_Valid()
        {
            // Arrange
            var contactId = "";
            var report = ServiceEndpoint.Opens;
            var createdSince = DateTime.Now;
            var limit = 1;

            var model = new ActivityReportModel { Id = contactId, ContactService = true, Report = report, CreatedSince = createdSince, Limit = limit };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.False(result.Errors.Any(e => e.ErrorMessage == "createdSince must be a in a ISO 8601 format and must be a date greater than 1/1/1970"));
        }

        // *************************************************************************************************

        [Fact]
        public void Limit_Is_Less_Than_One_Generates_Error()
        {
            // Arrange
            var contactId = "";
            var report = ServiceEndpoint.Opens;
            var createdSince = DateTime.Now;
            var limit = 0;

            var model = new ActivityReportModel { Id = contactId, ContactService = true, Report = report, CreatedSince = createdSince, Limit = limit };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage == "limit must be between 1 and 500"));
        }

        [Fact]
        public void Limit_Is_Less_Than_Five_Hundred_Generates_Error()
        {
            // Arrange
            var contactId = "";
            var report = ServiceEndpoint.Opens;
            var createdSince = DateTime.Now;
            var limit = 501;

            var model = new ActivityReportModel { Id = contactId, ContactService = true, Report = report, CreatedSince = createdSince, Limit = limit };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage == "limit must be between 1 and 500"));
        }

        [Fact]
        public void Limit_Is_One_Is_Valid()
        {
            // Arrange
            var contactId = "";
            var report = ServiceEndpoint.Opens;
            var createdSince = DateTime.Now;
            var limit = 1;

            var model = new ActivityReportModel { Id = contactId, ContactService = true, Report = report, CreatedSince = createdSince, Limit = limit };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.False(result.Errors.Any(e => e.ErrorMessage == "limit must be between 1 and 500"));
        }

        [Fact]
        public void Limit_Is_Five_Hundred_Is_Valid()
        {
            // Arrange
            var contactId = "";
            var report = ServiceEndpoint.Opens;
            var createdSince = DateTime.Now;
            var limit = 500;

            var model = new ActivityReportModel { Id = contactId, ContactService = true, Report = report, CreatedSince = createdSince, Limit = limit };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.False(result.Errors.Any(e => e.ErrorMessage == "limit must be between 1 and 500"));
        }

        [Fact]
        public void Limit_Is_Between_One_And_Five_Hundred_Is_Valid()
        {
            // Arrange
            var contactId = "";
            var report = ServiceEndpoint.Opens;
            var createdSince = DateTime.Now;
            var limit = 250;

            var model = new ActivityReportModel { Id = contactId, ContactService = true, Report = report, CreatedSince = createdSince, Limit = limit };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.False(result.Errors.Any(e => e.ErrorMessage == "limit must be between 1 and 500"));
        }
    }
}