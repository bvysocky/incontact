﻿#region License
// The MIT License (MIT)
//
// Copyright (c) 2015 Scott Lance, Ethan Tipton
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// 
// The most recent version of this license can be found at: http://opensource.org/licenses/MIT
#endregion

using InContactSdk.ContactTracking;
using InContactSdk.Helpers;
using InContactSdk.Models;
using InContactSdk.Reports;
using InContactSdk.Validation;
using Moq;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Threading.Tasks;
using Xunit;

namespace InContactTests.ContactTrackingTests
{
    [ExcludeFromCodeCoverage]
    public class ContactTrackingServiceTests
    {
        private ContactTrackingService contactTrackingService = null;
        private IInContactSettings settings = null;
        private Mock<IValidationService> validationService = null;
        private Mock<ISerialization> serialization = null;
        private Mock<IWebServiceRequest> webServiceRequest = null;

        public ContactTrackingServiceTests()
        {
            settings = new InContactSettings("123456789", "D27D92A2-6A0B-4F33-BE8A-3C63E597AB18");
            validationService = new Mock<IValidationService>();
            serialization = new Mock<ISerialization>();
            webServiceRequest = new Mock<IWebServiceRequest>();
        }

        public void InitializeContactTrackingService<T>()
            where T : class, new()
        {
            validationService.Setup(m => m.Validate(It.IsAny<IValidatable>()));
            serialization.Setup(m => m.Serialize(It.IsAny<object>())).Returns("");
            serialization.Setup(m => m.Deserialize<T>(It.IsAny<string>())).Returns(new T());
            contactTrackingService = new ContactTrackingService(settings, validationService.Object, webServiceRequest.Object);
        }

        // *************************************************************************************************       

        [Fact]
        public async Task GetContactListMembership_Uses_Correct_Endpoint()
        {
            // Arrange
            InitializeContactTrackingService<PaginatedResult<DetailedActivityReport>>();
            string listId = "123456";
            Uri endpoint = null;

            webServiceRequest.Setup(m => m.GetDeserializedAsync<PaginatedResult<DetailedActivityReport>>(It.IsAny<Uri>(), It.IsAny<string>()))
                .Callback((Uri a, string b) => endpoint = a)
                .ReturnsAsync(new PaginatedResult<DetailedActivityReport>());

            // Act
            await contactTrackingService.GetDetailedActivityReportAsync(listId);

            // Assert
            Assert.Equal(string.Format("{0}contacts/{1}/tracking?api_key={2}&limit=500", settings.BaseUrl, listId, settings.ApiKey), endpoint.AbsoluteUri);
        }

        [Fact]
        public async Task GetContactListMembership_Verify_MakeSerializedRequestAsync()
        {
            // Arrange
            InitializeContactTrackingService<PaginatedResult<DetailedActivityReport>>();
            string listId = "123456";

            // Act
            await contactTrackingService.GetDetailedActivityReportAsync(listId);

            // Assert
            webServiceRequest.Verify(m => m.GetDeserializedAsync<PaginatedResult<DetailedActivityReport>>(It.IsAny<Uri>(), It.IsAny<string>()), Times.Once());
        }

        // *************************************************************************************************

        [Fact]
        public async Task GetCampaignActivityReport_Uses_Correct_Endpoint()
        {
            // Arrange
            InitializeContactTrackingService<ActivitySummaryReport>();
            string contactId = "123456";
            Uri endpoint = null;

            webServiceRequest.Setup(m => m.GetDeserializedAsync<IEnumerable<CampaignActivityReport>>(It.IsAny<Uri>(), It.IsAny<string>()))
                .Callback((Uri a, string b) => endpoint = a)
                .ReturnsAsync(new List<CampaignActivityReport>());

            // Act
            await contactTrackingService.GetCampaignActivityReportAsync(contactId);

            // Assert            
            Assert.Equal(string.Format("{0}contacts/{1}/tracking/reports/summaryByCampaign?api_key={2}", settings.BaseUrl, contactId, settings.ApiKey), endpoint.AbsoluteUri);
        }

        [Fact]
        public async Task GetCampaignActivityReport_Verify_MakeSerializedRequestAsync()
        {
            // Arrange
            InitializeContactTrackingService<CampaignActivityReport>();
            string contactId = "123456";

            // Act
            await contactTrackingService.GetCampaignActivityReportAsync(contactId);

            // Assert
            webServiceRequest.Verify(m => m.GetDeserializedAsync<IEnumerable<CampaignActivityReport>>(It.IsAny<Uri>(), It.IsAny<string>()), Times.Once());
        }

        // *************************************************************************************************

        [Fact]
        public async Task GetActivitySummaryReport_Uses_Correct_Endpoint()
        {
            // Arrange
            InitializeContactTrackingService<ActivitySummaryReport>();
            string contactId = "123456";
            Uri endpoint = null;

            webServiceRequest.Setup(m => m.GetDeserializedAsync<ContactActivitySummaryReport>(It.IsAny<Uri>(), It.IsAny<string>()))
                .Callback((Uri a, string b) => endpoint = a)
                .ReturnsAsync(new ContactActivitySummaryReport());

            // Act
            await contactTrackingService.GetActivitySummaryReportAsync(contactId);

            // Assert            
            Assert.Equal(string.Format("{0}contacts/{1}/tracking/reports/summary?api_key={2}", settings.BaseUrl, contactId, settings.ApiKey), endpoint.AbsoluteUri);
        }

        [Fact]
        public async Task GetActivitySummaryReport_Verify_MakeSerializedRequestAsync()
        {
            // Arrange
            InitializeContactTrackingService<ActivitySummaryReport>();
            string contactId = "123456";

            // Act
            await contactTrackingService.GetActivitySummaryReportAsync(contactId);

            // Assert
            webServiceRequest.Verify(m => m.GetDeserializedAsync<ContactActivitySummaryReport>(It.IsAny<Uri>(), It.IsAny<string>()), Times.Once());
        }

        // *************************************************************************************************

        [Fact]
        public async Task GetBounceActivityReport_Uses_Correct_Endpoint()
        {
            // Arrange
            InitializeContactTrackingService<PaginatedResult<BounceActivityReport>>();
            string contactId = "123456";
            Uri endpoint = null;

            webServiceRequest.Setup(m => m.GetDeserializedAsync<PaginatedResult<BounceActivityReport>>(It.IsAny<Uri>(), It.IsAny<string>()))
                .Callback((Uri a, string b) => endpoint = a)
                .ReturnsAsync(new PaginatedResult<BounceActivityReport>());

            // Act
            await contactTrackingService.GetBounceActivityReportAsync(contactId);

            // Assert
            Assert.Equal(string.Format("{0}contacts/{1}/tracking/bounces?api_key={2}&limit=500", settings.BaseUrl, contactId, settings.ApiKey), endpoint.AbsoluteUri);
        }

        [Fact]
        public async Task GetBounceActivityReport_Verify_MakeSerializedRequestAsync()
        {
            // Arrange
            InitializeContactTrackingService<PaginatedResult<BounceActivityReport>>();
            string contactId = "123456";

            // Act
            await contactTrackingService.GetBounceActivityReportAsync(contactId);

            // Assert
            webServiceRequest.Verify(m => m.GetDeserializedAsync<PaginatedResult<BounceActivityReport>>(It.IsAny<Uri>(), It.IsAny<string>()), Times.Once());
        }

        // *************************************************************************************************

        [Fact]
        public async Task GetClickActivityReport_Uses_Correct_Endpoint()
        {
            // Arrange
            InitializeContactTrackingService<PaginatedResult<ContactClickActivityReport>>();
            string contactId = "123456";
            Uri endpoint = null;

            webServiceRequest.Setup(m => m.GetDeserializedAsync<PaginatedResult<ContactClickActivityReport>>(It.IsAny<Uri>(), It.IsAny<string>()))
                .Callback((Uri a, string b) => endpoint = a)
                .ReturnsAsync(new PaginatedResult<ContactClickActivityReport>());

            // Act
            await contactTrackingService.GetClickActivityReportAsync(contactId);

            // Assert
            Assert.Equal(string.Format("{0}contacts/{1}/tracking/clicks?api_key={2}&limit=500", settings.BaseUrl, contactId, settings.ApiKey), endpoint.AbsoluteUri);
        }

        [Fact]
        public async Task GetClickActivityReport_Verify_MakeSerializedRequestAsync()
        {
            // Arrange
            InitializeContactTrackingService<PaginatedResult<ContactClickActivityReport>>();
            string contactId = "123456";

            // Act
            await contactTrackingService.GetClickActivityReportAsync(contactId);

            // Assert
            webServiceRequest.Verify(m => m.GetDeserializedAsync<PaginatedResult<ContactClickActivityReport>>(It.IsAny<Uri>(), It.IsAny<string>()), Times.Once());
        }

        // *************************************************************************************************       

        [Fact]
        public async Task GetForwardActivityReport_Uses_Correct_Endpoint()
        {
            // Arrange
            InitializeContactTrackingService<PaginatedResult<ForwardActivityReport>>();
            string contactId = "123456";
            Uri endpoint = null;

            webServiceRequest.Setup(m => m.GetDeserializedAsync<PaginatedResult<ForwardActivityReport>>(It.IsAny<Uri>(), It.IsAny<string>()))
                .Callback((Uri a, string b) => endpoint = a)
                .ReturnsAsync(new PaginatedResult<ForwardActivityReport>());

            // Act
            await contactTrackingService.GetForwardActivityReportAsync(contactId);

            // Assert
            Assert.Equal(string.Format("{0}contacts/{1}/tracking/forwards?api_key={2}&limit=500", settings.BaseUrl, contactId, settings.ApiKey), endpoint.AbsoluteUri);
        }

        [Fact]
        public async Task GetForwardActivityReport_Verify_MakeSerializedRequestAsync()
        {
            // Arrange
            InitializeContactTrackingService<PaginatedResult<ForwardActivityReport>>();
            string contactId = "123456";

            // Act
            await contactTrackingService.GetForwardActivityReportAsync(contactId);

            // Assert
            webServiceRequest.Verify(m => m.GetDeserializedAsync<PaginatedResult<ForwardActivityReport>>(It.IsAny<Uri>(), It.IsAny<string>()), Times.Once());
        }

        // *************************************************************************************************       

        [Fact]
        public async Task GetOpenActivityReport_Uses_Correct_Endpoint()
        {
            // Arrange
            InitializeContactTrackingService<PaginatedResult<OpenActivityReport>>();
            string contactId = "123456";
            Uri endpoint = null;

            webServiceRequest.Setup(m => m.GetDeserializedAsync<PaginatedResult<OpenActivityReport>>(It.IsAny<Uri>(), It.IsAny<string>()))
                .Callback((Uri a, string b) => endpoint = a)
                .ReturnsAsync(new PaginatedResult<OpenActivityReport>());

            // Act
            await contactTrackingService.GetOpenActivityReportAsync(contactId);

            // Assert
            Assert.Equal(string.Format("{0}contacts/{1}/tracking/opens?api_key={2}&limit=500", settings.BaseUrl, contactId, settings.ApiKey), endpoint.AbsoluteUri);
        }

        [Fact]
        public async Task GetOpenActivityReport_Verify_MakeSerializedRequestAsync()
        {
            // Arrange
            InitializeContactTrackingService<PaginatedResult<OpenActivityReport>>();
            string contactId = "123456";

            // Act
            await contactTrackingService.GetOpenActivityReportAsync(contactId);

            // Assert
            webServiceRequest.Verify(m => m.GetDeserializedAsync<PaginatedResult<OpenActivityReport>>(It.IsAny<Uri>(), It.IsAny<string>()), Times.Once());
        }

        // *************************************************************************************************

        [Fact]
        public async Task GetSendActivityReport_Uses_Correct_Endpoint()
        {
            // Arrange
            InitializeContactTrackingService<PaginatedResult<SendActivityReport>>();
            string contactId = "123456";
            Uri endpoint = null;

            webServiceRequest.Setup(m => m.GetDeserializedAsync<PaginatedResult<SendActivityReport>>(It.IsAny<Uri>(), It.IsAny<string>()))
                .Callback((Uri a, string b) => endpoint = a)
                .ReturnsAsync(new PaginatedResult<SendActivityReport>());

            // Act
            await contactTrackingService.GetSendActivityReportAsync(contactId);

            // Assert
            Assert.Equal(string.Format("{0}contacts/{1}/tracking/sends?api_key={2}&limit=500", settings.BaseUrl, contactId, settings.ApiKey), endpoint.AbsoluteUri);
        }

        [Fact]
        public async Task GetSendActivityReport_Verify_MakeSerializedRequestAsync()
        {
            // Arrange
            InitializeContactTrackingService<PaginatedResult<SendActivityReport>>();
            string contactId = "123456";

            // Act
            await contactTrackingService.GetSendActivityReportAsync(contactId);

            // Assert
            webServiceRequest.Verify(m => m.GetDeserializedAsync<PaginatedResult<SendActivityReport>>(It.IsAny<Uri>(), It.IsAny<string>()), Times.Once());
        }

        // *************************************************************************************************      

        [Fact]
        public async Task GetUnsubscribeActivityReport_Uses_Correct_Endpoint()
        {
            // Arrange
            InitializeContactTrackingService<PaginatedResult<UnsubscribeActivityReport>>();
            string contactId = "123456";
            Uri endpoint = null;

            webServiceRequest.Setup(m => m.GetDeserializedAsync<PaginatedResult<UnsubscribeActivityReport>>(It.IsAny<Uri>(), It.IsAny<string>()))
                .Callback((Uri a, string b) => endpoint = a)
                .ReturnsAsync(new PaginatedResult<UnsubscribeActivityReport>());

            // Act
            await contactTrackingService.GetUnsubscribeActivityReportAsync(contactId);

            // Assert
            Assert.Equal(string.Format("{0}contacts/{1}/tracking/unsubscribes?api_key={2}&limit=500", settings.BaseUrl, contactId, settings.ApiKey), endpoint.AbsoluteUri);
        }

        [Fact]
        public async Task GetUnsubscribeActivityReport_Verify_MakeSerializedRequestAsync()
        {
            // Arrange
            InitializeContactTrackingService<PaginatedResult<UnsubscribeActivityReport>>();
            string contactId = "123456";

            // Act
            await contactTrackingService.GetUnsubscribeActivityReportAsync(contactId);

            // Assert
            webServiceRequest.Verify(m => m.GetDeserializedAsync<PaginatedResult<UnsubscribeActivityReport>>(It.IsAny<Uri>(), It.IsAny<string>()), Times.Once());
        }
    }
}