﻿#region License
// The MIT License (MIT)
//
// Copyright (c) 2015 Scott Lance, Ethan Tipton
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// 
// The most recent version of this license can be found at: http://opensource.org/licenses/MIT
#endregion

using InContactSdk.EventSpotEvents;
using InContactSdk.EventSpotEvents.Validation;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using Xunit;

namespace InContactTests.EventSpotEventTests.ValidationTests
{
    [ExcludeFromCodeCoverage]
    public class ItemValidatorTests
    {
        private readonly ItemValidator validator;

        public ItemValidatorTests()
        {
            validator = new ItemValidator();
        }

        // *************************************************************************************************

        [Fact]
        public void DefaultQuantityTotal_Is_Negitive_One_Generates_Error()
        {
            // Arrange
            var defaultQuantityTotal = -1;
            var model = new Item { DefaultQuantityTotal = defaultQuantityTotal };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage == "DefaultQuantityTotal must be 0 or greater"));
        }

        [Fact]
        public void DefaultQuantityTotal_Is_Zero_Is_Valid()
        {
            // Arrange
            var defaultQuantityTotal = 0;
            var model = new Item { DefaultQuantityTotal = defaultQuantityTotal };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.False(result.Errors.Any(e => e.ErrorMessage == "DefaultQuantityTotal must be 0 or greater"));
        }

        [Fact]
        public void DefaultQuantityTotal_Is_One_Is_Valid()
        {
            // Arrange
            var defaultQuantityTotal = 1;
            var model = new Item { DefaultQuantityTotal = defaultQuantityTotal };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.False(result.Errors.Any(e => e.ErrorMessage == "DefaultQuantityTotal must be 0 or greater"));
        }

        // *************************************************************************************************

        [Fact]
        public void Description_Is_Greater_Than_2048_Characters_Generates_Error()
        {
            // Arrange
            StringBuilder description = new StringBuilder();
            for (int i = 0; i < 2049; i++)
                description.Append("k");

            var model = new Item { Description = description.ToString() };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage == "Description cannot be greater than 2048 characters"));
        }

        [Fact]
        public void Description_Is_Less_Than_2048_Characters_Is_Valid()
        {
            // Arrange
            var description = "Item Description";
            var model = new Item { Description = description };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.False(result.Errors.Any(e => e.ErrorMessage == "Description cannot be greater than 2048 characters"));
        }

        // *************************************************************************************************

        [Fact]
        public void Name_IsNullOrWhiteSpace_Generates_Error()
        {
            // Arrange
            var name = "";
            var model = new Item { Name = name };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage == "Name is required"));
        }

        [Fact]
        public void Name_Is_Greater_Than_100_Characters_Generates_Error()
        {
            // Arrange
            var name = "LotsAndLotsAndLotsAndLotsAndLotsAndLotsAndLotsAndLotsAndLotsAndLotsAndLotsAndLotsAndLotsAndLotsOfStuffNThings";
            var model = new Item { Name = name };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage == "Name cannot be greater than 100 characters"));
        }

        [Fact]
        public void Name_Is_Not_NullOrWhiteSpace_Is_Valid()
        {
            // Arrange
            var name = "StuffNThings";
            var model = new Item { Name = name };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.False(result.Errors.Any(e => e.ErrorMessage == "Name is required"));
        }

        // *************************************************************************************************

        [Fact]
        public void PerRegistrantLimit_Is_Negitive_One_Generates_Error()
        {
            // Arrange
            var perRegistrantLimit = -1;
            var model = new Item { PerRegistrantLimit = perRegistrantLimit };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage == "PerRegistrantLimit must be 0 or greater"));
        }

        [Fact]
        public void PerRegistrantLimit_Is_Zero_Is_Valid()
        {
            // Arrange
            var perRegistrantLimit = 0;
            var model = new Item { PerRegistrantLimit = perRegistrantLimit };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.False(result.Errors.Any(e => e.ErrorMessage == "PerRegistrantLimit must be 0 or greater"));
        }

        [Fact]
        public void PerRegistrantLimit_Is_One_Is_Valid()
        {
            // Arrange
            var perRegistrantLimit = 1;
            var model = new Item { PerRegistrantLimit = perRegistrantLimit };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.False(result.Errors.Any(e => e.ErrorMessage == "PerRegistrantLimit must be 0 or greater"));
        }

        // *************************************************************************************************

        [Fact]
        public void Price_Is_Negitive_One_Generates_Error()
        {
            // Arrange
            var price = -1;
            var model = new Item { Price = price };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage == "Price must be 0 or greater"));
        }

        [Fact]
        public void Price_Is_Zero_Is_Valid()
        {
            // Arrange
            var price = 0;
            var model = new Item { Price = price };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.False(result.Errors.Any(e => e.ErrorMessage == "Price must be 0 or greater"));
        }

        [Fact]
        public void Price_Is_One_Is_Valid()
        {
            // Arrange
            var price = 1;
            var model = new Item { Price = price };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.False(result.Errors.Any(e => e.ErrorMessage == "Price must be 0 or greater"));
        }
    }
}