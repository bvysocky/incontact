﻿#region License
// The MIT License (MIT)
//
// Copyright (c) 2015 Scott Lance, Ethan Tipton
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// 
// The most recent version of this license can be found at: http://opensource.org/licenses/MIT
#endregion

using InContactSdk.EventSpotEvents;
using InContactSdk.Helpers;
using InContactSdk.Validation;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using Xunit;

namespace InContactTests.EventSpotEventTests.ValidationTests
{
    [ExcludeFromCodeCoverage]
    public class EventValidatorTests
    {
        private readonly EventValidator validator;

        public EventValidatorTests()
        {
            validator = new EventValidator();
        }

        // *************************************************************************************************

        [Fact]
        public void Address_Is_Null_When_Is_Map_Displayed_Is_True_Generates_Error()
        {
            // Arrange
            var model = new Event { Address = null, IsMapDisplayed = true };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage == "Address must be entered when IsMapDisplayed is true"));
        }

        [Fact]
        public void Address_Is_Not_Null_When_Is_Map_Displayed_Is_True_Is_Valid()
        {
            // Arrange
            var model = new Event { Address = new EventSpotAddress(), IsMapDisplayed = true };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.False(result.Errors.Any(e => e.ErrorMessage == "Address must be entered when IsMapDisplayed is true"));
        }

        // *************************************************************************************************

        [Fact]
        public void Contact_Is_Null_Generates_Error()
        {
            // Arrange
            var model = new Event { Contact = null };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage == "Contact is required"));
        }

        [Fact]
        public void Contact_Is_Not_Null_Is_Valid()
        {
            // Arrange
            var model = new Event { Contact = new EventSpotContact() };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.False(result.Errors.Any(e => e.ErrorMessage == "Contact is required"));
        }

        // *************************************************************************************************

        [Fact]
        public void Description_Is_Longer_Than_500_Characters_Generates_Error()
        {
            // Arrange
            var description = new StringBuilder();

            for (int i = 0; i < 51; i++) description.Append("kkkkkkkkkk");

            var model = new Event { Description = description.ToString() };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage == "Description cannot be greater than 500 characters"));
        }

        [Fact]
        public void Description_Is_Less_Than_500_Characters_Is_Valid()
        {
            // Arrange
            var description = "Description";
            var model = new Event { Description = description };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.False(result.Errors.Any(e => e.ErrorMessage == "Description cannot be greater than 500 characters"));
        }

        // *************************************************************************************************

        [Fact]
        public void End_Date_Is_Invalid_Iso8601_Date_Generates_Error()
        {
            // Arrange
            var endDate = new DateTime(1969, 12, 31);
            var model = new Event { EndDate = endDate };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage == "EndDate must be a in a ISO 8601 format and must be a date greater than 1/1/1970"));
        }

        // *************************************************************************************************

        [Fact]
        public void Early_Fee_Date_Is_Valid_Iso8601_Date_Is_Valid()
        {
            // Arrange
            var endDate = new DateTime(1970, 1, 1);
            var model = new Event { EndDate = endDate };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.False(result.Errors.Any(e => e.ErrorMessage == "EndDate must be a in a ISO 8601 format and must be a date greater than 1/1/1970"));
        }

        // *************************************************************************************************

        [Fact]
        public void Google_Analytics_Key_Is_Longer_Than_20_Characters_Generates_Error()
        {
            // Arrange
            var googleAnalyticsKey = "SuperVeryLongGoogleAnalyticsKey";
            var model = new Event { GoogleAnalyticsKey = googleAnalyticsKey };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage == "GoogleAnalyticsKey cannot be greater than 20 characters"));
        }

        [Fact]
        public void Google_Analytics_Key_Is_Less_Than_20_Characters_Is_Valid()
        {
            // Arrange
            var googleAnalyticsKey = "123456";
            var model = new Event { GoogleAnalyticsKey = googleAnalyticsKey };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.False(result.Errors.Any(e => e.ErrorMessage == "GoogleAnalyticsKey cannot be greater than 20 characters"));
        }

        // *************************************************************************************************

        [Fact]
        public void Google_Merchant_Id_Is_Longer_Than_20_Characters_Generates_Error()
        {
            // Arrange
            var googleMerchantId = "California Institute of Technology";
            var model = new Event { GoogleMerchantId = googleMerchantId };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage == "GoogleMerchantId cannot be greater than 20 characters"));
        }

        [Fact]
        public void Google_Merchant_Id_Is_Less_Than_20_Characters_Is_Valid()
        {
            // Arrange
            var googleMerchantId = "Cal Tech";
            var model = new Event { GoogleMerchantId = googleMerchantId };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.False(result.Errors.Any(e => e.ErrorMessage == "GoogleMerchantId cannot be greater than 20 characters"));
        }

        // *************************************************************************************************

        [Fact]
        public void Location_IsNullOrWhiteSpace_Generates_Error()
        {
            // Arrange
            var location = "";
            var model = new Event { Location = location };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage == "Location is required"));
        }

        [Fact]
        public void Location_Is_Longer_Than_20_Characters_Generates_Error()
        {
            // Arrange
            var location = new StringBuilder();

            for (int i = 0; i < 6; i++) location.Append("kkkkkkkkkkk");

            var model = new Event { Location = location.ToString() };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage == "Location cannot be greater than 50 characters"));
        }

        [Fact]
        public void Location_Is_Less_Than_20_Characters_Is_Valid()
        {
            // Arrange
            var location = "Cal Tech";
            var model = new Event { Location = location };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.False(result.Errors.Any(e => e.ErrorMessage == "Location cannot be greater than 50 characters"));
        }

        // *************************************************************************************************

        [Fact]
        public void Meta_Data_Tags_Is_Longer_Than_100_Characters_Generates_Error()
        {
            // Arrange
            var metaDataTags = new StringBuilder();

            for (int i = 0; i < 10; i++) metaDataTags.Append("kkkkkkkkkkk");

            var model = new Event { MetaDataTags = metaDataTags.ToString() };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage == "MetaDataTags cannot be greater than 100 characters"));
        }

        [Fact]
        public void Meta_Data_Tags_Is_Less_Than_100_Characters_Is_Valid()
        {
            // Arrange
            var metaDataTags = "meta data tags";
            var model = new Event { MetaDataTags = metaDataTags };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.False(result.Errors.Any(e => e.ErrorMessage == "MetaDataTags cannot be greater than 100 characters"));
        }

        // *************************************************************************************************

        [Fact]
        public void Name_IsNullOrWhiteSpace_Generates_Error()
        {
            // Arrange
            var model = new Event { Name = "" };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage == "Name is required"));
        }

        [Fact]
        public void Name_Is_Longer_Than_100_Characters_Generates_Error()
        {
            // Arrange
            var name = new StringBuilder();

            for (int i = 0; i < 10; i++) name.Append("kkkkkkkkkkk");

            var model = new Event { Name = name.ToString() };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage == "Name cannot be greater than 100 characters"));
        }

        [Fact]
        public void Name_Is_Less_Than_100_Characters_Is_Valid()
        {
            // Arrange
            var model = new Event { };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.False(result.Errors.Any(e => e.ErrorMessage == "Name cannot be greater than 100 characters"));
        }

        // *************************************************************************************************

        [Fact]
        public void Is_Virtual_Event_Is_True_Online_Meeting_Is_Null_Generates_Error()
        {
            // Arrange
            var isVirtualEvent = true;
            var model = new Event { IsVirtualEvent = isVirtualEvent, OnlineMeeting = null };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage == "OnlineMeeting is required when IsVirtualEvent is set to true"));
        }

        [Fact]
        public void Is_Virtual_Event_Is_True_Online_Meeting_Is_Not_Null_Is_Valid()
        {
            // Arrange
            var isVirtualEvent = true;
            var model = new Event { IsVirtualEvent = isVirtualEvent, OnlineMeeting = new List<OnlineMeeting>() };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.False(result.Errors.Any(e => e.ErrorMessage == "OnlineMeeting is required when IsVirtualEvent is set to true"));
        }

        [Fact]
        public void Is_Virtual_Event_Is_False_Online_Meeting_Is_Null_Is_Valid()
        {
            // Arrange           
            var isVirtualEvent = false;
            var model = new Event { IsVirtualEvent = isVirtualEvent, OnlineMeeting = null };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.False(result.Errors.Any(e => e.ErrorMessage == "OnlineMeeting is required when IsVirtualEvent is set to true"));
        }

        [Fact]
        public void Is_Virtual_Event_Is_True_Online_Meeting_Url_IsNullOrWhiteSpace_Generates_Error()
        {
            // Arrange
            var onlineMeeting = new List<OnlineMeeting> { new OnlineMeeting { Url = "" } };
            var isVirtualEvent = true;
            var model = new Event { IsVirtualEvent = isVirtualEvent, OnlineMeeting = onlineMeeting };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage == "OnlineMeeting.Url is required when IsVirtualMeeting is set to true"));
        }

        [Fact]
        public void Is_Virtual_Event_Is_True_Online_Meeting_Url_Is_Not_NullOrWhiteSpace_Is_Valid()
        {
            // Arrange
            var onlineMeeting = new List<OnlineMeeting> { new OnlineMeeting { Url = "http://join.me" } };
            var isVirtualEvent = true;
            var model = new Event { IsVirtualEvent = isVirtualEvent, OnlineMeeting = onlineMeeting };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.False(result.Errors.Any(e => e.ErrorMessage == "OnlineMeeting.Url is required when IsVirtualMeeting is set to true"));
        }

        // *************************************************************************************************

        [Fact]
        public void Payable_To_Is_Longer_Than_128_Characters_Generates_Error()
        {
            // Arrange
            var payableTo = new StringBuilder();

            for (int i = 0; i < 17; i++) payableTo.Append("kkkkkkkk");

            var model = new Event { PayableTo = payableTo.ToString() };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage == "PayableTo cannot be greater than 128 characters"));
        }

        [Fact]
        public void Payable_To_Is_Less_Than_128_Characters_Is_Valid()
        {
            // Arrange
            var payableTo = "Sheldon Cooper";
            var model = new Event { PayableTo = payableTo };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.False(result.Errors.Any(e => e.ErrorMessage == "PayableTo cannot be greater than 128 characters"));
        }

        [Fact]
        public void Payable_To_Is_Null_Payment_Options_Contains_Check_Generates_Error()
        {
            // Arrange
            var payableTo = "";
            var paymentAddress = new EventSpotAddress();
            var paymentOptions = new List<PaymentType> { PaymentType.Check };
            var model = new Event { PaymentAddress = paymentAddress, PayableTo = payableTo, PaymentOptions = paymentOptions };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage == "PayableTo is required when PaymentOptions contains the Check payment type"));
        }

        [Fact]
        public void Payable_To_Is_Not_Null_Payment_Options_Contains_Check_Is_Valid()
        {
            // Arrange
            var payableTo = "Sheldon Cooper";
            var paymentAddress = new EventSpotAddress();
            var paymentOptions = new List<PaymentType> { PaymentType.Check };
            var model = new Event { PaymentAddress = paymentAddress, PayableTo = payableTo, PaymentOptions = paymentOptions };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.False(result.Errors.Any(e => e.ErrorMessage == "PayableTo is required when PaymentOptions contains the Check payment type"));
        }

        // *************************************************************************************************

        [Fact]
        public void Payment_Address_Is_Null_Payment_Options_Contains_Check_Generates_Error()
        {
            // Arrange            
            var paymentOptions = new List<PaymentType> { PaymentType.Check };
            var model = new Event { PaymentAddress = null, PaymentOptions = paymentOptions };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage == "PaymentAddress is required when PaymentOptions contains the Check payment type"));
        }

        [Fact]
        public void Payment_Address_Is_Not_Null_Payment_Options_Contains_Check_Is_Valid()
        {
            // Arrange
            var paymentAddress = new EventSpotAddress();
            var paymentOptions = new List<PaymentType> { PaymentType.Check };
            var model = new Event { PaymentAddress = paymentAddress, PaymentOptions = paymentOptions };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.False(result.Errors.Any(e => e.ErrorMessage == "PaymentAddress is required when PaymentOptions contains the Check payment type"));
        }

        [Fact]
        public void Payment_Address_Is_Null_Is_Valid()
        {
            // Arrange
            var paypalAccountEmail = "";
            var model = new Event { PayPalAccountEmail = paypalAccountEmail };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.False(result.Errors.Any(e => e.ErrorMessage == "PaymentAddress is required when PaymentOptions contains the Check payment type"));
        }

        // *************************************************************************************************

        [Fact]
        public void PayPal_Account_Email_Is_Longer_Than_128_Characters_Generates_Error()
        {
            // Arrange
            var paypalAccountEmail = new StringBuilder("scooper@caltech");

            for (int i = 0; i < 111; i++) paypalAccountEmail.Append("k");

            var model = new Event { PayPalAccountEmail = paypalAccountEmail.Append(".edu").ToString() };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage == "PayPalAccountEmail cannot be greater than 128 characters"));
        }

        [Fact]
        public void PayPal_Account_Email_Is_Less_Than_128_Characters_Is_Valid()
        {
            // Arrange
            var paypalAccountEmail = "scooper@caltech.edu";
            var model = new Event { PayPalAccountEmail = paypalAccountEmail };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.False(result.Errors.Any(e => e.ErrorMessage == "PayPalAccountEmail cannot be greater than 128 characters"));
        }

        [Fact]
        public void PayPal_Account_Email_Is_Null_Payment_Options_Is_PayPal_Generates_Error()
        {
            // Arrange
            var paypalAccountEmail = "";
            var paymentOptions = new List<PaymentType> { PaymentType.PayPal };
            var model = new Event { PayPalAccountEmail = paypalAccountEmail, PaymentOptions = paymentOptions };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage == "PayPalAccountEmail is required when PaymentOptions contains the PayPal payment type"));
        }

        [Fact]
        public void PayPal_Account_Email_Is_Not_Null_Payment_Options_Is_PayPal_Is_Valid()
        {
            // Arrange
            var paypalAccountEmail = "scooper@caltech.edu";
            var paymentOptions = new List<PaymentType> { PaymentType.PayPal };
            var model = new Event { PayPalAccountEmail = paypalAccountEmail, PaymentOptions = paymentOptions };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.False(result.Errors.Any(e => e.ErrorMessage == "PayPalAccountEmail is required when PaymentOptions contains the PayPal payment type"));
        }

        // *************************************************************************************************

        [Fact]
        public void Start_Date_Is_Invalid_Iso8601_Date_Generates_Error()
        {
            // Arrange
            var startDate = new DateTime(1969, 12, 31);
            var model = new Event { StartDate = startDate };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage == "StartDate must be a in a ISO 8601 format and must be a date greater than 1/1/1970"));
        }

        [Fact]
        public void Start_Date_Is_Valid_Iso8601_Date_Is_Valid()
        {
            // Arrange
            var startDate = new DateTime(1970, 1, 1);
            var model = new Event { StartDate = startDate };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.False(result.Errors.Any(e => e.ErrorMessage == "StartDate must be a in a ISO 8601 format and must be a date greater than 1/1/1970"));
        }

        // *************************************************************************************************

        [Fact]
        public void Time_Zone_Description_Is_Longer_Than_80_Characters_Generates_Error()
        {
            // Arrange
            var timeZoneDescription = new StringBuilder();

            for (int i = 0; i < 9; i++) timeZoneDescription.Append("kkkkkkkkkk");

            var model = new Event { TimeZoneDescription = timeZoneDescription.ToString() };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage == "TimeZoneDescription cannot be greater than 80 characters"));
        }

        [Fact]
        public void Time_Zone_Description_Is_Less_Than_80_Characters_Is_Valid()
        {
            // Arrange            
            var timeZoneDescription = "Pacific Standard Time";
            var model = new Event { TimeZoneDescription = timeZoneDescription };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.False(result.Errors.Any(e => e.ErrorMessage == "TimeZoneDescription cannot be greater than 80 characters"));
        }

        // *************************************************************************************************

        [Fact]
        public void Title_Is_Null_Generates_Error()
        {
            // Arrange
            var title = "";
            var model = new Event { Title = title };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage == "Title is required"));
        }

        [Fact]
        public void Title_Is_Longer_Than_100_Characters_Generates_Error()
        {
            // Arrange
            var title = new StringBuilder();

            for (int i = 0; i < 11; i++) title.Append("kkkkkkkkkk");

            var model = new Event { Title = title.ToString() };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage == "Title cannot be longer than 100 characters"));
        }

        [Fact]
        public void Title_Is_Less_Than_100_Characters_Is_Valid()
        {
            // Arrange
            var title = "Event";
            var model = new Event { Title = title };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.False(result.Errors.Any(e => e.ErrorMessage == "Title cannot be longer than 100 characters"));
        }

        // *************************************************************************************************

        [Fact]
        public void Twitter_Hash_Tag_Is_Longer_Than_30_Characters_Generates_Error()
        {
            // Arrange
            var twitterHashTag = "#123456789012345678901234567890";
            var model = new Event { TwitterHashTag = twitterHashTag };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage == "TwitterHashTag cannot be greater than 30 characters"));
        }

        [Fact]
        public void Twitter_Hash_Tag_Is_Less_Than_30_Characters_Is_Valid()
        {
            // Arrange
            var twitterHashTag = "#Event";
            var model = new Event { TwitterHashTag = twitterHashTag };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.False(result.Errors.Any(e => e.ErrorMessage == "TwitterHashTag cannot be greater than 30 characters"));
        }

    }
}