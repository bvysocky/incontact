﻿#region License
// The MIT License (MIT)
//
// Copyright (c) 2015 Scott Lance, Ethan Tipton
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// 
// The most recent version of this license can be found at: http://opensource.org/licenses/MIT
#endregion

using InContactSdk.EventSpotEvents;
using InContactSdk.EventSpotEvents.Validation;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using Xunit;

namespace InContactTests.EventSpotEventTests.ValidationTests
{
    [ExcludeFromCodeCoverage]
    public class OnlineMeetingValidatorTests
    {
        private readonly OnlineMeetingValidator validator;

        public OnlineMeetingValidatorTests()
        {
            validator = new OnlineMeetingValidator();
        }

        // *************************************************************************************************

        [Fact]
        public void Instructions_Longer_Than_2500_Characters_Generates_Error()
        {
            var instructions = new StringBuilder();

            for (int i = 0; i < 251; i++) instructions.Append("kkkkkkkkkk");

            var model = new OnlineMeeting { Instructions = instructions.ToString() };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage == "Instructions cannot be greater than 2500 characters"));
        }

        [Fact]
        public void Instructions_Less_Than_2500_Characters_Is_Valid()
        {
            var instructions = "Instructions";
            var model = new OnlineMeeting { Instructions = instructions.ToString() };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.False(result.Errors.Any(e => e.ErrorMessage == "Instructions cannot be greater than 2500 characters"));
        }

        // *************************************************************************************************

        [Fact]
        public void Provider_Meeting_Id_Longer_Than_50_Characters_Generates_Error()
        {
            var providerMeetingId = "12345678901234567890123456789012345678901234567890123";
            var model = new OnlineMeeting { ProviderMeetingId = providerMeetingId };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage == "ProviderMeetingId cannot be greater than 50 characters"));
        }

        [Fact]
        public void Provider_Meeting_Id_Less_Than_50_Characters_Is_Valid()
        {
            var providerMeetingId = "1234567890";
            var model = new OnlineMeeting { ProviderMeetingId = providerMeetingId };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.False(result.Errors.Any(e => e.ErrorMessage == "ProviderMeetingId cannot be greater than 50 characters"));
        }

        // *************************************************************************************************

        [Fact]
        public void Provider_Type_Longer_Than_20_Characters_Generates_Error()
        {
            var providerType = "Join.Me Super Dooper Long Version";
            var model = new OnlineMeeting { ProviderType = providerType };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage == "ProviderType cannot be greater than 20 characters"));
        }

        [Fact]
        public void Provider_Type_Less_Than_20_Characters_Is_Valid()
        {
            var providerType = "Join.Me";
            var model = new OnlineMeeting { ProviderType = providerType };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.False(result.Errors.Any(e => e.ErrorMessage == "ProviderType cannot be greater than 20 characters"));
        }

        // *************************************************************************************************

        [Fact]
        public void Url_Longer_Than_250_Charaters_Generates_Error()
        {
            // Arrange
            var url = "http://join.me/000000000000000000000000111111111111111111111111111111111111222222222222222222222222222222223333333333333333333333333333333333444444444444444444444444455555555555555555555555555666666666666666666666777777777777777777777777788888888888888";
            var model = new OnlineMeeting { Url = url };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage == "Url cannot be greater than 250 characters"));
        }

        [Fact]
        public void Url_Less_Than_250_Charaters_Is_Valid()
        {
            // Arrange
            var url = "http://join.me";
            var model = new OnlineMeeting { Url = url };

            // Act
            var result = validator.Validate(model);

            // Assert
            Assert.False(result.Errors.Any(e => e.ErrorMessage == "Url cannot be greater than 250 characters"));
        }
    }
}
