﻿#region License
// The MIT License (MIT)
//
// Copyright (c) 2015 Scott Lance, Ethan Tipton
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// 
// The most recent version of this license can be found at: http://opensource.org/licenses/MIT
#endregion

using InContactSdk.EventSpotEvents;
using InContactSdk.EventSpotEvents.Models;
using InContactSdk.Helpers;
using InContactSdk.Models;
using InContactSdk.Validation;
using Moq;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Net.Http;
using System.Threading.Tasks;
using Xunit;

namespace InContactTests.EventSpotEventTests
{
    [ExcludeFromCodeCoverage]
    public class EventSpotEventsServiceTests
    {
        private readonly IInContactSettings settings;
        private readonly Mock<IValidationService> validationService;
        private readonly Mock<IWebServiceRequest> webServiceRequest;
        private readonly EventSpotEventsService service;

        public EventSpotEventsServiceTests()
        {
            settings = new InContactSettings("123456789", "D27D92A2-6A0B-4F33-BE8A-3C63E597AB18");
            validationService = new Mock<IValidationService>();
            webServiceRequest = new Mock<IWebServiceRequest>();
            service = new EventSpotEventsService(settings, validationService.Object, webServiceRequest.Object);
        }

        // *************************************************************************************************

        [Fact]
        public async Task GetEventsAsync_UsesCorrectEndpoint()
        {
            // Arrange
            Uri endpoint = null;

            webServiceRequest.Setup(m => m.GetDeserializedAsync<PaginatedResult<EventResponse>>(It.IsAny<Uri>(), It.IsAny<string>()))
                .Callback((Uri a, string b) => endpoint = a)
                .ReturnsAsync(new PaginatedResult<EventResponse>());

            // Act
            await service.GetEventsAsync();

            // Assert
            Assert.Equal(string.Format("{0}eventspot/events?limit=50&api_key={1}", settings.BaseUrl, settings.ApiKey), endpoint.AbsoluteUri);
        }

        [Fact]
        public async Task GetEventsAsync_Verify_WebServiceRequest_MakeRequest()
        {
            // Act
            await service.GetEventsAsync();

            // Assert
            webServiceRequest.Verify(m => m.GetDeserializedAsync<PaginatedResult<EventResponse>>(It.IsAny<Uri>(), It.IsAny<string>()), Times.Once);
        }

        // *************************************************************************************************

        [Fact]
        public async Task CreateEventAsync_UsesCorrectEndpoint()
        {
            // Arrange
            Uri endpoint = null;

            webServiceRequest.Setup(m => m.PostDeserializedAsync<Event, Event>(It.IsAny<Uri>(), It.IsAny<string>(), It.IsAny<Event>()))
                .Callback((Uri a, string b, Event c) => endpoint = a)
                .ReturnsAsync(new Event());

            // Act
            await service.CreateEventAsync(new Event());

            // Assert
            Assert.Equal(string.Format("{0}eventspot/events?api_key={1}", settings.BaseUrl, settings.ApiKey), endpoint.AbsoluteUri);
        }

        [Fact]
        public async Task CreateEventAsync_Verify_WebServiceRequest_MakeRequest()
        {
            // Act
            await service.CreateEventAsync(new Event());

            // Assert
            webServiceRequest.Verify(m => m.PostDeserializedAsync<Event, Event>(It.IsAny<Uri>(), It.IsAny<string>(), It.IsAny<Event>()), Times.Once);
        }

        // *************************************************************************************************

        [Fact]
        public async Task GetEventAsync_UsesCorrectEndpoint()
        {
            // Arrange
            var eventId = "12345";
            Uri endpoint = null;

            webServiceRequest.Setup(m => m.GetDeserializedAsync<Event>(It.IsAny<Uri>(), It.IsAny<string>()))
                .Callback((Uri a, string b) => endpoint = a)
                .ReturnsAsync(new Event());

            // Act
            await service.GetEventAsync(eventId);

            // Assert
            Assert.Equal(string.Format("{0}eventspot/events/12345?api_key={1}", settings.BaseUrl, settings.ApiKey), endpoint.AbsoluteUri);
        }

        [Fact]
        public async Task GetEventAsync_Verify_WebServiceRequest_MakeRequest()
        {
            // Arrange
            var eventId = "12345";

            // Act
            await service.GetEventAsync(eventId);

            // Assert
            webServiceRequest.Verify(m => m.GetDeserializedAsync<Event>(It.IsAny<Uri>(), It.IsAny<string>()), Times.Once);
        }

        // *************************************************************************************************

        [Fact]
        public async Task GetEventFeesAsync_UsesCorrectEndpoint()
        {
            // Arrange
            var eventId = "12345";
            Uri endpoint = null;

            webServiceRequest.Setup(m => m.GetDeserializedAsync<IEnumerable<EventFee>>(It.IsAny<Uri>(), It.IsAny<string>()))
                .Callback((Uri a, string b) => endpoint = a)
                .ReturnsAsync(new EventFee[0]);

            // Act
            await service.GetEventFeesAsync(eventId);

            // Assert
            Assert.Equal(string.Format("{0}eventspot/events/12345/fees?api_key={1}", settings.BaseUrl, settings.ApiKey), endpoint.AbsoluteUri);
        }

        [Fact]
        public async Task GetEventFeesAsync_Verify_WebServiceRequest_MakeRequest()
        {
            // Arrange
            var eventId = "12345";

            // Act
            await service.GetEventFeesAsync(eventId);

            // Assert
            webServiceRequest.Verify(m => m.GetDeserializedAsync<IEnumerable<EventFee>>(It.IsAny<Uri>(), It.IsAny<string>()), Times.Once);
        }

        // ************************************************************************************************* 

        [Fact]
        public async Task CreateEventFeeAsync_UsesCorrectEndpoint()
        {
            // Arrange
            var eventId = "12345";
            var eventFee = new EventFee();
            Uri endpoint = null;

            webServiceRequest.Setup(m => m.PostDeserializedAsync<EventFee, EventFee>(It.IsAny<Uri>(), It.IsAny<string>(), It.IsAny<EventFee>()))
                .Callback((Uri a, string b, EventFee c) => endpoint = a)
                .ReturnsAsync(new EventFee());

            // Act
            await service.CreateEventFeeAsync(eventId, eventFee);

            // Assert
            Assert.Equal(string.Format("{0}eventspot/events/12345/fees?api_key={1}", settings.BaseUrl, settings.ApiKey), endpoint.AbsoluteUri);
        }

        [Fact]
        public async Task CreateEventFeeAsync_Verify_WebServiceRequest_MakeRequest()
        {
            // Arrange
            var eventId = "12345";
            var eventFee = new EventFee();

            // Act
            await service.CreateEventFeeAsync(eventId, eventFee);

            // Assert
            webServiceRequest.Verify(m => m.PostDeserializedAsync<EventFee, EventFee>(It.IsAny<Uri>(), It.IsAny<string>(), It.IsAny<EventFee>()), Times.Once);
        }

        // *************************************************************************************************

        [Fact]
        public async Task GetEventFeeAsync_UsesCorrectEndpoint()
        {
            // Arrange
            var eventId = "12345";
            var feeId = "12345";
            Uri endpoint = null;

            webServiceRequest.Setup(m => m.GetDeserializedAsync<EventFee>(It.IsAny<Uri>(), It.IsAny<string>()))
                .Callback((Uri a, string b) => endpoint = a)
                .ReturnsAsync(new EventFee());

            // Act
            await service.GetEventFeeAsync(eventId, feeId);

            // Assert
            Assert.Equal(string.Format("{0}eventspot/events/12345/fees/12345?api_key={1}", settings.BaseUrl, settings.ApiKey), endpoint.AbsoluteUri);
        }

        [Fact]
        public async Task GetEventFeeAsync_Verify_WebServiceRequest_MakeRequest()
        {
            // Arrange
            var eventId = "12345";
            var feeId = "12345";

            // Act
            await service.GetEventFeeAsync(eventId, feeId);

            // Assert
            webServiceRequest.Verify(m => m.GetDeserializedAsync<EventFee>(It.IsAny<Uri>(), It.IsAny<string>()), Times.Once);
        }

        // *************************************************************************************************

        [Fact]
        public async Task UpdateEventFeeAsync_UsesCorrectEndpoint()
        {
            // Arrange
            var eventId = "12345";
            var feeId = "12345";
            var eventFee = new EventFee();
            Uri endpoint = null;

            webServiceRequest.Setup(m => m.PutDeserializedAsync<EventFee, EventFee>(It.IsAny<Uri>(), It.IsAny<string>(), It.IsAny<EventFee>()))
                .Callback((Uri a, string b, EventFee c) => endpoint = a)
                .ReturnsAsync(new EventFee());

            // Act
            await service.UpdateEventFeeAsync(eventId, feeId, eventFee);

            // Assert
            Assert.Equal(string.Format("{0}eventspot/events/12345/fees/12345?api_key={1}", settings.BaseUrl, settings.ApiKey), endpoint.AbsoluteUri);
        }

        [Fact]
        public async Task UpdateEventFeeAsync_Verify_WebServiceRequest_MakeRequest()
        {
            // Arrange
            var eventId = "12345";
            var feeId = "12345";
            var eventFee = new EventFee();

            // Act
            await service.UpdateEventFeeAsync(eventId, feeId, eventFee);

            // Assert
            webServiceRequest.Verify(m => m.PutDeserializedAsync<EventFee, EventFee>(It.IsAny<Uri>(), It.IsAny<string>(), It.IsAny<EventFee>()), Times.Once);
        }

        // *************************************************************************************************

        [Fact]
        public async Task DeleteEventFeeAsync_UsesCorrectEndpoint()
        {
            // Arrange
            var eventId = "12345";
            var feeId = "12345";
            Uri endpoint = null;

            webServiceRequest.Setup(m => m.DeleteAsync(It.IsAny<Uri>(), It.IsAny<string>()))
                .Callback((Uri a, string b) => endpoint = a)
                .ReturnsAsync(new HttpResponseMessage());

            // Act
            await service.DeleteEventFeeAsync(eventId, feeId);

            // Assert
            Assert.Equal(string.Format("{0}eventspot/events/12345/fees/12345?api_key={1}", settings.BaseUrl, settings.ApiKey), endpoint.AbsoluteUri);
        }

        [Fact]
        public async Task DeleteEventFeeAsync_Verify_WebServiceRequest_MakeRequest()
        {
            // Arrange
            var eventId = "12345";
            var feeId = "12345";

            // Act
            await service.DeleteEventFeeAsync(eventId, feeId);

            // Assert
            webServiceRequest.Verify(m => m.DeleteAsync(It.IsAny<Uri>(), It.IsAny<string>()), Times.Once);
        }

        // *************************************************************************************************

        [Fact]
        public async Task CreatePromoCodeAsync_DiscountAmount_UsesCorrectEndpoint()
        {
            // Arrange
            var eventId = "12345";
            var promoCodeDiscountAmount = new DiscountAmountPromoCode();
            Uri endpoint = null;

            webServiceRequest.Setup(m => m.PostDeserializedAsync<DiscountAmountPromoCode, PromoCodeResponse>(It.IsAny<Uri>(), It.IsAny<string>(), It.IsAny<DiscountAmountPromoCode>()))
                .Callback((Uri a, string b, DiscountAmountPromoCode c) => endpoint = a)
                .ReturnsAsync(new PromoCodeResponse());

            // Act
            await service.CreatePromoCodeAsync<DiscountAmountPromoCode>(eventId, promoCodeDiscountAmount);

            // Assert
            Assert.Equal(string.Format("{0}eventspot/events/12345/promocodes?api_key={1}", settings.BaseUrl, settings.ApiKey), endpoint.AbsoluteUri);
        }

        [Fact]
        public async Task CreatePromoCodeAsync_PromoCodeDiscountAmount_Verify_WebServiceRequest_MakeRequest()
        {
            // Arrange
            var eventId = "12345";
            var promoCodeDiscountAmount = new DiscountAmountPromoCode();

            // Act
            await service.CreatePromoCodeAsync<DiscountAmountPromoCode>(eventId, promoCodeDiscountAmount);

            // Assert
            webServiceRequest.Verify(m => m.PostDeserializedAsync<DiscountAmountPromoCode, PromoCodeResponse>(It.IsAny<Uri>(), It.IsAny<string>(), It.IsAny<DiscountAmountPromoCode>()), Times.Once);
        }

        [Fact]
        public async Task CreatePromoCodeAsync_DiscountPercent_UsesCorrectEndpoint()
        {
            // Arrange
            var eventId = "12345";
            var promoCodeDiscountPercent = new DiscountPercentPromoCode();
            Uri endpoint = null;

            webServiceRequest.Setup(m => m.PostDeserializedAsync<DiscountPercentPromoCode, PromoCodeResponse>(It.IsAny<Uri>(), It.IsAny<string>(), It.IsAny<DiscountPercentPromoCode>()))
                .Callback((Uri a, string b, DiscountPercentPromoCode c) => endpoint = a)
                .ReturnsAsync(new PromoCodeResponse());

            // Act
            await service.CreatePromoCodeAsync<DiscountPercentPromoCode>(eventId, promoCodeDiscountPercent);

            // Assert
            Assert.Equal(string.Format("{0}eventspot/events/12345/promocodes?api_key={1}", settings.BaseUrl, settings.ApiKey), endpoint.AbsoluteUri);
        }

        [Fact]
        public async Task CreatePromoCodeAsync_PromoCodeDiscountPercent_Verify_WebServiceRequest_MakeRequest()
        {
            // Arrange
            var eventId = "12345";
            var promoCodeDiscountPercent = new DiscountPercentPromoCode();

            // Act
            await service.CreatePromoCodeAsync<DiscountPercentPromoCode>(eventId, promoCodeDiscountPercent);

            // Assert
            webServiceRequest.Verify(m => m.PostDeserializedAsync<DiscountPercentPromoCode, PromoCodeResponse>(It.IsAny<Uri>(), It.IsAny<string>(), It.IsAny<DiscountPercentPromoCode>()), Times.Once);
        }

        // *************************************************************************************************

        [Fact]
        public async Task GetDeserializedAsync_UsesCorrectEndpoint()
        {
            // Arrange
            var eventId = "12345";
            Uri endpoint = null;

            webServiceRequest.Setup(m => m.GetDeserializedAsync<IEnumerable<PromoCodeResponse>>(It.IsAny<Uri>(), It.IsAny<string>()))
                .Callback((Uri a, string b) => endpoint = a)
                .ReturnsAsync(new PromoCodeResponse[0]);

            // Act
            await service.GetPromoCodesAsync(eventId);

            // Assert
            Assert.Equal(string.Format("{0}eventspot/events/12345/promocodes?api_key={1}", settings.BaseUrl, settings.ApiKey), endpoint.AbsoluteUri);
        }

        [Fact]
        public async Task GetPromoCodesAsync_Verify_WebServiceRequest_MakeRequest()
        {
            // Arrange
            var eventId = "12345";

            // Act
            await service.GetPromoCodesAsync(eventId);

            // Assert
            webServiceRequest.Verify(m => m.GetDeserializedAsync<IEnumerable<PromoCodeResponse>>(It.IsAny<Uri>(), It.IsAny<string>()), Times.Once);
        }

        // *************************************************************************************************

        [Fact]
        public async Task GetPromoCodeAsync_UsesCorrectEndpoint()
        {
            // Arrange
            var eventId = "12345";
            var promoCodeId = "123456";
            Uri endpoint = null;

            webServiceRequest.Setup(m => m.GetDeserializedAsync<PromoCodeResponse>(It.IsAny<Uri>(), It.IsAny<string>()))
                .Callback((Uri a, string b) => endpoint = a)
                .ReturnsAsync(new PromoCodeResponse());

            // Act
            await service.GetPromoCodeAsync(eventId, promoCodeId);

            // Assert
            Assert.Equal(string.Format("{0}eventspot/events/12345/promocodes/123456?api_key={1}", settings.BaseUrl, settings.ApiKey), endpoint.AbsoluteUri);
        }

        [Fact]
        public async Task GetPromoCodeAsync_Verify_WebServiceRequest_MakeRequest()
        {
            // Arrange
            var eventId = "12345";
            var promoCodeId = "123456";

            // Act
            await service.GetPromoCodeAsync(eventId, promoCodeId);

            // Assert
            webServiceRequest.Verify(m => m.GetDeserializedAsync<PromoCodeResponse>(It.IsAny<Uri>(), It.IsAny<string>()), Times.Once);
        }

        // *************************************************************************************************

        [Fact]
        public async Task UpdatePromoCodeAsync_UsesCorrectEndpoint()
        {
            // Arrange
            var eventId = "12345";
            var promoCodeId = "123456";
            var promoCode = new DiscountAmountPromoCode();
            Uri endpoint = null;

            webServiceRequest.Setup(m => m.PutDeserializedAsync<DiscountAmountPromoCode, PromoCodeResponse>(It.IsAny<Uri>(), It.IsAny<string>(), It.IsAny<DiscountAmountPromoCode>()))
                .Callback((Uri a, string b, DiscountAmountPromoCode c) => endpoint = a)
                .ReturnsAsync(new PromoCodeResponse());

            // Act
            await service.UpdatePromoCodeAsync(eventId, promoCodeId, promoCode);

            // Assert
            Assert.Equal(string.Format("{0}eventspot/events/12345/promocodes/123456?api_key={1}", settings.BaseUrl, settings.ApiKey), endpoint.AbsoluteUri);
        }

        [Fact]
        public async Task UpdatePromoCodeAsync_Verify_WebServiceRequest_MakeRequest()
        {
            // Arrange
            var eventId = "12345";
            var promoCodeId = "123456";
            var promoCode = new DiscountAmountPromoCode();

            // Act
            await service.UpdatePromoCodeAsync(eventId, promoCodeId, promoCode);

            // Assert
            webServiceRequest.Verify(m => m.PutDeserializedAsync<DiscountAmountPromoCode, PromoCodeResponse>(It.IsAny<Uri>(), It.IsAny<string>(), It.IsAny<DiscountAmountPromoCode>()), Times.Once);
        }

        // *************************************************************************************************

        [Fact]
        public async Task DeletePromoCodeAsync_UsesCorrectEndpoint()
        {
            // Arrange
            var eventId = "12345";
            var promoCodeId = "123456";
            Uri endpoint = null;

            webServiceRequest.Setup(m => m.DeleteAsync(It.IsAny<Uri>(), It.IsAny<string>()))
                .Callback((Uri a, string b) => endpoint = a)
                .ReturnsAsync(new HttpResponseMessage());

            // Act
            await service.DeletePromoCodeAsync(eventId, promoCodeId);

            // Assert
            Assert.Equal(string.Format("{0}eventspot/events/12345/promocodes/123456?api_key={1}", settings.BaseUrl, settings.ApiKey), endpoint.AbsoluteUri);
        }

        [Fact]
        public async Task DeletePromoCodeAsync_Verify_WebServiceRequest_MakeRequest()
        {
            // Arrange
            var eventId = "12345";
            var promoCodeId = "123456";

            // Act
            await service.DeletePromoCodeAsync(eventId, promoCodeId);

            // Assert
            webServiceRequest.Verify(m => m.DeleteAsync(It.IsAny<Uri>(), It.IsAny<string>()), Times.Once);
        }

        // *************************************************************************************************

        [Fact]
        public async Task GetEventRegistrantAsync_UsesCorrectEndpoint()
        {
            // Arrange
            var eventId = "12345";
            var registrantId = "123456";
            Uri endpoint = null;

            webServiceRequest.Setup(m => m.GetDeserializedAsync<EventRegistrant>(It.IsAny<Uri>(), It.IsAny<string>()))
                .Callback((Uri a, string b) => endpoint = a)
                .ReturnsAsync(new EventRegistrant());

            // Act
            await service.GetEventRegistrantAsync(eventId, registrantId);

            // Assert
            Assert.Equal(string.Format("{0}eventspot/events/12345/registrants/123456?api_key={1}", settings.BaseUrl, settings.ApiKey), endpoint.AbsoluteUri);
        }

        [Fact]
        public async Task GetEventRegistrantAsync_Verify_WebServiceRequest_MakeRequest()
        {
            // Arrange
            var eventId = "12345";
            var registrantId = "123456";

            // Act
            await service.GetEventRegistrantAsync(eventId, registrantId);

            // Assert
            webServiceRequest.Verify(m => m.GetDeserializedAsync<EventRegistrant>(It.IsAny<Uri>(), It.IsAny<string>()), Times.Once);
        }

        // *************************************************************************************************

        [Fact]
        public async Task GetEventRegistrantsAsync_UsesCorrectEndpoint()
        {
            // Arrange
            var eventId = "12345";
            Uri endpoint = null;

            webServiceRequest.Setup(m => m.GetDeserializedAsync<PaginatedResult<EventRegistrant>>(It.IsAny<Uri>(), It.IsAny<string>()))
                .Callback((Uri a, string b) => endpoint = a)
                .ReturnsAsync(new PaginatedResult<EventRegistrant>());

            // Act
            await service.GetEventRegistrantsAsync(eventId);

            // Assert
            Assert.Equal(string.Format("{0}eventspot/events/12345/registrants?api_key={1}", settings.BaseUrl, settings.ApiKey), endpoint.AbsoluteUri);
        }

        [Fact]
        public async Task GetEventRegistrantsAsync_Verify_WebServiceRequest_MakeRequest()
        {
            // Arrange
            var eventId = "12345";

            // Act
            await service.GetEventRegistrantsAsync(eventId);

            // Assert
            webServiceRequest.Verify(m => m.GetDeserializedAsync<PaginatedResult<EventRegistrant>>(It.IsAny<Uri>(), It.IsAny<string>()), Times.Once);
        }

        // *************************************************************************************************

        [Fact]
        public async Task CreateItemAsync_UsesCorrectEndpoint()
        {
            // Arrange
            var eventId = "12345";
            var item = new Item();
            Uri endpoint = null;

            webServiceRequest.Setup(m => m.PostDeserializedAsync<Item, Item>(It.IsAny<Uri>(), It.IsAny<string>(), It.IsAny<Item>()))
                .Callback((Uri a, string b, Item c) => endpoint = a)
                .ReturnsAsync(new Item());

            // Act
            await service.CreateItemAsync(eventId, item);

            // Assert
            Assert.Equal(string.Format("{0}eventspot/events/12345/items?api_key={1}", settings.BaseUrl, settings.ApiKey), endpoint.AbsoluteUri);
        }

        [Fact]
        public async Task CreateItemAsync_Verify_WebServiceRequest_MakeRequest()
        {
            // Arrange
            var eventId = "12345";
            var item = new Item();

            // Act
            await service.CreateItemAsync(eventId, item);

            // Assert
            webServiceRequest.Verify(m => m.PostDeserializedAsync<Item, Item>(It.IsAny<Uri>(), It.IsAny<string>(), It.IsAny<Item>()), Times.Once);
        }

        // *************************************************************************************************

        [Fact]
        public async Task GetItemsAsync_UsesCorrectEndpoint()
        {
            // Arrange
            var eventId = "12345";
            Uri endpoint = null;

            webServiceRequest.Setup(m => m.GetDeserializedAsync<IEnumerable<Item>>(It.IsAny<Uri>(), It.IsAny<string>()))
                .Callback((Uri a, string b) => endpoint = a)
                .ReturnsAsync(new Item[0]);

            // Act
            await service.GetItemsAsync(eventId);

            // Assert
            Assert.Equal(string.Format("{0}eventspot/events/12345/items?api_key={1}", settings.BaseUrl, settings.ApiKey), endpoint.AbsoluteUri);
        }

        [Fact]
        public async Task GetItemsAsync_Verify_WebServiceRequest_MakeRequest()
        {
            // Arrange
            var eventId = "12345";

            // Act
            await service.GetItemsAsync(eventId);

            // Assert
            webServiceRequest.Verify(m => m.GetDeserializedAsync<IEnumerable<Item>>(It.IsAny<Uri>(), It.IsAny<string>()), Times.Once);
        }

        // *************************************************************************************************

        [Fact]
        public async Task GetItemAsync_UsesCorrectEndpoint()
        {
            // Arrange
            var eventId = "12345";
            var itemId = "12345";
            Uri endpoint = null;

            webServiceRequest.Setup(m => m.GetDeserializedAsync<Item>(It.IsAny<Uri>(), It.IsAny<string>()))
                .Callback((Uri a, string b) => endpoint = a)
                .ReturnsAsync(new Item());

            // Act
            await service.GetItemAsync(eventId, itemId);

            // Assert
            Assert.Equal(string.Format("{0}eventspot/events/12345/items/12345?api_key={1}", settings.BaseUrl, settings.ApiKey), endpoint.AbsoluteUri);
        }

        [Fact]
        public async Task GetItemAsync_Verify_WebServiceRequest_MakeRequest()
        {
            // Arrange
            var eventId = "12345";
            var itemId = "12345";

            // Act
            await service.GetItemAsync(eventId, itemId);

            // Assert
            webServiceRequest.Verify(m => m.GetDeserializedAsync<Item>(It.IsAny<Uri>(), It.IsAny<string>()), Times.Once);
        }

        // *************************************************************************************************

        [Fact]
        public async Task UpdateItemAsync_UsesCorrectEndpoint()
        {
            // Arrange
            var eventId = "12345";
            var itemId = "12345";
            var item = new Item();
            Uri endpoint = null;

            webServiceRequest.Setup(m => m.PutDeserializedAsync<Item, Item>(It.IsAny<Uri>(), It.IsAny<string>(), It.IsAny<Item>()))
                .Callback((Uri a, string b, Item c) => endpoint = a)
                .ReturnsAsync(new Item());

            // Act
            await service.UpdateItemAsync(eventId, itemId, item);

            // Assert
            Assert.Equal(string.Format("{0}eventspot/events/12345/items/12345?api_key={1}", settings.BaseUrl, settings.ApiKey), endpoint.AbsoluteUri);
        }

        [Fact]
        public async Task UpdateItemAsync_Verify_WebServiceRequest_MakeRequest()
        {
            // Arrange
            var eventId = "12345";
            var itemId = "12345";
            var item = new Item();

            // Act
            await service.UpdateItemAsync(eventId, itemId, item);

            // Assert
            webServiceRequest.Verify(m => m.PutDeserializedAsync<Item, Item>(It.IsAny<Uri>(), It.IsAny<string>(), It.IsAny<Item>()), Times.Once);
        }

        // *************************************************************************************************

        [Fact]
        public async Task DeleteItemAsync_UsesCorrectEndpoint()
        {
            // Arrange
            var eventId = "12345";
            var itemId = "12345";
            Uri endpoint = null;

            webServiceRequest.Setup(m => m.DeleteAsync(It.IsAny<Uri>(), It.IsAny<string>()))
                .Callback((Uri a, string b) => endpoint = a)
                .ReturnsAsync(new HttpResponseMessage());

            // Act
            await service.DeleteItemAsync(eventId, itemId);

            // Assert
            Assert.Equal(string.Format("{0}eventspot/events/12345/items/12345?api_key={1}", settings.BaseUrl, settings.ApiKey), endpoint.AbsoluteUri);
        }

        [Fact]
        public async Task DeleteItemAsync_Verify_WebServiceRequest_MakeRequest()
        {
            // Arrange
            var eventId = "12345";
            var itemId = "12345";

            // Act
            await service.DeleteItemAsync(eventId, itemId);

            // Assert
            webServiceRequest.Verify(m => m.DeleteAsync(It.IsAny<Uri>(), It.IsAny<string>()), Times.Once);
        }

        // *************************************************************************************************

        [Fact]
        public async Task GetItemAttributesAsync_UsesCorrectEndpoint()
        {
            // Arrange
            var eventId = "12345";
            var itemId = "12345";
            Uri endpoint = null;

            webServiceRequest.Setup(m => m.GetDeserializedAsync<IEnumerable<ItemAttribute>>(It.IsAny<Uri>(), It.IsAny<string>()))
                .Callback((Uri a, string b) => endpoint = a)
                .ReturnsAsync(new ItemAttribute[0]);

            // Act
            await service.GetItemAttributesAsync(eventId, itemId);

            // Assert
            Assert.Equal(string.Format("{0}eventspot/events/12345/items/12345/attributes?api_key={1}", settings.BaseUrl, settings.ApiKey), endpoint.AbsoluteUri);
        }

        [Fact]
        public async Task GetItemAttributesAsync_Verify_WebServiceRequest_MakeRequest()
        {
            // Arrange
            var eventId = "12345";
            var itemId = "12345";

            // Act
            await service.GetItemAttributesAsync(eventId, itemId);

            // Assert
            webServiceRequest.Verify(m => m.GetDeserializedAsync<IEnumerable<ItemAttribute>>(It.IsAny<Uri>(), It.IsAny<string>()), Times.Once);
        }

        // *************************************************************************************************

        [Fact]
        public async Task CreateItemAttributeAsync_UsesCorrectEndpoint()
        {
            // Arrange
            var eventId = "12345";
            var itemId = "12345";
            var itemAttribute = new ItemAttribute();
            Uri endpoint = null;

            webServiceRequest.Setup(m => m.PostDeserializedAsync<ItemAttribute, ItemAttribute>(It.IsAny<Uri>(), It.IsAny<string>(), It.IsAny<ItemAttribute>()))
                .Callback((Uri a, string b, ItemAttribute c) => endpoint = a)
                .ReturnsAsync(new ItemAttribute());

            // Act
            await service.CreateItemAttributeAsync(eventId, itemId, itemAttribute);

            // Assert
            Assert.Equal(string.Format("{0}eventspot/events/12345/items/12345/attributes?api_key={1}", settings.BaseUrl, settings.ApiKey), endpoint.AbsoluteUri);
        }

        [Fact]
        public async Task CreateItemAttributeAsync_Verify_WebServiceRequest_MakeRequest()
        {
            // Arrange
            var eventId = "12345";
            var itemId = "12345";
            var itemAttribute = new ItemAttribute();

            // Act
            await service.CreateItemAttributeAsync(eventId, itemId, itemAttribute);

            // Assert
            webServiceRequest.Verify(m => m.PostDeserializedAsync<ItemAttribute, ItemAttribute>(It.IsAny<Uri>(), It.IsAny<string>(), It.IsAny<ItemAttribute>()), Times.Once);
        }

        // *************************************************************************************************

        [Fact]
        public async Task GetItemAttributeAsync_UsesCorrectEndpoint()
        {
            // Arrange
            var eventId = "12345";
            var itemId = "12345";
            var attributeId = "123455";
            Uri endpoint = null;

            webServiceRequest.Setup(m => m.GetDeserializedAsync<ItemAttribute>(It.IsAny<Uri>(), It.IsAny<string>()))
                .Callback((Uri a, string b) => endpoint = a)
                .ReturnsAsync(new ItemAttribute());

            // Act
            await service.GetItemAttributeAsync(eventId, itemId, attributeId);

            // Assert
            Assert.Equal(string.Format("{0}eventspot/events/12345/items/12345/attributes/123455?api_key={1}", settings.BaseUrl, settings.ApiKey), endpoint.AbsoluteUri);
        }

        [Fact]
        public async Task GetItemAttributeAsync_Verify_WebServiceRequest_MakeRequest()
        {
            // Arrange
            var eventId = "12345";
            var itemId = "12345";
            var attributeId = "123455";

            // Act
            await service.GetItemAttributeAsync(eventId, itemId, attributeId);

            // Assert
            webServiceRequest.Verify(m => m.GetDeserializedAsync<ItemAttribute>(It.IsAny<Uri>(), It.IsAny<string>()), Times.Once);
        }

        // *************************************************************************************************

        [Fact]
        public async Task UpdateItemAttributeAsync_UsesCorrectEndpoint()
        {
            // Arrange
            var eventId = "12345";
            var itemId = "12345";
            var attributeId = "123455";
            var itemAttribute = new ItemAttribute();
            Uri endpoint = null;

            webServiceRequest.Setup(m => m.PutDeserializedAsync<ItemAttribute, ItemAttribute>(It.IsAny<Uri>(), It.IsAny<string>(), It.IsAny<ItemAttribute>()))
                .Callback((Uri a, string b, ItemAttribute c) => endpoint = a)
                .ReturnsAsync(new ItemAttribute());

            // Act
            await service.UpdateItemAttributeAsync(eventId, itemId, attributeId, itemAttribute);

            // Assert
            Assert.Equal(string.Format("{0}eventspot/events/12345/items/12345/attributes/123455?api_key={1}", settings.BaseUrl, settings.ApiKey), endpoint.AbsoluteUri);
        }

        [Fact]
        public async Task UpdateItemAttributeAsync_Verify_WebServiceRequest_MakeRequest()
        {
            // Arrange
            var eventId = "12345";
            var itemId = "12345";
            var attributeId = "123455";
            var itemAttribute = new ItemAttribute();

            // Act
            await service.UpdateItemAttributeAsync(eventId, itemId, attributeId, itemAttribute);

            // Assert
            webServiceRequest.Verify(m => m.PutDeserializedAsync<ItemAttribute, ItemAttribute>(It.IsAny<Uri>(), It.IsAny<string>(), It.IsAny<ItemAttribute>()), Times.Once);
        }

        // *************************************************************************************************

        [Fact]
        public async Task DeleteItemAttributeAsync_UsesCorrectEndpoint()
        {
            // Arrange
            var eventId = "12345";
            var itemId = "12345";
            var attributeId = "12345";
            Uri endpoint = null;

            webServiceRequest.Setup(m => m.DeleteAsync(It.IsAny<Uri>(), It.IsAny<string>()))
                .Callback((Uri a, string b) => endpoint = a)
                .ReturnsAsync(new HttpResponseMessage());

            // Act
            await service.DeleteItemAttributeAsync(eventId, itemId, attributeId);

            // Assert
            Assert.Equal(string.Format("{0}eventspot/events/12345/items/12345/attributes/12345?api_key={1}", settings.BaseUrl, settings.ApiKey), endpoint.AbsoluteUri);
        }

        [Fact]
        public async Task DeleteItemAttributeAsync_Verify_WebServiceRequest_MakeRequest()
        {
            // Arrange
            var eventId = "12345";
            var itemId = "12345";
            var attributeId = "12345";

            // Act
            await service.DeleteItemAttributeAsync(eventId, itemId, attributeId);

            // Assert
            webServiceRequest.Verify(m => m.DeleteAsync(It.IsAny<Uri>(), It.IsAny<string>()), Times.Once);
        }

        // *************************************************************************************************

        [Fact]
        public async Task CancelEventAsync_UsesCorrectEndpoint()
        {
            // Arrange
            var eventId = "12345";
            Uri endpoint = null;

            webServiceRequest.Setup(m => m.PatchDeserializedAsync<IEnumerable<PublishCancelModel>, Event>(It.IsAny<Uri>(), It.IsAny<string>(), It.IsAny<IEnumerable<PublishCancelModel>>()))
                .Callback((Uri a, string b, IEnumerable<PublishCancelModel> c) => endpoint = a)
                .ReturnsAsync(new Event());

            // Act
            await service.CancelEventAsync(eventId);

            // Assert
            Assert.Equal(string.Format("{0}eventspot/events/12345?api_key={1}", settings.BaseUrl, settings.ApiKey), endpoint.AbsoluteUri);
        }

        [Fact]
        public async Task CancelEventAsync_Verify_WebServiceRequest_MakeRequest()
        {
            // Arrange
            var eventId = "12345";

            // Act
            await service.CancelEventAsync(eventId);

            // Assert
            webServiceRequest.Verify(m => m.PatchDeserializedAsync<IEnumerable<PublishCancelModel>, Event>(It.IsAny<Uri>(), It.IsAny<string>(), It.IsAny<IEnumerable<PublishCancelModel>>()), Times.Once);
        }

        // *************************************************************************************************

        [Fact]
        public async Task PublishEventAsync_UsesCorrectEndpoint()
        {
            // Arrange
            var eventId = "12345";
            Uri endpoint = null;

            webServiceRequest.Setup(m => m.PatchDeserializedAsync<IEnumerable<PublishCancelModel>, Event>(It.IsAny<Uri>(), It.IsAny<string>(), It.IsAny<IEnumerable<PublishCancelModel>>()))
                .Callback((Uri a, string b, IEnumerable<PublishCancelModel> c) => endpoint = a)
                .ReturnsAsync(new Event());

            // Act
            await service.PublishEventAsync(eventId);

            // Assert
            Assert.Equal(string.Format("{0}eventspot/events/12345?api_key={1}", settings.BaseUrl, settings.ApiKey), endpoint.AbsoluteUri);
        }

        [Fact]
        public async Task PublishEventAsync_Verify_WebServiceRequest_MakeRequest()
        {
            // Arrange
            var eventId = "12345";

            // Act
            await service.PublishEventAsync(eventId);

            // Assert
            webServiceRequest.Verify(m => m.PatchDeserializedAsync<IEnumerable<PublishCancelModel>, Event>(It.IsAny<Uri>(), It.IsAny<string>(), It.IsAny<IEnumerable<PublishCancelModel>>()), Times.Once);
        }

        // *************************************************************************************************

        [Fact]
        public async Task UpdateEventAsync_UsesCorrectEndpoint()
        {
            // Arrange
            var eventId = "12345";
            var eventSpotEvent = new Event();
            Uri endpoint = null;

            webServiceRequest.Setup(m => m.PutDeserializedAsync<Event, Event>(It.IsAny<Uri>(), It.IsAny<string>(), It.IsAny<Event>()))
                .Callback((Uri a, string b, Event c) => endpoint = a)
                .ReturnsAsync(new Event());

            // Act
            await service.UpdateEventAsync(eventId, eventSpotEvent);

            // Assert
            Assert.Equal(string.Format("{0}eventspot/events/12345?api_key={1}", settings.BaseUrl, settings.ApiKey), endpoint.AbsoluteUri);
        }

        [Fact]
        public async Task UpdateEventAsync_Verify_WebServiceRequest_MakeRequest()
        {
            // Arrange
            var eventId = "12345";
            var eventSpotEvent = new Event();

            // Act
            await service.UpdateEventAsync(eventId, eventSpotEvent);

            // Assert
            webServiceRequest.Verify(m => m.PutDeserializedAsync<Event, Event>(It.IsAny<Uri>(), It.IsAny<string>(), It.IsAny<Event>()), Times.Once);
        }

        // *************************************************************************************************

    }
}