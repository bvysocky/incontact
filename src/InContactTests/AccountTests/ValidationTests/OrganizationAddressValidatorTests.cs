﻿#region License
// The MIT License (MIT)
//
// Copyright (c) 2015 Scott Lance, Ethan Tipton
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// 
// The most recent version of this license can be found at: http://opensource.org/licenses/MIT
#endregion

using InContactSdk.Account;
using InContactSdk.Account.Validation;
using InContactSdk.Helpers;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Xunit;

namespace InContactTests.AccountTests.ValidationTests
{
    [ExcludeFromCodeCoverage]
    public class OrganizationAddressValidatorTests
    {
        private readonly OrganizationAddressValidator validator;

        public OrganizationAddressValidatorTests()
        {
            validator = new OrganizationAddressValidator();
        }

        // *************************************************************************************************

        [Fact]
        public void City_Is_Null_Generates_Error()
        {
            // Arrange
            var organizationAddress = new OrganizationAddress
            {
                Line1 = "California Institute of Technology",
                PostalCode = "91125",
            };

            // Act
            var result = validator.Validate(organizationAddress);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage == "City is required"));
        }

        // *************************************************************************************************

        [Fact]
        public void State_Is_Entered_With_Country_Code_Generates_Error()
        {
            // Arrange
            var organizationAddress = new OrganizationAddress
            {
                Line1 = "California Institute of Technology",
                City = "Pasadena",
                PostalCode = "91125",
                State = "CA",
                CountryCode = CountryCode.US,

            };
            var validator = new OrganizationAddressValidator();

            // Act
            var result = validator.Validate(organizationAddress);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage == "State cannot be entered if the CountryCode is US or CA"));
        }

        // *************************************************************************************************

        [Fact]
        public void Country_Code_Is_Not_US_or_CA_State_Code_Is_Invalid()
        {
            // Arrange
            var organizationAddress = new OrganizationAddress
            {
                Line1 = "California Institute of Technology",
                City = "Pasadena",
                PostalCode = "91125",
                StateCode = "CA",
                CountryCode = CountryCode.TZ,

            };
            var validator = new OrganizationAddressValidator();

            // Act
            var result = validator.Validate(organizationAddress);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage == "StateCode cannot be entered if the CountryCode is not US or CA"));
        }

        // *************************************************************************************************

        [Fact]
        public void State_Code_Must_Be_Entered_If_Country_Code_Is_US_or_CA()
        {
            // Arrange
            var organizationAddress = new OrganizationAddress
            {
                Line1 = "California Institute of Technology",
                City = "Pasadena",
                PostalCode = "91125",
                CountryCode = CountryCode.US
            };
            var validator = new OrganizationAddressValidator();

            // Act
            var result = validator.Validate(organizationAddress);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage == "StateCode must be entered if the CountryCode is US or CA"));
        }
    }
}