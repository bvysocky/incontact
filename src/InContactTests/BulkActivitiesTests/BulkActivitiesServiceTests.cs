﻿#region License
// The MIT License (MIT)
//
// Copyright (c) 2015 Scott Lance, Ethan Tipton
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// 
// The most recent version of this license can be found at: http://opensource.org/licenses/MIT
#endregion

using InContactSdk.BulkActivities;
using InContactSdk.Helpers;
using InContactSdk.Models;
using InContactSdk.Reports;
using InContactSdk.Validation;
using Moq;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Threading.Tasks;
using Xunit;

namespace InContactTests.BulkActivitiesTests
{
    [ExcludeFromCodeCoverage]
    public class BulkActivitiesServiceTests
    {
        private BulkActivitiesService bulkActivitiesService = null;
        private IInContactSettings settings = null;
        private Mock<IValidationService> validationService = null;
        private Mock<ISerialization> serialization = null;
        private Mock<IWebServiceRequest> webServiceRequest = null;

        public BulkActivitiesServiceTests()
        {
            settings = new InContactSettings("123456789", "D27D92A2-6A0B-4F33-BE8A-3C63E597AB18");
            validationService = new Mock<IValidationService>();
            serialization = new Mock<ISerialization>();
            webServiceRequest = new Mock<IWebServiceRequest>();
        }

        public void InitializeBulkActivitiesService<T>(bool fileReaderReturnsNull = false)
           where T : class, new()
        {
            validationService.Setup(m => m.Validate(It.IsAny<IValidatable>()));

            serialization.Setup(m => m.Serialize(It.IsAny<object>())).Returns("");
            serialization.Setup(m => m.Deserialize<T>(It.IsAny<string>())).Returns(new T());
            bulkActivitiesService = new BulkActivitiesService(settings, validationService.Object, webServiceRequest.Object);
        }

        public void InitializeBulkActivitiesService<T1, T2>(bool fileReaderReturnsNull = false)
            where T1 : class, new()
            where T2 : class, new()
        {
            validationService.Setup(m => m.Validate(It.IsAny<IValidatable>()));
            serialization.Setup(m => m.Serialize(It.IsAny<object>())).Returns("");
            serialization.Setup(m => m.Deserialize<T1>(It.IsAny<string>())).Returns(new T1());
            bulkActivitiesService = new BulkActivitiesService(settings, validationService.Object, webServiceRequest.Object);
        }

        // *************************************************************************************************    

        [Fact]
        public async Task GetActivityStatusAsync_Uses_Correct_Endpoint()
        {
            // Arrange
            InitializeBulkActivitiesService<ActivityStatus>();
            string activityId = "123456";
            Uri endpoint = null;

            webServiceRequest.Setup(m => m.GetDeserializedAsync<ActivityStatus>(It.IsAny<Uri>(), It.IsAny<string>()))
                .Callback((Uri a, string b) => endpoint = a)
                .ReturnsAsync(new ActivityStatus());

            // Act
            await bulkActivitiesService.GetActivityStatusAsync(activityId);

            // Assert
            Assert.Equal(string.Format("{0}activities/{1}?api_key={2}", settings.BaseUrl, activityId, settings.ApiKey), endpoint.AbsoluteUri);
        }

        [Fact]
        public async Task GetActivityStatusAsync_Verify_WebServiceRequest_MakeRequest()
        {
            // Arrange            
            InitializeBulkActivitiesService<ActivityStatus>();

            string activityId = "123456";

            // Act
            await bulkActivitiesService.GetActivityStatusAsync(activityId);

            // Assert
            webServiceRequest.Verify(m => m.GetDeserializedAsync<ActivityStatus>(It.IsAny<Uri>(), It.IsAny<string>()), Times.Once);
        }

        // *************************************************************************************************       

        [Fact]
        public async Task GetBulkActivityStatusReportAsync_Status_And_Type_Are_None_Uses_Correct_Endpoint()
        {
            // Arrange    
            InitializeBulkActivitiesService<ActivityStatusReport>();
            var status = BulkActivityStatus.None;
            var type = BulkActivityType.None;
            Uri endpoint = null;

            webServiceRequest.Setup(m => m.GetDeserializedAsync<IEnumerable<ActivityStatusReport>>(It.IsAny<Uri>(), It.IsAny<string>()))
                .Callback((Uri a, string b) => endpoint = a)
                .ReturnsAsync(new List<ActivityStatusReport>());

            // Act
            await bulkActivitiesService.GetBulkActivityStatusReportAsync(type, status);

            // Assert
            Assert.Equal(string.Format("{0}activities/?api_key={1}", settings.BaseUrl, settings.ApiKey), endpoint.AbsoluteUri);
        }

        [Fact]
        public async Task GetBulkActivityStatusReportAsync_Status_All_Type_None_Uses_Correct_Endpoint()
        {
            // Arrange       
            InitializeBulkActivitiesService<ActivityStatusReport>();
            var status = BulkActivityStatus.All;
            var type = BulkActivityType.None;
            Uri endpoint = null;

            webServiceRequest.Setup(m => m.GetDeserializedAsync<IEnumerable<ActivityStatusReport>>(It.IsAny<Uri>(), It.IsAny<string>()))
                .Callback((Uri a, string b) => endpoint = a)
                .ReturnsAsync(new List<ActivityStatusReport>());

            // Act
            await bulkActivitiesService.GetBulkActivityStatusReportAsync(type, status);

            // Assert
            Assert.Equal(string.Format("{0}activities/?api_key={1}&status=ALL", settings.BaseUrl, settings.ApiKey), endpoint.AbsoluteUri);
        }

        [Fact]
        public async Task GetBulkActivityStatusReportAsync_Status_None_Type_AddContacts_Uses_Correct_Endpoint()
        {
            // Arrange
            InitializeBulkActivitiesService<ActivityStatusReport>();
            var status = BulkActivityStatus.None;
            var type = BulkActivityType.AddContacts;
            Uri endpoint = null;

            webServiceRequest.Setup(m => m.GetDeserializedAsync<IEnumerable<ActivityStatusReport>>(It.IsAny<Uri>(), It.IsAny<string>()))
                .Callback((Uri a, string b) => endpoint = a)
                .ReturnsAsync(new List<ActivityStatusReport>());

            // Act
            await bulkActivitiesService.GetBulkActivityStatusReportAsync(type, status);

            // Assert
            Assert.Equal(string.Format("{0}activities/?api_key={1}&type=ADD_CONTACTS", settings.BaseUrl, settings.ApiKey), endpoint.AbsoluteUri);
        }

        [Fact]
        public async Task GetBulkActivityStatusReportAsync_Status_All_Type_AddContacts_Uses_Correct_Endpoint()
        {
            // Arrange
            InitializeBulkActivitiesService<ActivityStatusReport>();
            var status = BulkActivityStatus.All;
            var type = BulkActivityType.AddContacts;
            Uri endpoint = null;

            webServiceRequest.Setup(m => m.GetDeserializedAsync<IEnumerable<ActivityStatusReport>>(It.IsAny<Uri>(), It.IsAny<string>()))
                .Callback((Uri a, string b) => endpoint = a)
                .ReturnsAsync(new List<ActivityStatusReport>());

            // Act
            await bulkActivitiesService.GetBulkActivityStatusReportAsync(type, status);

            // Assert
            Assert.Equal(string.Format("{0}activities/?api_key={1}&status=ALL&type=ADD_CONTACTS", settings.BaseUrl, settings.ApiKey), endpoint.AbsoluteUri);
        }

        [Fact]
        public async Task GetBulkActivityStatusReportAsync_Model_IsValid_Verify_WebServiceRequest_MakeRequest()
        {
            // Arrange            
            InitializeBulkActivitiesService<ActivityStatusReport>();

            // Act
            await bulkActivitiesService.GetBulkActivityStatusReportAsync();

            // Assert
            webServiceRequest.Verify(m => m.GetDeserializedAsync<IEnumerable<ActivityStatusReport>>(It.IsAny<Uri>(), It.IsAny<string>()), Times.Once);
        }

        // *************************************************************************************************        

        [Fact]
        public async Task BulkDeleteFileAsync_Uses_Correct_Endpoint()
        {
            // Arrange
            InitializeBulkActivitiesService<BulkActivityResponse>();

            var data = new byte[] { 128 };
            var fileName = "ContactsUpload.csv";
            var fileFormat = BulkUploadFileFormat.Csv;
            var lists = "1,2";
            Uri endpoint = null;

            webServiceRequest.Setup(m => m.PostMultiparttDeserializedAsync<BulkActivityResponse>(It.IsAny<Uri>(), It.IsAny<string>(), It.IsAny<IEnumerable<KeyValuePair<string, object>>>()))
                .Callback((Uri a, string b, IEnumerable<KeyValuePair<string, object>> c) => endpoint = a)
                .ReturnsAsync(new BulkActivityResponse());

            // Act
            await bulkActivitiesService.BulkDeleteContactFileAsync(data, fileName, fileFormat, lists);

            // Assert
            Assert.Equal(string.Format("{0}activities/removefromlists?api_key={1}", settings.BaseUrl, settings.ApiKey), endpoint.AbsoluteUri);
        }

        [Fact]
        public async Task BulkDeleteFileAsync_Is_Valid_Verify_WebServiceRequest_MakeMultipartFormDataRequest()
        {
            // Arrange
            InitializeBulkActivitiesService<BulkActivityResponse>();

            var data = new byte[] { 128 };
            var fileName = "ContactsUpload.csv";
            var fileFormat = BulkUploadFileFormat.Csv;
            var lists = "1,2";

            // Act
            await bulkActivitiesService.BulkDeleteContactFileAsync(data, fileName, fileFormat, lists);

            // Assert
            webServiceRequest.Verify(m => m.PostMultiparttDeserializedAsync<BulkActivityResponse>(It.IsAny<Uri>(), It.IsAny<string>(), It.IsAny<IEnumerable<KeyValuePair<string, object>>>()), Times.Once);
        }

        // *************************************************************************************************       

        [Fact]
        public async Task ExportContactsAsync_Uses_Correct_Endpoint()
        {
            // Arrange
            InitializeBulkActivitiesService<BulkActivityResponse>();
            var model = new ContactExport
            {
                Lists = new List<string> { "123456" },
                SortBy = BulkActivitySortBy.DateDesc,
                ColumnNames = new List<ExportColumnName>
                {
                    ExportColumnName.Email
                }
            };
            Uri endpoint = null;

            webServiceRequest.Setup(m => m.PostDeserializedAsync<ContactExport, BulkActivityResponse>(It.IsAny<Uri>(), It.IsAny<string>(), It.IsAny<ContactExport>()))
                .Callback((Uri a, string b, ContactExport c) => endpoint = a)
                .ReturnsAsync(new BulkActivityResponse());

            // Act
            await bulkActivitiesService.ExportContactsAsync(model);

            // Assert
            Assert.Equal(string.Format("{0}activities/exportcontacts?api_key={1}", settings.BaseUrl, settings.ApiKey), endpoint.AbsoluteUri);
        }

        [Fact]
        public async Task ExportContactsAsync_Is_Valid_Verify_WebServiceRequest_MakeRequest()
        {
            // Arrange
            InitializeBulkActivitiesService<BulkActivityResponse>();
            var model = new ContactExport
            {
                Lists = new List<string> { "123456" },
                SortBy = BulkActivitySortBy.DateDesc,
                ColumnNames = new List<ExportColumnName>
                {
                    ExportColumnName.Email
                }
            };

            // Act
            await bulkActivitiesService.ExportContactsAsync(model);

            // Assert
            webServiceRequest.Verify(m => m.PostDeserializedAsync<ContactExport, BulkActivityResponse>(It.IsAny<Uri>(), It.IsAny<string>(), It.IsAny<ContactExport>()), Times.Once);
        }

        // *************************************************************************************************      

        [Fact]
        public async Task RetrieveExportedFileAsync_Uses_Correct_Endpoint()
        {
            // Arrange
            InitializeBulkActivitiesService<BulkActivityResponse>();
            var activityId = "123456789";
            var fileExtension = BulkExportFileFormat.Csv;
            Uri endpoint = null;

            webServiceRequest.Setup(m => m.GetDeserializedAsync<string>(It.IsAny<Uri>(), It.IsAny<string>()))
                .Callback((Uri a, string b) => endpoint = a)
                .ReturnsAsync("");

            // Act
            await bulkActivitiesService.RetrieveExportedFileAsync(activityId, fileExtension);

            // Assert
            Assert.Equal(string.Format("{0}activities/exportfiles/{1}.{2}?api_key={3}", settings.BaseUrl, activityId, EnumStrings.BulkExportFileFormatStrings[fileExtension], settings.ApiKey), endpoint.AbsoluteUri);
        }

        [Fact]
        public async Task RetrieveExportedFileAsync_Path_Is_Not_NullOrWhiteSpace_Verify_MakeRequestAsync()
        {
            // Arrange
            InitializeBulkActivitiesService<BulkActivityResponse>();
            var activityId = "123456789";
            var fileExtension = BulkExportFileFormat.Csv;

            // Act
            await bulkActivitiesService.RetrieveExportedFileAsync(activityId, fileExtension);

            // Assert
            webServiceRequest.Verify(m => m.GetDeserializedAsync<string>(It.IsAny<Uri>(), It.IsAny<string>()), Times.Once());
        }

        // *************************************************************************************************      

        [Fact]
        public async Task ImportContactsAsync_Uses_Correct_Endpoint()
        {
            // Arrange
            InitializeBulkActivitiesService<ContactImport, BulkActivityResponse>();
            var importContactsAsync = new ContactImport
            {
                ImportData = new List<ImportData>
                {
                    new ImportData
                    {
                        EmailAddresses = new string[] { "scooper@caltech.edu" }
                    }
                },
                Lists = new List<string> { "123456" },
                ColumnNames = new List<ImportColumnName> { ImportColumnName.Email }
            };
            Uri endpoint = null;

            webServiceRequest.Setup(m => m.PostDeserializedAsync<ContactImport, BulkActivityResponse>(It.IsAny<Uri>(), It.IsAny<string>(), It.IsAny<ContactImport>()))
                .Callback((Uri a, string b, ContactImport c) => endpoint = a)
                .ReturnsAsync(new BulkActivityResponse());

            // Act
            await bulkActivitiesService.ImportContactsAsync(importContactsAsync);

            // Assert
            Assert.Equal(string.Format("{0}activities/addcontacts?api_key={1}", settings.BaseUrl, settings.ApiKey), endpoint.AbsoluteUri);            
        }

        [Fact]
        public async Task ImportContactsAsync_Has_No_Errors_Verify_MakeSerializedRequestAsync()
        {
            // Arrange
            InitializeBulkActivitiesService<ContactImport, BulkActivityResponse>();
            var importContactsAsync = new ContactImport
            {
                ImportData = new List<ImportData>
                {
                    new ImportData
                    {
                        EmailAddresses = new string[] { "scooper@caltech.edu" }
                    }
                },
                Lists = new List<string> { "123456" },
                ColumnNames = new List<ImportColumnName> { ImportColumnName.Email }
            };

            // Act
            await bulkActivitiesService.ImportContactsAsync(importContactsAsync);

            // Assert
            webServiceRequest.Verify(m => m.PostDeserializedAsync<ContactImport, BulkActivityResponse>(It.IsAny<Uri>(), It.IsAny<string>(), It.IsAny<ContactImport>()), Times.Once());
        }

        // *************************************************************************************************       

        [Fact]
        public async Task RemoveContactsAsync_Uses_Correct_Endpoint()
        {
            // Arrange
            InitializeBulkActivitiesService<ContactRemove, BulkActivityResponse>();
            var removeContactsAsync = new ContactRemove
            {
                ImportData = new ImportData[]
                {
                    new ImportData
                    {
                        EmailAddresses = new List<string> { "scooper@caltech.edu" }
                    }
                },
                Lists = new List<string> { "123456" }
            };
            Uri endpoint = null;

            webServiceRequest.Setup(m => m.PostDeserializedAsync<ContactRemove, BulkActivityResponse>(It.IsAny<Uri>(), It.IsAny<string>(), It.IsAny<ContactRemove>()))
                .Callback((Uri a, string b, ContactRemove c) => endpoint = a)
                .ReturnsAsync(new BulkActivityResponse());

            // Act
            await bulkActivitiesService.RemoveContactsAsync(removeContactsAsync);

            // Assert
            Assert.Equal(string.Format("{0}activities/removefromlists?api_key={1}", settings.BaseUrl, settings.ApiKey), endpoint.AbsoluteUri);            
        }

        [Fact]
        public async Task RemoveContactsAsync_Has_No_Errors_Verify_MakeSerializedRequestAsync()
        {
            // Arrange
            InitializeBulkActivitiesService<ContactRemove, BulkActivityResponse>();
            var removeContactsAsync = new ContactRemove
            {
                ImportData = new ImportData[]
                {
                    new ImportData
                    {
                        EmailAddresses = new List<string> { "scooper@caltech.edu" }
                    }
                },
                Lists = new List<string> { "123456" }
            };

            // Act
            await bulkActivitiesService.RemoveContactsAsync(removeContactsAsync);

            // Assert
            webServiceRequest.Verify(m => m.PostDeserializedAsync<ContactRemove, BulkActivityResponse>(It.IsAny<Uri>(), It.IsAny<string>(), It.IsAny<ContactRemove>()), Times.Once());
        }

        // *************************************************************************************************            

        [Fact]
        public async Task ClearContactListsAsync_Uses_Correct_Endpoint()
        {
            // Arrange
            InitializeBulkActivitiesService<ContactLists, BulkActivityResponse>();
            var lists = new ContactLists
            {
                Lists = new List<string>
                {
                    "123456",
                    "654321"
                }
            };
            Uri endpoint = null;

            webServiceRequest.Setup(m => m.PostDeserializedAsync<ContactLists, BulkActivityResponse>(It.IsAny<Uri>(), It.IsAny<string>(), It.IsAny<ContactLists>()))
                .Callback((Uri a, string b, ContactLists c) => endpoint = a)
                .ReturnsAsync(new BulkActivityResponse());

            // Act
            await bulkActivitiesService.ClearContactListsAsync(lists);

            // Assert
            Assert.Equal(string.Format("{0}activities/clearlists?api_key={1}", settings.BaseUrl, settings.ApiKey), endpoint.AbsoluteUri);            
        }

        [Fact]
        public async Task ClearContactListsAsync_Lists_Is_Valid_Verify_MakeSerializedRequestAsync()
        {
            // Arrange 
            InitializeBulkActivitiesService<ContactLists, BulkActivityResponse>();
            var lists = new ContactLists
            {
                Lists = new List<string>
                {
                    "123456",
                    "654321"
                }
            };

            // Act
            await bulkActivitiesService.ClearContactListsAsync(lists);

            // Assert
            webServiceRequest.Verify(m => m.PostDeserializedAsync<ContactLists, BulkActivityResponse>(It.IsAny<Uri>(), It.IsAny<string>(), It.IsAny<ContactLists>()), Times.Once());
        }
    }
}