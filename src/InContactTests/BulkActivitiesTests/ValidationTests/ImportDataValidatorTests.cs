﻿#region License
// The MIT License (MIT)
//
// Copyright (c) 2015 Scott Lance, Ethan Tipton
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// 
// The most recent version of this license can be found at: http://opensource.org/licenses/MIT
#endregion

using InContactSdk.BulkActivities;
using InContactSdk.BulkActivities.Validation;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Xunit;

namespace InContactTests.BulkActivitiesTests.ValidationTests
{
    [ExcludeFromCodeCoverage]
    public class ImportDataValidatorTests
    {
        private readonly ImportDataValidator validator;

        public ImportDataValidatorTests()
        {
            validator = new ImportDataValidator();
        }

        // *************************************************************************************************

        [Fact]
        public void ImportData_Email_Addresses_Is_Empty_Is_Not_Valid()
        {
            // Arrange
            var importData = new ImportData();

            // Act
            var result = validator.Validate(importData);

            // Assert
            Assert.True(result.Errors.Any(e => e.ErrorMessage == "EmailAddresses must contain at least one email address"));
        }

        [Fact]
        public void ImportData_Email_Addresses_Contains_Email_Address_Is_Valid()
        {
            // Arrange
            var importData = new ImportData
            {
                EmailAddresses = new string[] { "scooper@caltech.edu" }
            };

            // Act
            var result = validator.Validate(importData);

            // Assert
            Assert.False(result.Errors.Any(e => e.ErrorMessage == "EmailAddresses must contain at least one email address"));
        }
    }
}