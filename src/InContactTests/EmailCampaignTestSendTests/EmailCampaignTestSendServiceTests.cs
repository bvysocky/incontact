﻿#region License
// The MIT License (MIT)
//
// Copyright (c) 2015 Scott Lance, Ethan Tipton
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// 
// The most recent version of this license can be found at: http://opensource.org/licenses/MIT
#endregion

using System;
using System.Diagnostics.CodeAnalysis;
using System.Net.Http;
using System.Threading.Tasks;
using InContactSdk.EmailCampaignTestSend;
using InContactSdk.EmailCampaignTestSend.Models;
using InContactSdk.Helpers;
using InContactSdk.Models;
using InContactSdk.Validation;
using Moq;
using Xunit;

namespace InContactTests.EmailCampaignTestSendTests
{
    [ExcludeFromCodeCoverage]
    public class EmailCampaignTestSendServiceTests
    {
        private readonly IInContactSettings settings;
        private readonly Mock<IWebServiceRequest> webServiceRequest;
        private readonly Mock<IValidationService> validationService;
        private readonly EmailCampaignTestSendService service;

        public EmailCampaignTestSendServiceTests()
        {
            settings = new InContactSettings("123456789", "D27D92A2-6A0B-4F33-BE8A-3C63E597AB18");
            webServiceRequest = new Mock<IWebServiceRequest>();
            validationService = new Mock<IValidationService>();
            validationService.Setup(m => m.Validate(It.IsAny<IValidatable>()));
            service = new EmailCampaignTestSendService(settings, validationService.Object, webServiceRequest.Object);
        }

        // *************************************************************************************************

        [Fact]
        public async Task SendEmailCampaignTest_UsesCorrectEndpoint()
        {
            // Arrange
            Uri endpoint = null;

            webServiceRequest.Setup(m => m.PostAsync<SendEmailCampaignTestModel>(It.IsAny<Uri>(), It.IsAny<string>(), It.IsAny<SendEmailCampaignTestModel>()))
                .Callback((Uri a, string b, SendEmailCampaignTestModel c) => endpoint = a)
                .ReturnsAsync(new HttpResponseMessage());

            // Act
            await service.SendEmailCampaignTestAsync("aaa", new string[] { "test@example.com" }, EmailFormat.Text, "msg");

            // Assert
            Assert.Equal(string.Format("{0}emailmarketing/campaigns/aaa/tests?api_key={1}", settings.BaseUrl, settings.ApiKey), endpoint.AbsoluteUri);
        }

        [Fact]
        public async Task SendEmailCampaignTest_IfModelIsValid_VerifyMakeSerializedRequestAsync()
        {
            // Act
            await service.SendEmailCampaignTestAsync("27", new string[] { "test@example.com" }, EmailFormat.Text, "");

            // Assert
            webServiceRequest.Verify(m => m.PostAsync<SendEmailCampaignTestModel>(It.IsAny<Uri>(), It.IsAny<string>(), It.IsAny<SendEmailCampaignTestModel>()), Times.Once());
        }

        // *************************************************************************************************

        [Fact]
        public async Task GetEmailCampaignPreviewAsync_UsesCorrectEndpoint()
        {
            // Arrange
            Uri endpoint = null;

            webServiceRequest.Setup(m => m.GetDeserializedAsync<GetEmailCampaignPreviewModel>(It.IsAny<Uri>(), It.IsAny<string>()))
                .Callback((Uri a, string b) => endpoint = a)
                .ReturnsAsync(new GetEmailCampaignPreviewModel());

            // Act
            await service.GetEmailCampaignPreviewAsync("aaa");

            // Assert
            Assert.Equal(string.Format("{0}emailmarketing/campaigns/aaa/preview?api_key={1}", settings.BaseUrl, settings.ApiKey), endpoint.AbsoluteUri);
        }

        [Fact]
        public async Task GetEmailCampaignPreview_IfModelIsValid_VerifyMakeSerializedRequestAsync()
        {
            // Act
            await service.GetEmailCampaignPreviewAsync("27");

            // Assert
            webServiceRequest.Verify(m => m.GetDeserializedAsync<GetEmailCampaignPreviewModel>(It.IsAny<Uri>(), It.IsAny<string>()), Times.Once());
        }

        // *************************************************************************************************

    }
}
