﻿#region License
// The MIT License (MIT)
//
// Copyright (c) 2015 Scott Lance, Ethan Tipton
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// 
// The most recent version of this license can be found at: http://opensource.org/licenses/MIT
#endregion

using InContactSdk.Helpers;
using InContactSdk.Reports;
using System;
using System.Threading.Tasks;

namespace InContactSdk.EmailCampaignTracking
{
    public interface IEmailCampaignTrackingService
    {
        Task<ActivitySummaryReport> GetActivitySummaryReportAsync(string campaignId, bool updateSummary = false);
        Task<PaginatedResult<BounceActivityReport>> GetBounceActivityReportAsync(string campaignId, DateTime? created_since = null, int limit = 500);
        Task<PaginatedResult<EmailClickActivityReport>> GetClickActivityReportAsync(string campaignId, DateTime? created_since = null, int limit = 500);
        Task<PaginatedResult<ClickByLinkActivityReport>> GetClickByLinkActivityReportAsync(string campaignId, string linkId, DateTime? createdSince = null, int limit = 500);
        Task<PaginatedResult<ForwardActivityReport>> GetForwardActivityReportAsync(string campaignId, DateTime? created_since = null, int limit = 500);
        Task<PaginatedResult<OpenActivityReport>> GetOpenActivityReportAsync(string campaignId, DateTime? created_since = null, int limit = 500);
        Task<PaginatedResult<SendActivityReport>> GetSendActivityReportAsync(string campaignId, DateTime? created_since = null, int limit = 500);
        Task<PaginatedResult<UnsubscribeActivityReport>> GetUnsubscribeActivityReportAsync(string campaignId, DateTime? created_since = null, int limit = 500);
    }
}