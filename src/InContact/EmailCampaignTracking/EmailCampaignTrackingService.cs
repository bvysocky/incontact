﻿#region License
// The MIT License (MIT)
//
// Copyright (c) 2015 Scott Lance, Ethan Tipton
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// 
// The most recent version of this license can be found at: http://opensource.org/licenses/MIT
#endregion

using InContactSdk.EmailCampaignTracking.Models;
using InContactSdk.Helpers;
using InContactSdk.Models;
using InContactSdk.Reports;
using InContactSdk.Validation;
using System;
using System.Threading.Tasks;

namespace InContactSdk.EmailCampaignTracking
{
    public class EmailCampaignTrackingService : ServiceBase, IEmailCampaignTrackingService
    {
        public EmailCampaignTrackingService(IInContactSettings settings)
            : base(settings)
        { }

        internal EmailCampaignTrackingService(IInContactSettings settings, IValidationService validationService, IWebServiceRequest webServiceRequest)
            : base(settings, validationService, webServiceRequest)
        { }

        // Reference: https://developer.constantcontact.com/docs/campaign-tracking/summary-report.html
        public async Task<ActivitySummaryReport> GetActivitySummaryReportAsync(string campaignId, bool updateSummary = false)
        {
            var model = new EmailActivitySummaryReportModel() { Settings = settings, CampaignId = campaignId, UpdateSummary = updateSummary };
            return await webServiceRequest.GetDeserializedAsync<ActivitySummaryReport>(new Uri(model.GenerateEndpoint()), settings.AuthToken);
        }

        // Reference: https://developer.constantcontact.com/docs/campaign-tracking/bounce-activities-collection.html
        public async Task<PaginatedResult<BounceActivityReport>> GetBounceActivityReportAsync(string campaignId, DateTime? createdSince = null, int limit = 500)
        {
            return await GetActivityReportAsync<BounceActivityReport>(campaignId, ServiceEndpoint.Bounces, createdSince, limit);
        }

        // Reference: https://developer.constantcontact.com/docs/campaign-tracking/summary-report.html
        public async Task<PaginatedResult<EmailClickActivityReport>> GetClickActivityReportAsync(string campaignId, DateTime? createdSince = null, int limit = 500)
        {
            return await GetActivityReportAsync<EmailClickActivityReport>(campaignId, ServiceEndpoint.Clicks, createdSince, limit);
        }

        // Reference: https://developer.constantcontact.com/docs/campaign-tracking/click-activities-by-link-report.html
        public async Task<PaginatedResult<ClickByLinkActivityReport>> GetClickByLinkActivityReportAsync(string campaignId, string linkId, DateTime? createdSince = null, int limit = 500)
        {
            var model = new ClickByLinkActivityReportModel() { Settings = settings, CampaignId = campaignId, LinkId = linkId };
            return await webServiceRequest.GetDeserializedAsync<PaginatedResult<ClickByLinkActivityReport>>(new Uri(model.GenerateEndpoint()), settings.AuthToken);
        }

        // Reference: https://developer.constantcontact.com/docs/campaign-tracking/forward-activities-collection.html
        public async Task<PaginatedResult<ForwardActivityReport>> GetForwardActivityReportAsync(string campaignId, DateTime? createdSince = null, int limit = 500)
        {
            return await GetActivityReportAsync<ForwardActivityReport>(campaignId, ServiceEndpoint.Forwards, createdSince, limit);
        }

        // Reference: https://developer.constantcontact.com/docs/campaign-tracking/open-activities-collection.html
        public async Task<PaginatedResult<OpenActivityReport>> GetOpenActivityReportAsync(string campaignId, DateTime? createdSince = null, int limit = 500)
        {
            return await GetActivityReportAsync<OpenActivityReport>(campaignId, ServiceEndpoint.Opens, createdSince, limit);
        }

        // Reference: https://developer.constantcontact.com/docs/campaign-tracking/send-activities-collection.html
        public async Task<PaginatedResult<SendActivityReport>> GetSendActivityReportAsync(string campaignId, DateTime? createdSince = null, int limit = 500)
        {
            return await GetActivityReportAsync<SendActivityReport>(campaignId, ServiceEndpoint.Sends, createdSince, limit);
        }

        // Reference: https://developer.constantcontact.com/docs/campaign-tracking/opt-out-activities-collection.html
        public async Task<PaginatedResult<UnsubscribeActivityReport>> GetUnsubscribeActivityReportAsync(string campaignId, DateTime? createdSince = null, int limit = 500)
        {
            return await GetActivityReportAsync<UnsubscribeActivityReport>(campaignId, ServiceEndpoint.Unsubscribes, createdSince, limit);
        }

        private async Task<PaginatedResult<T>> GetActivityReportAsync<T>(string campaignId, ServiceEndpoint report, DateTime? createdSince = null, int limit = 500)
            where T : ActivityReport, new()
        {
            var model = new ActivityReportModel { Settings = settings, Id = campaignId, ContactService = false, Report = report, CreatedSince = createdSince, Limit = limit };
            validationService.Validate(model);
            return await webServiceRequest.GetDeserializedAsync<PaginatedResult<T>>(new Uri(model.GenerateEndpoint()), settings.AuthToken);
        }
    }
}