﻿#region License
// The MIT License (MIT)
//
// Copyright (c) 2015 Scott Lance, Ethan Tipton
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// 
// The most recent version of this license can be found at: http://opensource.org/licenses/MIT
#endregion

using FluentValidation;
using InContactSdk.Helpers;
using System.Collections.Generic;
using System.Linq;

namespace InContactSdk.Contacts.Validation
{
    internal class ContactValidator : AbstractValidator<Contact>
    {
        public ContactValidator()
        {
            RuleFor(c => c.Addresses).Must(a => ArrayIsNullOrLessThanMaxElements<Address>(a, 2)).WithMessage("Addresses array cannot contain more than 2 items").Must(OnePersonalOneBusiness).WithMessage("Addresses may only contain one personal and one business address");
            RuleFor(c => c.CellPhone).Length(0, 50).WithMessage("CellPhone cannot contain more than 50 characters");
            RuleFor(c => c.CompanyName).Length(0, 50).WithMessage("CompanyName cannot contain more than 50 characters");
            RuleFor(c => c.CustomFields).Must(c => ArrayIsNullOrLessThanMaxElements<CustomField>(c, 15)).WithMessage("CustomFields array cannot contain more than 15 items");
            RuleFor(c => c.EmailAddresses).Must(e => e != null && e.Count() > 0).WithMessage("At least one EmailAddress must be included");
            RuleFor(c => c.Fax).Length(0, 50).WithMessage("Fax cannot contain more than 50 characters");
            RuleFor(c => c.FirstName).Length(0, 50).WithMessage("FirstName cannot contain more than 50 characters");
            RuleFor(c => c.HomePhone).Length(0, 50).WithMessage("HomePhone cannot contain more than 50 characters");
            RuleFor(c => c.JobTitle).Length(0, 50).WithMessage("JobTitle cannot contain more than 50 characters");
            RuleFor(c => c.LastName).Length(0, 50).WithMessage("LastName cannot contain more than 50 characters");
            RuleFor(c => c.Lists).Must(l => l != null && l.Count() > 0).WithMessage("At least one List must be included");
            RuleFor(c => c.PrefixName).Length(0, 4).WithMessage("PrefixName cannot contain more than 4 characters");
            RuleFor(c => c.WorkPhone).Length(0, 50).WithMessage("WorkPhone cannot contain more than 50 characters");
        }

        private bool ArrayIsNullOrLessThanMaxElements<T>(IEnumerable<T> t, int max)
        {
            if (t == null)
                return true;

            if (t.Count() <= max)
                return true;

            return false;
        }

        private bool OnePersonalOneBusiness(IEnumerable<Address> addresses)
        {
            if (addresses == null)
                return true;

            int personal = 0;
            int business = 0;
            foreach (var a in addresses)
            {
                switch (a.AddressType)
                {
                    case ContactAddressType.Business: business++; break;
                    case ContactAddressType.Personal: personal++; break;
                }
            }

            if (personal > 1 || business > 1)
                return false;

            return true;
        }
    }
}