﻿#region License
// The MIT License (MIT)
//
// Copyright (c) 2015 Scott Lance, Ethan Tipton
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// 
// The most recent version of this license can be found at: http://opensource.org/licenses/MIT
#endregion

using InContactSdk.Contacts.Models;
using InContactSdk.Helpers;
using InContactSdk.Models;
using InContactSdk.Validation;
using System;
using System.Threading.Tasks;

namespace InContactSdk.Contacts
{
    public class ContactsService : ServiceBase, IContactsService
    {
        public ContactsService(IInContactSettings settings)
            : base(settings)
        { }

        internal ContactsService(IInContactSettings settings, IValidationService validationService, IWebServiceRequest webServiceRequest)
            : base(settings, validationService, webServiceRequest)
        { }

        // Reference: http://developer.constantcontact.com/docs/contacts-api/contacts-collection.html?method=POST
        public async Task<Contact> CreateContactAsync(Contact contact, ActionBy actionBy = ActionBy.ActionByOwner)
        {
            var model = new CreateContactModel { Settings = settings, Contact = contact, ActionBy = actionBy };
            validationService.Validate(model);
            return await webServiceRequest.PostDeserializedAsync<Contact, Contact>(new Uri(model.GenerateEndpoint()), settings.AuthToken, model.Contact);
        }

        // Reference: http://developer.constantcontact.com/docs/contacts-api/contacts-resource.html?method=DELETE
        public async Task DeleteContactAsync(string contactId)
        {
            var model = new ContactIdModel { Settings = settings, ContactId = contactId };
            validationService.Validate(model);
            await webServiceRequest.DeleteAsync(new Uri(model.GenerateEndpoint()), settings.AuthToken);
        }

        // Reference: http://developer.constantcontact.com/docs/contacts-api/contacts-collection.html?method=GET
        public async Task<PaginatedResult<Contact>> GetContactsPagedAsync(int limit = 50, ContactStatusQuery status = ContactStatusQuery.All, DateTime? modifiedSince = null, string email = "")
        {
            var model = new GetAllContactsModel { Settings = settings, Status = status, Limit = limit, ModifiedSince = modifiedSince.HasValue ? modifiedSince.Value.ToString("o") : "", Email = email };
            validationService.Validate(model);
            return await webServiceRequest.GetDeserializedAsync<PaginatedResult<Contact>>(new Uri(model.GenerateEndpoint()), settings.AuthToken);
        }

        // Reference: http://developer.constantcontact.com/docs/contacts-api/contacts-resource.html?method=GET
        public async Task<Contact> GetContactAsync(string contactId)
        {
            var model = new ContactIdModel { Settings = settings, ContactId = contactId };
            validationService.Validate(model);
            return await webServiceRequest.GetDeserializedAsync<Contact>(new Uri(model.GenerateEndpoint()), settings.AuthToken);
        }

        // Reference: http://developer.constantcontact.com/docs/contacts-api/contacts-resource.html?method=PUT
        public async Task<Contact> UpdateContactAsync(Contact contact, string contactId, ActionBy actionBy = ActionBy.ActionByOwner)
        {
            var model = new UpdateContactModel { Settings = settings, Contact = contact, ContactId = contactId, ActionBy = actionBy };
            validationService.Validate(model);
            return await webServiceRequest.PutDeserializedAsync<Contact, Contact>(new Uri(model.GenerateEndpoint()), settings.AuthToken, model.Contact);
        }
    }
}