﻿#region License
// The MIT License (MIT)
//
// Copyright (c) 2015 Scott Lance, Ethan Tipton
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// 
// The most recent version of this license can be found at: http://opensource.org/licenses/MIT
#endregion

using InContactSdk.Contacts.Validation;
using InContactSdk.Exceptions;
using InContactSdk.Helpers;
using InContactSdk.Validation;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace InContactSdk.Contacts
{
    public class Address : IValidatable
    {
        [JsonProperty("address_type"), JsonConverter(typeof(TypeEnumConverter<ContactAddressType, BiLookup<ContactAddressType, string>>))]
        public ContactAddressType AddressType { get; set; }
        [JsonProperty("city")]
        public string City { get; set; }
        [JsonProperty("country_code"), JsonConverter(typeof(TypeEnumConverter<CountryCode, BiLookup<CountryCode, string>>))]
        public CountryCode CountryCode { get; set; }
        [JsonProperty("id")]
        public string Id { get; internal set; }
        [JsonProperty("line1")]
        public string Line1 { get; set; }
        [JsonProperty("line2")]
        public string Line2 { get; set; }
        [JsonProperty("line3")]
        public string Line3 { get; set; }
        [JsonProperty("postal_code")]
        public string PostalCode { get; set; }
        [JsonProperty("state")]
        public string State { get; set; }
        [JsonProperty("state_code")]
        public string StateCode { get; set; }
        [JsonProperty("sub_postal_code")]
        public string SubPostalCode { get; set; }

        public IEnumerable<ValidationError> Validate()
        {
            return Validator.Validate<Address, AddressValidator>(this);
        }
    }
}