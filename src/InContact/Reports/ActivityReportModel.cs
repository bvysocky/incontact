﻿#region License
// The MIT License (MIT)
//
// Copyright (c) 2015 Scott Lance, Ethan Tipton
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// 
// The most recent version of this license can be found at: http://opensource.org/licenses/MIT
#endregion

using InContactSdk.Exceptions;
using InContactSdk.Helpers;
using InContactSdk.Models;
using InContactSdk.Reports.Validation;
using InContactSdk.Validation;
using System;
using System.Collections.Generic;
using System.Text;

namespace InContactSdk.Reports
{
    internal class ActivityReportModel : IValidatable
    {
        public IInContactSettings Settings { get; set; }
        public string Id { get; set; }
        public ServiceEndpoint Report { get; set; }
        public DateTime? CreatedSince { get; set; }
        public int Limit { get; set; }
        public bool ContactService { get; set; }

        public IEnumerable<ValidationError> Validate()
        {
            return Validator.Validate<ActivityReportModel, ActivityReportModelValidator>(this);
        }

        public string GenerateEndpoint()
        {
            var endpoint = new StringBuilder(string.Format("{0}{1}/{2}/{3}?api_key={4}", this.Settings.BaseUrl, this.ContactService ? EnumStrings.ServiceEndpointStrings[ServiceEndpoint.Contacts] : EnumStrings.ServiceEndpointStrings[ServiceEndpoint.EmailCampaigns], this.Id, EnumStrings.ServiceEndpointStrings[this.Report], this.Settings.ApiKey));

            if (this.Limit != 50)
                endpoint.AppendFormat("&limit={0}", this.Limit);

            if (this.CreatedSince.HasValue)
                endpoint.AppendFormat("&created_since={0}", this.CreatedSince.Value.ToString("o"));

            return endpoint.ToString();
        }
    }
}