﻿#region License
// The MIT License (MIT)
//
// Copyright (c) 2015 Scott Lance, Ethan Tipton
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// 
// The most recent version of this license can be found at: http://opensource.org/licenses/MIT
#endregion

using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace InContactSdk.Helpers
{
    public interface IWebServiceRequest
    {
        Task<HttpResponseMessage> GetAsync(Uri requestUri, string accessToken);
        Task<T> GetDeserializedAsync<T>(Uri requestUri, string accessToken) where T : class;
        Task<HttpResponseMessage> PutAsync<T>(Uri requestUri, string accessToken, T payload) where T : class;
        Task<T2> PutDeserializedAsync<T1, T2>(Uri requestUri, string accessToken, T1 payload) where T1 : class where T2 : class;
        Task<HttpResponseMessage> DeleteAsync(Uri requestUri, string accessToken);
        Task<HttpResponseMessage> PatchAsync<T>(Uri requestUri, string accessToken, T payload) where T : class;
        Task<T2> PatchDeserializedAsync<T1, T2>(Uri requestUri, string accessToken, T1 payload) where T1 : class where T2 : class;
        Task<HttpResponseMessage> PostAsync<T>(Uri requestUri, string accessToken, T payload) where T : class;
        Task<T2> PostDeserializedAsync<T1, T2>(Uri requestUri, string accessToken, T1 payload) where T1 : class where T2 : class;
        Task<T> PostMultiparttDeserializedAsync<T>(Uri requestUri, string accessToken, IEnumerable<KeyValuePair<string, object>> postParameters) where T : class;
        Task<HttpResponseMessage> PostMultipartAsync(Uri requestUri, string accessToken, IEnumerable<KeyValuePair<string, object>> postParameters);
    }
}