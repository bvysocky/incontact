﻿#region License
// The MIT License (MIT)
//
// Copyright (c) 2015 Scott Lance, Ethan Tipton
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// 
// The most recent version of this license can be found at: http://opensource.org/licenses/MIT
#endregion

using InContactSdk.ContactTracking.Models;
using InContactSdk.Helpers;
using InContactSdk.Models;
using InContactSdk.Reports;
using InContactSdk.Validation;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace InContactSdk.ContactTracking
{
    public class ContactTrackingService : ServiceBase, IContactTrackingService
    {
        public ContactTrackingService(IInContactSettings settings)
            : base(settings)
        { }

        internal ContactTrackingService(IInContactSettings settings, IValidationService validationService, IWebServiceRequest webServiceRequest)
            : base(settings, validationService, webServiceRequest)
        { }

        // Reference: http://developer.constantcontact.com/docs/contact-tracking/summary-report.html
        public async Task<ContactActivitySummaryReport> GetActivitySummaryReportAsync(string contactId)
        {
            var model = new GetActivitySummaryReportModel { Settings = settings, ContactId = contactId };
            validationService.Validate(model);
            return await webServiceRequest.GetDeserializedAsync<ContactActivitySummaryReport>(new Uri(model.GenerateEndpoint()), settings.AuthToken);
        }

        // Reference: http://developer.constantcontact.com/docs/contact-tracking/bounce-activities-report.html
        public async Task<PaginatedResult<BounceActivityReport>> GetBounceActivityReportAsync(string contactId, DateTime? createdSince = null, int limit = 500)
        {
            return await GetActivityReportAsync<BounceActivityReport>(contactId, ServiceEndpoint.Bounces, createdSince, limit);
        }

        // Reference: http://developer.constantcontact.com/docs/contact-tracking/click-activities-report.html
        public async Task<PaginatedResult<ContactClickActivityReport>> GetClickActivityReportAsync(string contactId, DateTime? createdSince = null, int limit = 500)
        {
            return await GetActivityReportAsync<ContactClickActivityReport>(contactId, ServiceEndpoint.Clicks, createdSince, limit);
        }

        // Reference: http://developer.constantcontact.com/docs/contact-tracking/contacttracking-bycampaign.html
        public async Task<IEnumerable<CampaignActivityReport>> GetCampaignActivityReportAsync(string contactId)
        {
            var model = new GetCampaignActivityReportModel { Settings = settings, ContactId = contactId };
            validationService.Validate(model);
            return await webServiceRequest.GetDeserializedAsync<IEnumerable<CampaignActivityReport>>(new Uri(model.GenerateEndpoint()), settings.AuthToken);
        }

        // Reference: http://developer.constantcontact.com/docs/contact-tracking/contact-tracking-all-activities-api.html
        public async Task<PaginatedResult<DetailedActivityReport>> GetDetailedActivityReportAsync(string contactId, DateTime? createdSince = null, int limit = 500)
        {
            return await GetActivityReportAsync<DetailedActivityReport>(contactId, ServiceEndpoint.Tracking, createdSince, limit);
        }

        // Reference: http://developer.constantcontact.com/docs/contact-tracking/forward-activities-report.html
        public async Task<PaginatedResult<ForwardActivityReport>> GetForwardActivityReportAsync(string contactId, DateTime? createdSince = null, int limit = 500)
        {
            return await GetActivityReportAsync<ForwardActivityReport>(contactId, ServiceEndpoint.Forwards, createdSince, limit);
        }

        // Reference: http://developer.constantcontact.com/docs/contact-tracking/open-activities-report.html
        public async Task<PaginatedResult<OpenActivityReport>> GetOpenActivityReportAsync(string contactId, DateTime? createdSince = null, int limit = 500)
        {
            return await GetActivityReportAsync<OpenActivityReport>(contactId, ServiceEndpoint.Opens, createdSince, limit);
        }

        // Reference: http://developer.constantcontact.com/docs/contact-tracking/send-activities-report.html
        public async Task<PaginatedResult<SendActivityReport>> GetSendActivityReportAsync(string contactId, DateTime? createdSince = null, int limit = 500)
        {
            return await GetActivityReportAsync<SendActivityReport>(contactId, ServiceEndpoint.Sends, createdSince, limit);
        }

        // Reference: http://developer.constantcontact.com/docs/contact-tracking/unsubscribes-activities-report.html
        public async Task<PaginatedResult<UnsubscribeActivityReport>> GetUnsubscribeActivityReportAsync(string contactId, DateTime? createdSince = null, int limit = 500)
        {
            return await GetActivityReportAsync<UnsubscribeActivityReport>(contactId, ServiceEndpoint.Unsubscribes, createdSince, limit);
        }

        private async Task<PaginatedResult<T>> GetActivityReportAsync<T>(string contactId, ServiceEndpoint report, DateTime? createdSince = null, int limit = 500)
            where T : ActivityReport, new()
        {
            var model = new ActivityReportModel { Settings = settings, Id = contactId, ContactService = true, Report = report, CreatedSince = createdSince, Limit = limit };
            validationService.Validate(model);
            return await webServiceRequest.GetDeserializedAsync<PaginatedResult<T>>(new Uri(model.GenerateEndpoint()), settings.AuthToken);
        }
    }
}