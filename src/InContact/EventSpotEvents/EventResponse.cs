﻿#region License
// The MIT License (MIT)
//
// Copyright (c) 2015 Scott Lance, Ethan Tipton
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// 
// The most recent version of this license can be found at: http://opensource.org/licenses/MIT
#endregion

using InContactSdk.Helpers;
using Newtonsoft.Json;

namespace InContactSdk.EventSpotEvents
{
    public class EventResponse
    {
        [JsonProperty("active_date")]
        public string ActiveDate { get; internal set; }
        [JsonProperty("address")]
        public EventSpotAddress Address { get; internal set; }
        [JsonProperty("created_date")]
        public string CreatedDate { get; internal set; }
        [JsonProperty("deleted_date")]
        public string DeletedDate { get; internal set; }
        [JsonProperty("description")]
        public string Description { get; internal set; }
        [JsonProperty("end_date")]
        public string EndDate { get; internal set; }
        [JsonProperty("event_detail_url")]
        public string EventDetailUrl { get; internal set; }
        [JsonProperty("id")]
        public string Id { get; internal set; }
        [JsonProperty("is_checkin_available")]
        public bool IsCheckinAvailable { get; internal set; }
        [JsonProperty("location")]
        public string Location { get; internal set; }
        [JsonProperty("name")]
        public string Name { get; internal set; }
        [JsonProperty("registration_url")]
        public string RegistrationUrl { get; internal set; }
        [JsonProperty("start_date")]
        public string StartDate { get; internal set; }
        [JsonProperty("status"), JsonConverter(typeof(TypeEnumConverter<EventSpotStatus, BiLookup<EventSpotStatus, string>>))]
        public EventSpotStatus Status { get; internal set; }
        [JsonProperty("time_zone_id"), JsonConverter(typeof(TypeEnumConverter<InContactSdk.Helpers.TimeZone, BiLookup<InContactSdk.Helpers.TimeZone, string>>))]
        public InContactSdk.Helpers.TimeZone TimeZoneId { get; internal set; }
        [JsonProperty("title")]
        public string Title { get; internal set; }
        [JsonProperty("total_registered_count")]
        public int TotalRegisteredCount { get; internal set; }
        [JsonProperty("type"), JsonConverter(typeof(TypeEnumConverter<EventSpotType, BiLookup<EventSpotType, string>>))]
        public EventSpotType Type { get; internal set; }
    }
}