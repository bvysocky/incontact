﻿#region License
// The MIT License (MIT)
//
// Copyright (c) 2015 Scott Lance, Ethan Tipton
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// 
// The most recent version of this license can be found at: http://opensource.org/licenses/MIT
#endregion

using InContactSdk.Helpers;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace InContactSdk.EventSpotEvents
{
    public interface IEventSpotEventsService
    {
        Task<Event> CancelEventAsync(string eventId);
        Task<Event> CreateEventAsync(Event eventSpotEvent);
        Task<EventFee> CreateEventFeeAsync(string eventId, EventFee eventFee);
        Task<Item> CreateItemAsync(string eventId, Item item);
        Task<ItemAttribute> CreateItemAttributeAsync(string eventId, string itemId, ItemAttribute itemAttribute);
        Task<PromoCodeResponse> CreatePromoCodeAsync<T>(string eventId, T promoCode) where T : PromoCode;
        Task DeleteEventFeeAsync(string eventId, string feeId);
        Task DeleteItemAsync(string eventId, string itemId);
        Task DeleteItemAttributeAsync(string eventId, string itemId, string attributeId);
        Task DeletePromoCodeAsync(string eventId, string promoCodeId);
        Task<Event> GetEventAsync(string eventId);
        Task<EventFee> GetEventFeeAsync(string eventId, string feeId);
        Task<IEnumerable<EventFee>> GetEventFeesAsync(string eventId);
        Task<EventRegistrant> GetEventRegistrantAsync(string eventId, string registrantId);
        Task<PaginatedResult<EventRegistrant>> GetEventRegistrantsAsync(string eventId, int limit = 50);
        Task<PaginatedResult<EventResponse>> GetEventsAsync(int limit = 50);
        Task<Item> GetItemAsync(string eventId, string itemId);
        Task<ItemAttribute> GetItemAttributeAsync(string eventId, string itemId, string attributeId);
        Task<IEnumerable<Item>> GetItemsAsync(string eventId);
        Task<IEnumerable<ItemAttribute>> GetItemAttributesAsync(string eventId, string itemId);
        Task<PromoCodeResponse> GetPromoCodeAsync(string eventId, string promoCodeId);
        Task<IEnumerable<PromoCodeResponse>> GetPromoCodesAsync(string eventId);
        Task<Event> PublishEventAsync(string eventId);
        Task<Event> UpdateEventAsync(string eventId, Event eventSpotEvent);
        Task<EventFee> UpdateEventFeeAsync(string eventId, string feeId, EventFee eventFee);
        Task<Item> UpdateItemAsync(string eventId, string itemId, Item item);
        Task<ItemAttribute> UpdateItemAttributeAsync(string eventId, string itemId, string attributeId, ItemAttribute itemAttribute);
        Task<PromoCodeResponse> UpdatePromoCodeAsync<T>(string eventId, string promoCodeId, T promoCode) where T : PromoCode;
    }
}