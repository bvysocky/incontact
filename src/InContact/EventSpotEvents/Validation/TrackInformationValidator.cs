﻿#region License
// The MIT License (MIT)
//
// Copyright (c) 2015 Scott Lance, Ethan Tipton
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// 
// The most recent version of this license can be found at: http://opensource.org/licenses/MIT
#endregion

using FluentValidation;

namespace InContactSdk.EventSpotEvents.Validation
{
    internal class TrackInformationValidator : AbstractValidator<TrackInformation>
    {
        public TrackInformationValidator()
        {
            RuleFor(m => m.EarlyFeeDate).ISO8601Date().WithMessage("EarlyFeeDate must be a in a ISO 8601 format and must be a date greater than 1/1/1970");
            RuleFor(m => m.GuestDisplayLabel).Length(0, 50).WithMessage("GuestDisplayLable cannot be greater than 50 characters");
            RuleFor(m => m.GuestLimit).InclusiveBetween(0, 100).WithMessage("GuestLimit cannot exceed 100 guests");
            RuleFor(m => m.IsGuestNameRequired).Must(m => !m).When(m => m.IsGuestAnonymousEnabled).WithMessage("IsGuestNameRequired must be set to false when IsGuestAnonymousEnabled is set to true");
            RuleFor(m => m.LateFeeDate).ISO8601Date().WithMessage("LateFeeDate must be a in a ISO 8601 format and must be a date greater than 1/1/1970");
            RuleFor(m => m.RegistrationLimitCount).GreaterThanOrEqualTo(0).WithMessage("RegistrationLimitCount must be a positive number");
            RuleFor(m => m.RegistrationLimitDate).ISO8601Date().WithMessage("RegistrationLimitDate must be a in a ISO 8601 format and must be a date greater than 1/1/1970");
        }
    }
}