﻿#region License
// The MIT License (MIT)
//
// Copyright (c) 2015 Scott Lance, Ethan Tipton
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// 
// The most recent version of this license can be found at: http://opensource.org/licenses/MIT
#endregion

using FluentValidation;

namespace InContactSdk.EventSpotEvents.Validation
{
    internal class EventSpotContactValidator : AbstractValidator<EventSpotContact>
    {
        public EventSpotContactValidator()
        {
            RuleFor(m => m.EmailAddress).Length(0, 128).WithMessage("EmailAddress cannot be greater than 128 characters");
            RuleFor(m => m.Name).NotNull().WithMessage("Name is required").NotEmpty().WithMessage("Name is required").Length(0, 100).WithMessage("Name cannot be greater than 100 characters");
            RuleFor(m => m.OrganizationName).Length(0, 255).WithMessage("OrganizationName cannot be greater than 255 characters");
            RuleFor(m => m.PhoneNumber).Length(0, 25).WithMessage("PhoneNumber cannot be greater than 25 characters");
        }
    }
}