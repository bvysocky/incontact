﻿#region License
// The MIT License (MIT)
//
// Copyright (c) 2015 Scott Lance, Ethan Tipton
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// 
// The most recent version of this license can be found at: http://opensource.org/licenses/MIT
#endregion

using FluentValidation;

namespace InContactSdk.EventSpotEvents.Validation
{
    internal class EventSpotAddressValidator : AbstractValidator<EventSpotAddress>
    {
        public EventSpotAddressValidator()
        {
            RuleFor(m => m.Line1).Length(0, 50).WithMessage("Line1 cannot be greater than 50 characters");
            RuleFor(m => m.Line2).Length(0, 50).WithMessage("Line2 cannot be greater than 50 characters");
            RuleFor(m => m.Line3).Length(0, 50).WithMessage("Line3 cannot be greater than 50 characters");
            RuleFor(m => m.City).Length(0, 50).WithMessage("City cannot be greater than 50 characters");          
            RuleFor(m => m.State).Length(0, 50).WithMessage("State cannot be greater than 50 characters");
            RuleFor(m => m.StateCode).Length(0, 50).WithMessage("StateCode cannot be greater than 50 characters");
            RuleFor(m => m.PostalCode).Length(0, 25).WithMessage("PostalCode cannot be greater than 25 characters");
            RuleFor(m => m.Country).Length(0, 128).WithMessage("Country cannot be greater than 128 characters");
        }
    }
}