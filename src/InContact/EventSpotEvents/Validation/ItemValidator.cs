﻿#region License
// The MIT License (MIT)
//
// Copyright (c) 2015 Scott Lance, Ethan Tipton
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// 
// The most recent version of this license can be found at: http://opensource.org/licenses/MIT
#endregion

using FluentValidation;

namespace InContactSdk.EventSpotEvents.Validation
{
    internal class ItemValidator : AbstractValidator<Item>
    {
        public ItemValidator()
        {
            RuleFor(m => m.DefaultQuantityTotal).GreaterThanOrEqualTo(0).WithMessage("DefaultQuantityTotal must be 0 or greater");
            RuleFor(m => m.Description).Length(0, 2048).WithMessage("Description cannot be greater than 2048 characters");
            RuleFor(m => m.Name).NotEmpty().WithMessage("Name is required").Length(1, 100).WithMessage("Name cannot be greater than 100 characters");
            RuleFor(m => m.PerRegistrantLimit).GreaterThanOrEqualTo(0).WithMessage("PerRegistrantLimit must be 0 or greater");
            RuleFor(m => m.Price).GreaterThanOrEqualTo(0).WithMessage("Price must be 0 or greater");
        }
    }
}