﻿#region License
// The MIT License (MIT)
//
// Copyright (c) 2015 Scott Lance, Ethan Tipton
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// 
// The most recent version of this license can be found at: http://opensource.org/licenses/MIT
#endregion

using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using InContactSdk.EventSpotEvents.Models;
using InContactSdk.Helpers;
using InContactSdk.Models;
using InContactSdk.Validation;

namespace InContactSdk.EventSpotEvents
{
    public class EventSpotEventsService : ServiceBase, IEventSpotEventsService
    {
        public EventSpotEventsService(IInContactSettings settings)
            : base(settings)
        { }

        internal EventSpotEventsService(IInContactSettings settings, IValidationService validationService, IWebServiceRequest webServiceRequest)
            : base(settings, validationService, webServiceRequest)
        { }

        // Reference: http://developer.constantcontact.com/docs/eventspot-apis/individual-events.html?method=PATCH
        public async Task<Event> CancelEventAsync(string eventId)
        {
            var cancelModel = new PublishCancelModel[] { new PublishCancelModel(true) };
            var model = new EventSpotIndividualModel() { EventId = eventId };

            validationService.Validate(model);

            return await webServiceRequest.PatchDeserializedAsync<IEnumerable<PublishCancelModel>, Event>(new Uri(model.GenerateEndpoint(settings)), settings.AuthToken, cancelModel);
        }

        // Reference: http://developer.constantcontact.com/docs/eventspot-apis/events-collection.html?method=POST
        public async Task<Event> CreateEventAsync(Event eventSpotEvent)
        {
            var model = new CreateEventModel() { Event = eventSpotEvent };
            validationService.Validate(eventSpotEvent);

            return await webServiceRequest.PostDeserializedAsync<Event, Event>(new Uri(model.GenerateEndpoint(settings)), settings.AuthToken, eventSpotEvent);
        }

        // Reference: http://developer.constantcontact.com/docs/eventspot-apis/event-fees-collection.html?method=POST
        public async Task<EventFee> CreateEventFeeAsync(string eventId, EventFee eventFee)
        {
            var model = new CreateEventFeeModel { EventId = eventId, EventFee = eventFee };
            validationService.Validate(model);

            return await webServiceRequest.PostDeserializedAsync<EventFee, EventFee>(new Uri(model.GenerateEndpoint(settings)), settings.AuthToken, eventFee);
        }

        // Reference: http://developer.constantcontact.com/docs/eventspot-apis/event-item-collection.html?method=POST
        public async Task<Item> CreateItemAsync(string eventId, Item item)
        {
            var model = new CreateItemModel() { EventId = eventId, Item = item };
            validationService.Validate(model);

            return await webServiceRequest.PostDeserializedAsync<Item, Item>(new Uri(model.GenerateEndpoint(settings)), settings.AuthToken, item);
        }

        // Reference: http://developer.constantcontact.com/docs/eventspot-apis/item-attribute-collection.html?method=POST
        public async Task<ItemAttribute> CreateItemAttributeAsync(string eventId, string itemId, ItemAttribute itemAttribute)
        {
            var model = new CreateItemAttributeModel() { EventId = eventId, ItemId = itemId, ItemAttribute = itemAttribute };
            validationService.Validate(model);

            return await webServiceRequest.PostDeserializedAsync<ItemAttribute, ItemAttribute>(new Uri(model.GenerateEndpoint(settings)), settings.AuthToken, itemAttribute);
        }

        // Reference: http://developer.constantcontact.com/docs/eventspot-apis/promocode-collection.html?method=POST
        public async Task<PromoCodeResponse> CreatePromoCodeAsync<T>(string eventId, T promoCode)
            where T : PromoCode
        {
            var model = new CreatePromoCodeModel<T>() { EventId = eventId, PromoCode = promoCode };
            model.Validate();

            return await webServiceRequest.PostDeserializedAsync<T, PromoCodeResponse>(new Uri(model.GenerateEndpoint(settings)), settings.AuthToken, promoCode);
        }

        // Reference: http://developer.constantcontact.com/docs/eventspot-apis/individual-event-fees.html?method=DELETE
        public async Task DeleteEventFeeAsync(string eventId, string feeId)
        {
            var model = new EventSpotFeeInfoModel() { EventId = eventId, FeeId = feeId };
            validationService.Validate(model);

            await webServiceRequest.DeleteAsync(new Uri(model.GenerateEndpoint(settings)), settings.AuthToken);
        }

        // Reference: http://developer.constantcontact.com/docs/eventspot-apis/event-item-resource.html?method=DELETE
        public async Task DeleteItemAsync(string eventId, string itemId)
        {
            var model = new EventSpotItemInfoModel() { EventId = eventId, ItemId = itemId };
            validationService.Validate(model);

            await webServiceRequest.DeleteAsync(new Uri(model.GenerateEndpoint(settings)), settings.AuthToken);
        }

        // Reference: http://developer.constantcontact.com/docs/eventspot-apis/item-attribute-resource.html?method=DELETE
        public async Task DeleteItemAttributeAsync(string eventId, string itemId, string attributeId)
        {
            var model = new EventSpotItemAttributeInfoModel() { EventId = eventId, ItemId = itemId, AttributeId = attributeId };
            validationService.Validate(model);

            await webServiceRequest.DeleteAsync(new Uri(model.GenerateEndpoint(settings)), settings.AuthToken);
        }

        // Reference: http://developer.constantcontact.com/docs/eventspot-apis/individual-promocodes.html?method=DELETE
        public async Task DeletePromoCodeAsync(string eventId, string promoCodeId)
        {
            var model = new EventSpotPromoCodeInfoModel() { EventId = eventId, PromoCodeId = promoCodeId };
            validationService.Validate(model);

            await webServiceRequest.DeleteAsync(new Uri(model.GenerateEndpoint(settings)), settings.AuthToken);
        }

        // Reference: http://developer.constantcontact.com/docs/eventspot-apis/individual-events.html
        public async Task<Event> GetEventAsync(string eventId)
        {
            var model = new EventSpotIndividualModel() { EventId = eventId };
            validationService.Validate(model);

            return await webServiceRequest.GetDeserializedAsync<Event>(new Uri(model.GenerateEndpoint(settings)), settings.AuthToken);
        }

        // Reference: http://developer.constantcontact.com/docs/eventspot-apis/individual-event-fees.html
        public async Task<EventFee> GetEventFeeAsync(string eventId, string feeId)
        {
            var model = new EventSpotFeeInfoModel() { EventId = eventId, FeeId = feeId };
            validationService.Validate(model);

            return await webServiceRequest.GetDeserializedAsync<EventFee>(new Uri(model.GenerateEndpoint(settings)), settings.AuthToken);
        }

        // Reference: http://developer.constantcontact.com/docs/eventspot-apis/event-fees-collection.html
        public async Task<IEnumerable<EventFee>> GetEventFeesAsync(string eventId)
        {
            var model = new EventSpotFeesModel() { EventId = eventId };
            validationService.Validate(model);

            return await webServiceRequest.GetDeserializedAsync<IEnumerable<EventFee>>(new Uri(model.GenerateEndpoint(settings)), settings.AuthToken);
        }

        // Reference: http://developer.constantcontact.com/docs/eventspot-apis/registrants-resource.html
        public async Task<EventRegistrant> GetEventRegistrantAsync(string eventId, string registrantId)
        {
            var model = new EventSpotRegistrantInfoModel() { EventId = eventId, RegistrantId = registrantId };
            validationService.Validate(model);

            return await webServiceRequest.GetDeserializedAsync<EventRegistrant>(new Uri(model.GenerateEndpoint(settings)), settings.AuthToken);
        }

        // Reference: http://developer.constantcontact.com/docs/eventspot-apis/event-registrants-collection.html
        public async Task<PaginatedResult<EventRegistrant>> GetEventRegistrantsAsync(string eventId, int limit = 50)
        {
            var model = new GetEventRegistrantsModel() { EventId = eventId, Limit = limit };
            validationService.Validate(model);

            return await webServiceRequest.GetDeserializedAsync<PaginatedResult<EventRegistrant>>(new Uri(model.GenerateEndpoint(settings)), settings.AuthToken);
        }

        // Reference: http://developer.constantcontact.com/docs/eventspot-apis/events-collection.html?method=GET
        public async Task<PaginatedResult<EventResponse>> GetEventsAsync(int limit = 50)
        {
            var model = new GetEventsModel() { Limit = limit };
            validationService.Validate(model);

            return await webServiceRequest.GetDeserializedAsync<PaginatedResult<EventResponse>>(new Uri(model.GenerateEndpoint(settings)), settings.AuthToken);
        }

        // Reference: http://developer.constantcontact.com/docs/eventspot-apis/event-item-resource.html
        public async Task<Item> GetItemAsync(string eventId, string itemId)
        {
            var model = new EventSpotItemInfoModel() { EventId = eventId, ItemId = itemId };
            validationService.Validate(model);

            return await webServiceRequest.GetDeserializedAsync<Item>(new Uri(model.GenerateEndpoint(settings)), settings.AuthToken);
        }

        // Reference: http://developer.constantcontact.com/docs/eventspot-apis/item-attribute-resource.html
        public async Task<ItemAttribute> GetItemAttributeAsync(string eventId, string itemId, string attributeId)
        {
            var model = new EventSpotItemAttributeInfoModel() { EventId = eventId, ItemId = itemId, AttributeId = attributeId };
            validationService.Validate(model);

            return await webServiceRequest.GetDeserializedAsync<ItemAttribute>(new Uri(model.GenerateEndpoint(settings)), settings.AuthToken);
        }

        // Reference: http://developer.constantcontact.com/docs/eventspot-apis/event-item-collection.html?method=GET
        public async Task<IEnumerable<Item>> GetItemsAsync(string eventId)
        {
            var model = new EventSpotItemsModel() { EventId = eventId };
            validationService.Validate(model);

            return await webServiceRequest.GetDeserializedAsync<IEnumerable<Item>>(new Uri(model.GenerateEndpoint(settings)), settings.AuthToken);
        }

        // Reference: http://developer.constantcontact.com/docs/eventspot-apis/item-attribute-collection.html
        public async Task<IEnumerable<ItemAttribute>> GetItemAttributesAsync(string eventId, string itemId)
        {
            var model = new EventSpotItemAttributesModel() { EventId = eventId, ItemId = itemId };
            validationService.Validate(model);

            return await webServiceRequest.GetDeserializedAsync<IEnumerable<ItemAttribute>>(new Uri(model.GenerateEndpoint(settings)), settings.AuthToken);
        }

        // Reference: http://developer.constantcontact.com/docs/eventspot-apis/individual-promocodes.html
        public async Task<PromoCodeResponse> GetPromoCodeAsync(string eventId, string promoCodeId)
        {
            var model = new EventSpotPromoCodeInfoModel() { EventId = eventId, PromoCodeId = promoCodeId };
            validationService.Validate(model);

            return await webServiceRequest.GetDeserializedAsync<PromoCodeResponse>(new Uri(model.GenerateEndpoint(settings)), settings.AuthToken);
        }

        // Reference: http://developer.constantcontact.com/docs/eventspot-apis/individual-promocodes.html
        public async Task<IEnumerable<PromoCodeResponse>> GetPromoCodesAsync(string eventId)
        {
            var model = new EventSpotPromoCodesModel() { EventId = eventId };
            validationService.Validate(model);

            return await webServiceRequest.GetDeserializedAsync<IEnumerable<PromoCodeResponse>>(new Uri(model.GenerateEndpoint(settings)), settings.AuthToken);
        }

        // Reference: http://developer.constantcontact.com/docs/eventspot-apis/individual-events.html?method=PATCH
        public async Task<Event> PublishEventAsync(string eventId)
        {
            var publishModel = new PublishCancelModel[] { new PublishCancelModel(false) };
            var model = new EventSpotIndividualModel() { EventId = eventId };

            validationService.Validate(model);

            return await webServiceRequest.PatchDeserializedAsync<IEnumerable<PublishCancelModel>, Event>(new Uri(model.GenerateEndpoint(settings)), settings.AuthToken, publishModel);
        }

        // Reference: http://developer.constantcontact.com/docs/eventspot-apis/individual-events.html?method=PUT
        public async Task<Event> UpdateEventAsync(string eventId, Event eventSpotEvent)
        {
            var model = new UpdateEventModel() { EventId = eventId, Event = eventSpotEvent };
            validationService.Validate(model);

            return await webServiceRequest.PutDeserializedAsync<Event, Event>(new Uri(model.GenerateEndpoint(settings)), settings.AuthToken, eventSpotEvent);
        }

        // Reference: http://developer.constantcontact.com/docs/eventspot-apis/individual-event-fees.html?method=PUT
        public async Task<EventFee> UpdateEventFeeAsync(string eventId, string feeId, EventFee eventFee)
        {
            var model = new UpdateEventFeeModel() { EventId = eventId, FeeId = feeId, EventFee = eventFee };
            validationService.Validate(model);

            return await webServiceRequest.PutDeserializedAsync<EventFee, EventFee>(new Uri(model.GenerateEndpoint(settings)), settings.AuthToken, eventFee);
        }

        // Reference: http://developer.constantcontact.com/docs/eventspot-apis/event-item-resource.html?method=PUT
        public async Task<Item> UpdateItemAsync(string eventId, string itemId, Item item)
        {
            var model = new UpdateItemModel() { EventId = eventId, ItemId = itemId, Item = item };
            validationService.Validate(model);

            return await webServiceRequest.PutDeserializedAsync<Item, Item>(new Uri(model.GenerateEndpoint(settings)), settings.AuthToken, item);
        }

        // Reference: http://developer.constantcontact.com/docs/eventspot-apis/item-attribute-resource.html?method=PUT
        public async Task<ItemAttribute> UpdateItemAttributeAsync(string eventId, string itemId, string attributeId, ItemAttribute itemAttribute)
        {
            var model = new UpdateItemAttributeModel() { EventId = eventId, ItemId = itemId, AttributeId = attributeId, ItemAttribute = itemAttribute };
            validationService.Validate(model);

            return await webServiceRequest.PutDeserializedAsync<ItemAttribute, ItemAttribute>(new Uri(model.GenerateEndpoint(settings)), settings.AuthToken, itemAttribute);
        }

        // Reference: http://developer.constantcontact.com/docs/eventspot-apis/individual-promocodes.html?method=PUT
        public async Task<PromoCodeResponse> UpdatePromoCodeAsync<T>(string eventId, string promoCodeId, T promoCode)
            where T : PromoCode
        {
            var model = new UpdatePromoCodeModel<T>() { EventId = eventId, PromoCodeId = promoCodeId, PromoCode = promoCode };
            validationService.Validate(model);

            return await webServiceRequest.PutDeserializedAsync<T, PromoCodeResponse>(new Uri(model.GenerateEndpoint(settings)), settings.AuthToken, promoCode);
        }
    }
}