﻿#region License
// The MIT License (MIT)
//
// Copyright (c) 2015 Scott Lance, Ethan Tipton
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// 
// The most recent version of this license can be found at: http://opensource.org/licenses/MIT
#endregion

using FluentValidation;
using InContactSdk.Helpers;

namespace InContactSdk.Account.Validation
{
    internal class OrganizationAddressValidator : AbstractValidator<OrganizationAddress>
    {
        public OrganizationAddressValidator()
        {
            RuleFor(o => o.City).NotNull().WithMessage("City is required").NotEmpty().WithMessage("City is required");
            RuleFor(o => o.Line1).NotNull().NotEmpty().WithMessage("Line1 is required");
            RuleFor(o => o.State).Length(0, 50).WithMessage("State cannot contain more than 50 characters");
            RuleFor(o => o.State).Must(s => s == null).When(cc => cc.CountryCode != CountryCode.NONE && (cc.CountryCode == CountryCode.US || cc.CountryCode == CountryCode.CA)).WithMessage("State cannot be entered if the CountryCode is US or CA");
            RuleFor(o => o.State).Length(2).When(s => s.State != null).WithMessage("StateCode must be exactly 2 characters");
            RuleFor(o => o.StateCode).Must(sc => sc == null).When(cc => cc.CountryCode != CountryCode.NONE && (cc.CountryCode != CountryCode.US && cc.CountryCode != CountryCode.CA)).WithMessage("StateCode cannot be entered if the CountryCode is not US or CA");
            RuleFor(o => o.StateCode).Must(sc => sc != null).When(cc => cc.CountryCode != CountryCode.NONE && (cc.CountryCode == CountryCode.US || cc.CountryCode == CountryCode.CA)).WithMessage("StateCode must be entered if the CountryCode is US or CA");
        }
    }
}