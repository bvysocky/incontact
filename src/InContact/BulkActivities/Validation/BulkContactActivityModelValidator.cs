﻿#region License
// The MIT License (MIT)
//
// Copyright (c) 2015 Scott Lance, Ethan Tipton
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// 
// The most recent version of this license can be found at: http://opensource.org/licenses/MIT
#endregion

using FluentValidation;
using InContactSdk.BulkActivities.Models;
using System;

namespace InContactSdk.BulkActivities.Validation
{
    internal class BulkContactActivityModelValidator : AbstractValidator<BulkContactActivityModel>
    {
        public BulkContactActivityModelValidator()
        {
            RuleFor(m => m.FileName).NotEmpty().WithMessage("FileName is requried");
            RuleFor(m => m.Data).NotNull().WithMessage("The provided file was not able to be read");
            RuleFor(m => m.Lists).Must(m => ListsIsValidArray(m)).WithMessage("Lists must be a comma delimited set of contact list ids");
            RuleFor(m => m.MimeType).NotEmpty().WithMessage("MimeType is required");
        }

        private bool ListsIsValidArray(string lists)
        {
            foreach (var l in lists.Split(','))
            {
                Int64 output;
                if (!Int64.TryParse(l, out output))
                    return false;
            }

            return true;
        }
    }
}