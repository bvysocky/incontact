﻿#region License
// The MIT License (MIT)
//
// Copyright (c) 2015 Scott Lance, Ethan Tipton
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// 
// The most recent version of this license can be found at: http://opensource.org/licenses/MIT
#endregion

using InContactSdk.BulkActivities.Validation;
using InContactSdk.Exceptions;
using InContactSdk.Helpers;
using InContactSdk.Validation;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace InContactSdk.BulkActivities
{
    public class ContactExport : IValidatable
    {
        [JsonProperty("lists")]
        public IEnumerable<string> Lists { get; set; }
        [JsonProperty("file_type")]
        public string FileType { get { return "CSV"; } }
        [JsonProperty("sort_by"), JsonConverter(typeof(TypeEnumConverter<BulkActivitySortBy, BiLookup<BulkActivitySortBy, string>>))]
        public BulkActivitySortBy SortBy { get; set; }
        [JsonProperty("export_date_added")]
        public bool ExportDateAdded { get; set; }
        [JsonProperty("export_added_by")]
        public bool ExportAddedBy { get; set; }
        [JsonProperty("column_names"), JsonConverter(typeof(TypeEnumConverter<ExportColumnName, BiLookup<ExportColumnName, string>>))]
        public IEnumerable<ExportColumnName> ColumnNames { get; set; }

        public IEnumerable<ValidationError> Validate()
        {
            return Validator.Validate<ContactExport, ContactExportValidator>(this);
        }
    }
}
