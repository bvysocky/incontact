﻿#region License
// The MIT License (MIT)
//
// Copyright (c) 2015 Scott Lance, Ethan Tipton
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// 
// The most recent version of this license can be found at: http://opensource.org/licenses/MIT
#endregion

using InContactSdk.EmailCampaigns.Models;
using InContactSdk.Helpers;
using InContactSdk.Models;
using InContactSdk.Validation;
using System;
using System.Threading.Tasks;

namespace InContactSdk.EmailCampaigns
{
    public class EmailCampaignsService : ServiceBase, IEmailCampaignsService
    {
        public EmailCampaignsService(IInContactSettings settings)
            : base(settings)
        { }

        internal EmailCampaignsService(IInContactSettings settings, IValidationService validationService, IWebServiceRequest webServiceRequest)
            : base(settings, validationService, webServiceRequest)
        { }

        // Reference: http://developer.constantcontact.com/docs/email-campaigns/email-campaigns-collection.html?method=POST
        public async Task<EmailCampaign> CreateEmailCampaignAsync(EmailCampaign campaign)
        {
            validationService.Validate(campaign);

            var endpoint = new SimpleModel(ServiceEndpoint.EmailCampaigns).GenerateEndpoint(settings);
            return await webServiceRequest.PostDeserializedAsync<EmailCampaign, EmailCampaign>(new Uri(endpoint), settings.AuthToken, campaign);
        }

        // Reference: http://developer.constantcontact.com/docs/email-campaigns/email-campaign-resource.html?method=DELETE
        public async Task DeleteEmailCampaignAsync(string campaignId)
        {
            var model = new EmailCampaignIdModel { Settings = settings, CampaignId = campaignId };
            validationService.Validate(model);           
            await webServiceRequest.DeleteAsync(new Uri(model.GenerateEndpoint()), settings.AuthToken);
        }

        // Reference: http://developer.constantcontact.com/docs/email-campaigns/email-campaign-resource.html?method=GET
        public async Task<EmailCampaign> GetEmailCampaignAsync(string campaignId, bool updateSummary = false)
        {
            var model = new GetEmailCampaignModel { Settings = settings, CampaignId = campaignId, UpdateSummary = updateSummary };
            validationService.Validate(model);
            return await webServiceRequest.GetDeserializedAsync<EmailCampaign>(new Uri(model.GenerateEndpoint()), settings.AuthToken);
        }

        // Reference: http://developer.constantcontact.com/docs/email-campaigns/email-campaigns-collection.html?method=GET
        public async Task<PaginatedResult<EmailCampaignResponse>> GetEmailCampaignsAsync(int limit = 50, DateTime? modifiedSince = null, EmailCampaignStatus status = EmailCampaignStatus.All)
        {
            var model = new EmailCampaignModel { Settings = settings, Limit = limit, ModifiedSince = modifiedSince.HasValue ? modifiedSince.Value.ToString("o") : null, Status = status };
            validationService.Validate(model);
            return await webServiceRequest.GetDeserializedAsync<PaginatedResult<EmailCampaignResponse>>(new Uri(model.GenerateEndpoint()), settings.AuthToken);
        }

        // Reference: http://developer.constantcontact.com/docs/email-campaigns/email-campaign-resource.html?method=PUT
        public async Task<EmailCampaign> UpdateEmailCampaignAsync(string campaignId, EmailCampaign campaign)
        {
            var model = new EmailCampaignIdModel { Settings = settings, CampaignId = campaignId };
            validationService.Validate(model);
            validationService.Validate(campaign);            
            return await webServiceRequest.PutDeserializedAsync<EmailCampaign, EmailCampaign>(new Uri(model.GenerateEndpoint()), settings.AuthToken, campaign);
        }
    }
}