﻿#region License
// The MIT License (MIT)
//
// Copyright (c) 2015 Scott Lance, Ethan Tipton
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// 
// The most recent version of this license can be found at: http://opensource.org/licenses/MIT
#endregion

using System.Collections.Generic;
using InContactSdk.EmailCampaigns.Validation;
using InContactSdk.Exceptions;
using InContactSdk.Validation;
using Newtonsoft.Json;

namespace InContactSdk.EmailCampaigns
{
    public class MessageFooter : IValidatable
    {
        [JsonProperty("organization_name")]
        public string OrganizationName { get; set; }
        [JsonProperty("address_line_1")]
        public string AddressLine1 { get; set; }
        [JsonProperty("address_line_2")]
        public string AddressLine2 { get; set; }
        [JsonProperty("address_line_3")]
        public string AddressLine3 { get; set; }
        [JsonProperty("city")]
        public string City { get; set; }
        [JsonProperty("state")]
        public string State { get; set; }
        [JsonProperty("international_state")]
        public string InternationalState { get; set; }
        [JsonProperty("postal_code")]
        public string PostalCode { get; set; }
        [JsonProperty("country")]
        public string Country { get; set; }
        [JsonProperty("forward_email_link_text")]
        public string ForwardEmailLinkText { get; set; }
        [JsonProperty("include_forward_email")]
        public bool IncludeForwardEmail { get; set; }
        [JsonProperty("include_subscribe_link")]
        public bool InlcudeSubscribeLink { get; set; }
        [JsonProperty("subscribe_link_text")]
        public string SubscribeLinkText { get; set; }

        public IEnumerable<ValidationError> Validate()
        {
            return Validator.Validate<MessageFooter, MessageFooterValidator>(this);
        }
    }
}