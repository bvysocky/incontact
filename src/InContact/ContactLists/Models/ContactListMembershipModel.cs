﻿#region License
// The MIT License (MIT)
//
// Copyright (c) 2015 Scott Lance, Ethan Tipton
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// 
// The most recent version of this license can be found at: http://opensource.org/licenses/MIT
#endregion

using InContactSdk.ContactLists.Validation;
using InContactSdk.Exceptions;
using InContactSdk.Helpers;
using InContactSdk.Models;
using InContactSdk.Validation;
using System;
using System.Collections.Generic;
using System.Text;

namespace InContactSdk.ContactLists.Models
{
    internal class ContactListMembershipModel : IValidatable
    {
        public IInContactSettings Settings { get; set; }
        public string ListId { get; set; }
        public DateTime? ModifiedSince { get; set; }
        public int Limit { get; set; }

        public IEnumerable<ValidationError> Validate()
        {
            return Validator.Validate<ContactListMembershipModel, ContactListMembershipModelValidator>(this);
        }

        public string GenerateEndpoint()
        {
            StringBuilder endpoint = new StringBuilder();
            endpoint.AppendFormat("{0}{1}/{2}/contacts?api_key={3}", Settings.BaseUrl, EnumStrings.ServiceEndpointStrings[ServiceEndpoint.ContactList], this.ListId, Settings.ApiKey);

            if (this.Limit != 50)
                endpoint.AppendFormat("&limit={0}", this.Limit);

            if (this.ModifiedSince.HasValue)
                endpoint.AppendFormat("&modified_since={0}", this.ModifiedSince.Value.ToString("o"));

            return endpoint.ToString();
        }
    }
}