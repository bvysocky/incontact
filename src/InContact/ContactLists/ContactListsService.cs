﻿#region License
// The MIT License (MIT)
//
// Copyright (c) 2015 Scott Lance, Ethan Tipton
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// 
// The most recent version of this license can be found at: http://opensource.org/licenses/MIT
#endregion

using InContactSdk.ContactLists.Models;
using InContactSdk.Contacts;
using InContactSdk.Helpers;
using InContactSdk.Models;
using InContactSdk.Validation;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace InContactSdk.ContactLists
{
    public class ContactListsService : ServiceBase, IContactListsService
    {
        public ContactListsService(IInContactSettings settings)
            : base(settings)
        { }

        internal ContactListsService(IInContactSettings settings, IValidationService validationService, IWebServiceRequest webServiceRequest)
            : base(settings, validationService, webServiceRequest)
        { }

        // Reference: http://developer.constantcontact.com/docs/contact-list-api/contactlist-collection.html?method=POST
        public async Task<ContactList> CreateContactListAsync(ContactList list)
        {
            validationService.Validate(list);

            var endpoint = new SimpleModel(ServiceEndpoint.ContactList).GenerateEndpoint(settings);
            return await webServiceRequest.PostDeserializedAsync<ContactList, ContactList>(new Uri(endpoint), settings.AuthToken, list);
        }

        // Reference: http://developer.constantcontact.com/docs/contact-list-api/contactlist-resource.html?method=DELETE
        public async Task DeleteContactListAsync(string listId)
        {
            var model = new ListIdModel { Settings = settings, ListId = listId };
            validationService.Validate(model);
            await webServiceRequest.DeleteAsync(new Uri(model.GenerateEndpoint()), settings.AuthToken);
        }

        // Reference: http://developer.constantcontact.com/docs/contact-list-api/contactlist-resource.html
        public async Task<ContactList> GetContactListAsync(string listId)
        {
            var model = new ListIdModel { Settings = settings, ListId = listId };
            validationService.Validate(model);
            return await webServiceRequest.GetDeserializedAsync<ContactList>(new Uri(model.GenerateEndpoint()), settings.AuthToken);
        }

        // Reference: http://developer.constantcontact.com/docs/contact-list-api/contactlist-membership-collection.html
        public async Task<PaginatedResult<Contact>> GetContactListMembershipAsync(string listId, DateTime? modifiedSince = null, int limit = 50)
        {
            var model = new ContactListMembershipModel { Settings = settings, ListId = listId, ModifiedSince = modifiedSince, Limit = limit };
            validationService.Validate(model);
            return await webServiceRequest.GetDeserializedAsync<PaginatedResult<Contact>>(new Uri(model.GenerateEndpoint()), settings.AuthToken);
        }

        // Reference: https://developer.constantcontact.com/docs/contact-list-api/contactlist-collection.html
        public async Task<IEnumerable<ContactList>> GetContactListsAsync(DateTime? modifiedSince)
        {
            var model = new GetContactListModel { Settings = settings, ModifiedSince = modifiedSince };
            return await webServiceRequest.GetDeserializedAsync<IEnumerable<ContactList>>(new Uri(model.GenerateEndpoint()), settings.AuthToken);
        }

        // Reference: http://developer.constantcontact.com/docs/contact-list-api/contactlist-resource.html?method=PUT
        public async Task<ContactList> UpdateContactListAsync(string listId, ContactList list)
        {
            var model = new UpdateContactListModel { Settings = settings, ListId = listId, List = list };
            validationService.Validate(model);
            return await webServiceRequest.PutDeserializedAsync<ContactList, ContactList>(new Uri(model.GenerateEndpoint()), settings.AuthToken, list);
        }
    }
}