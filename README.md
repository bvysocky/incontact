# InContact SDK v0.7.0-beta

InContact is a fully asynchronous and validatable .NET SDK designed around the Constant Contact API v2

## Installation
Use NuGet to get the latest version of the SDK 

```
Install-Package InContact -Pre
```

Create new instance of the InContact object and pass your Api Key and Access Token:

```
#!C#
var settings = new InContactSettings("E63uc1T1LrB3W4P5KUs3yZg59", "ef141a22-b606-4577-b39c-87322f4799fb");
```

Create a service and start calling the API:

```
#!C#
var contactsService = new ContactsService(settings);
var contacts = await contactsService.GetContactsPagedAsync();
```

That's it!

## Documentation

Visit the documentation wiki at: https://bitbucket.org/scottlance/incontact/wiki/Home